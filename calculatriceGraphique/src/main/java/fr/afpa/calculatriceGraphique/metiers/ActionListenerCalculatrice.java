package fr.afpa.calculatriceGraphique.metiers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;

import javax.swing.JOptionPane;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActionListenerCalculatrice extends Observable implements ActionListener {

	private String nb1;
	private String nb2;
	private String operateur;
	private String resultat;
	private String message;
	private boolean enCours;

	private String calcul;

	public ActionListenerCalculatrice() {
		super();
		reset();
		message = "";
	}

	public void actionPerformed(ActionEvent e) {
		message = "";

		switch (e.getActionCommand()) {
		case "C":
			reset();
			break;
		case "quitter":
			System.exit(0);
			break;
		case "histo":
			affichageHistorique();
			break;
		case "aide":
			affichageAide();
			break;
		case "effacer":
			effacer();
			break;
		default:
			break;
		}

		if (e.getActionCommand().matches("[0-9]")) {
			if ("".equals(operateur)) {
				if (enCours) {
					reset();
					calcul="";
					enCours = false;
				}
				nb1 += e.getActionCommand();
			} else {
				nb2 += e.getActionCommand();
			}
			calcul += e.getActionCommand();
		}
		if (e.getActionCommand().matches("[/*\\-+]")) {
			if ("".equals(operateur) && nb1.length() > 0) {
				operateur = e.getActionCommand();
			} else {
				if (calcul()) {
					nb1 = resultat;
					nb2 = "";
					resultat="";
					operateur = e.getActionCommand();
				} else if (e.getActionCommand().matches("[\\-+]")) {
					nb1 = e.getActionCommand();
					operateur = "";
				} else {
					message = "ERREUR CALCUL";
					reset();
				}
			}
			calcul += e.getActionCommand();
		}

		if (e.getActionCommand().matches("[=]")) {
			if (nb1.length() > 0 && nb2.length() > 0) {
				calcul();
				enregistrerResultat();
			}
		}
		setChanged();
	    notifyObservers("coucou");
	}

	public boolean calcul() {
		if (nb1.length() > 0 && nb2.length() > 0) {
			enCours=true;
			switch (operateur) {
			case "+":
				resultat = "" + (Integer.parseInt(nb1) + Integer.parseInt(nb2));
				return true;
			case "-":
				resultat = "" + (Integer.parseInt(nb1) - Integer.parseInt(nb2));
				return true;
			case "*":
				resultat = "" + (Integer.parseInt(nb1) * Integer.parseInt(nb2));
				return true;
			case "/":
				if (Integer.parseInt(nb2) != 0) {
					resultat = "" + (Integer.parseInt(nb1) / Integer.parseInt(nb2));
					return true;
				} else {
					message = "ERREUR DIVISION PAR 0";
					reset();
				}
			}
		}
		return false;
	}

	public void affichageAide() {
		String message = "calculatrice : aide à faire des opérations";
		JOptionPane.showMessageDialog(null, message);

	}

	public void affichageHistorique() {
		FichierService fs = new FichierService();
		List<String> historique = fs.lectureFichier("ressources/histo.txt");
		String message = "";
		for (String calcul : historique) {
			message += calcul + "\n";
		}
		JOptionPane.showMessageDialog(null, message);

	}

	public void enregistrerResultat() {
		if (!"".equals(nb1) && !"".equals(nb2) && !"".equals(resultat)) {
			FichierService fs = new FichierService();
			fs.ecritureFichier("ressources/histo.txt", calcul + "=" + resultat, true);
			nb1 = resultat;
			resultat = "";
			nb2 = "";
			operateur = "";
			calcul = nb1;
			enCours = true;
		}
	}

	public void reset() {
		nb1 = "";
		nb2 = "";
		operateur = "";
		resultat = "";
		calcul = "";
		enCours = false;
	}

	private void effacer() {
		FichierService fs = new FichierService();
		fs.ecritureFichier("ressources/histo.txt", "", false);

	}

}
