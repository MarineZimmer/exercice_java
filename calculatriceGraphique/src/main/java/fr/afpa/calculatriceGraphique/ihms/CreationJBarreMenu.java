package fr.afpa.calculatriceGraphique.ihms;

import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;



public class CreationJBarreMenu extends JMenuBar{
	private JMenu menu;
	
	
	private ActionListener listener;

	public CreationJBarreMenu(ActionListener listener) {
		super();
		this.listener=listener;
		initialisation();
		
	}
	
	public void initialisation() {
		menu=creationMenu("Fichier");
		menu.add(creationMenuItem("Reset", "C"));
		menu.add(creationMenuItem("Histo", "histo"));
		menu.add(creationMenuItem("Effacer historique", "effacer"));
		menu.add(creationMenuItem("Quitter", "quitter"));
		add(menu);
		menu=creationMenu("?");
		menu.add(creationMenuItem("aide", "aide"));
		add(menu);
	}
	
	public JMenu creationMenu(String nom) {
		return new JMenu(nom);
	}
	
	public JMenuItem creationMenuItem(String nom, String actionCommand) {
		JMenuItem menuItem = new JMenuItem(nom);
		if(!"".equals(actionCommand)) {
			menuItem.setActionCommand(actionCommand);
		}
		menuItem.addActionListener(listener);
		return menuItem;
	}
	
	
}
