package fr.afpa.calculatriceGraphique.ihms;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fr.afpa.calculatriceGraphique.metiers.ActionListenerCalculatrice;

public class Calculatrice extends JFrame implements Runnable, Observer {

	private JButton bouton;
	private JTextField jTextFieldEcran;
	private JTextField jTextFieldCalcul;
	
	private JPanel jPanelEcran;
	private JPanel jPanelEcran2;
	private JPanel jPanelBoutons;
	private JPanel jPanelBoutonsZero;
	private JPanel jPanelBoutonsOperateur;
	
	
	private ActionListenerCalculatrice actionListener;
	private JMenuBar menuCalculatrice;

	public Calculatrice(String title) throws HeadlessException {
		super(title);
		setBounds(600, 400, 250, 200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		actionListener = new ActionListenerCalculatrice();
		actionListener.addObserver(this);
		setResizable(false);
		
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		initialisation();

	}

	/**
	 * 
	 */
	public void initialisation() {
		GridLayout gridLayout;
		menuCalculatrice = new CreationJBarreMenu(actionListener);
		setJMenuBar(menuCalculatrice);
		jPanelEcran= new JPanel();
		jPanelEcran2= new JPanel();
		gridLayout = new GridLayout(1, 1);
		gridLayout.setHgap(0);
		gridLayout.setVgap(0);
		jPanelEcran.setLayout(gridLayout);
		jPanelEcran2.setLayout(new GridLayout(1, 2));
		
		jTextFieldCalcul = new JTextField("");
		jTextFieldCalcul.setEditable(false);
		jPanelEcran2.add(jTextFieldCalcul);
		
		
		jPanelEcran2.add(new CreationJButton("C", "C", actionListener));
		
		//jPanelEcran.add(jPanelEcran2);

		jTextFieldEcran = new JTextField("");
		jTextFieldEcran.setEditable(false);
		jPanelEcran.add(jTextFieldEcran);
		getContentPane().add(jPanelEcran, BorderLayout.NORTH);

		jPanelBoutons = new JPanel();
		 gridLayout = new GridLayout(3, 4);
		gridLayout.setHgap(2);
		gridLayout.setVgap(2);
		jPanelBoutons.setLayout(gridLayout);
		String[] boutons = { "7", "8", "9", "4", "5", "6", "1", "2", "3" };
		for (int i = 0; i < boutons.length; i++) {
			jPanelBoutons.add(new CreationJButton("" + boutons[i], "", actionListener));

		}
		getContentPane().add(jPanelBoutons, BorderLayout.WEST);

		jPanelBoutonsZero = new JPanel();
		 gridLayout = new GridLayout(2, 1);
			gridLayout.setHgap(10);
			gridLayout.setVgap(40);
		jPanelBoutonsZero.setLayout(gridLayout);
		
		jPanelBoutonsZero.add(new CreationJButton("0", "", actionListener));

		jPanelBoutonsZero.add(new CreationJButton("=", "", actionListener));
		
		getContentPane().add(jPanelBoutonsZero, BorderLayout.CENTER);

		jPanelBoutonsOperateur = new JPanel();
		jPanelBoutonsOperateur.setLayout(new GridLayout(4, 1));
		String[] operateur = { "+", "-", "*", "/" };
		for (int i = 0; i < operateur.length; i++) {
			jPanelBoutonsOperateur.add(new CreationJButton(operateur[i], "", actionListener));
		}
		getContentPane().add(jPanelBoutonsOperateur, BorderLayout.EAST);

	}

	public void calcul() {
		jTextFieldCalcul.setText(actionListener.getCalcul());
		jTextFieldEcran.setBackground(Color.WHITE);
		if(!"".equals(actionListener.getMessage())){
			jTextFieldEcran.setBackground(Color.RED);
			jTextFieldEcran.setText(actionListener.getMessage());
		}else if (!"".equals(actionListener.getResultat())) {
			jTextFieldEcran.setText(actionListener.getResultat());
		} else if ("".equals(actionListener.getNb2())) {
			jTextFieldEcran.setText(actionListener.getNb1());
		} else {
			jTextFieldEcran.setText(actionListener.getNb2());
		}
	}

	public void run() {
		while (true) {
			calcul();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Observable o, Object arg) {
		calcul();
	}

}
