package fr.afpa.main;

import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Main {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        String MESSAGE = in.nextLine();
        String mesCode="";
        String ajout="";
        int indiceDeb = 0;
        int indiceFin=0;
        int nbtour=0;
        if(N<0){
        for(int i=0;i<Math.abs(N);i++){
            nbtour=0;
             indiceDeb = 0;
         indiceFin=0;
         mesCode="";
        while(indiceDeb<MESSAGE.length()){
            nbtour++;
            indiceFin=(indiceDeb+nbtour>=MESSAGE.length())?MESSAGE.length():indiceDeb+nbtour;
            ajout=MESSAGE.substring(indiceDeb,indiceFin);
            if(nbtour % 2 == 1){
                mesCode+=ajout;
            }else{
                mesCode=ajout+mesCode;
            }
            indiceDeb=indiceFin;
        } 
        MESSAGE=mesCode;
        }
        }else{
             for(int i=0;i<Math.abs(N);i++){
            nbtour=0;
             indiceDeb = 0;
         indiceFin=0;
         mesCode="";
        while(MESSAGE.length()>0){
            nbtour++;
           
            if(nbtour % 2 == 1){
                indiceDeb=(MESSAGE.length()%2==0)?MESSAGE.length()/2:MESSAGE.length()/2+1;
                indiceFin=(indiceDeb+nbtour>MESSAGE.length())?MESSAGE.length():indiceDeb+nbtour;
                ajout=MESSAGE.substring(indiceDeb,indiceFin);
                MESSAGE=MESSAGE.substring(0,indiceDeb)+MESSAGE.substring(indiceFin);
                System.out.println("ajou " + ajout);
                System.out.println("mess " + MESSAGE);
                mesCode+=ajout;
            }else{
            	if(nbtour>MESSAGE.length()) {
            		ajout=MESSAGE;
            		MESSAGE="";
            	}else {
            	indiceDeb=(MESSAGE.length()%2==1)?MESSAGE.length()/2:MESSAGE.length()/2-1;
                indiceFin=(indiceDeb+nbtour>MESSAGE.length())?MESSAGE.length():indiceDeb+nbtour;
                ajout=MESSAGE.substring(indiceDeb,indiceFin);
                MESSAGE=MESSAGE.substring(0,indiceDeb)+MESSAGE.substring(indiceFin);
            	}
                System.out.println("ajou2 " + ajout);
                System.out.println("mess2 " + MESSAGE);
                mesCode+=ajout;
            }
        } 
        System.out.println();
        MESSAGE=mesCode;
        }
        }
        /*
         1
        ghibcadef
        */
        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(mesCode);
    }
}