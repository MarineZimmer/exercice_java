package fr.afpa.objets;

public class Rejex {
	private static String rejexIban = "FR\\d{25}";

	private static String rejexIpv4 = "(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d{0,1})\\."
			+ "((25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]?\\d)\\.){2}" + "(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d{0,1})";
	
	/*String p255="(25[0-5]|2[0-4]\\\\d|1\\\\d{2}|[1-9]\\\\d|[1-9])";
    String pt="\\.";
    String p2550="(25[0-5]|2[0-4]\\\\d|1\\\\d{2}|[1-9]\\\\d|\\\\d)";
    String patternIp= "("+p255+pt+"("+p2550+pt+")"+"{2}"+p255+")";
    
    
    String patternIp2="(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d|[1-9])\\."
            + "((25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d|\\d)\\.){2}"
            + "(25[0-5]|2[0-4]\\d|1\\d{2}|[1-9]\\d|[1-9])";*/

	private static String regexMailPartie1 = "\\w(\\.?[\\w-]+)*";
	private static String regexMailPartie2 = "[\\w&&[^_]]{3,13}";
	private static String regexMailPartie3 = "[\\w&&[^_]]{2,3}";

	private static String rejexMail = "^" + regexMailPartie1 + "@" + regexMailPartie2 + "\\." + regexMailPartie3 + "$";

	private static String annee = "([1-9]\\d{3}|\\d[1-9]\\d{2}|\\d{2}[1-9]\\d|000[1-9])";
	private static String mois31 = "(0[13578]|1[02])";
	private static String mois30 = "(0[469]|11)";
	private static String mois28 = "02";
	private static String jour31 = "(0[1-9]|[12]\\d|3[01])";
	private static String jour30 = "(0[1-9]|[12]\\d|30)";
	private static String jour28 = "(0[1-9]|[1]\\d|2[0-8])";
	private static String regexDate2 = "((" + jour31 + "\\\\" + mois31 + ")|" + "(" + jour30 + "\\\\" + mois30 + ")|"
			+ "(" + jour28 + "\\\\" + mois28 + "))" + "\\\\" + annee;
}
