
package objets;

import java.util.Scanner;

public class Banque {
	private Agence[] listeAgence;
	private int nbAgence;

	/**
	 * Constructeur de la Banque, initialisation de la taille de la liste agences �
	 * 20, nombre d'agences de la banque � 0
	 */
	public Banque() {
		listeAgence = new Agence[20];
		nbAgence = 0;
	}

	/**
	 * modifie le tableau d'agence
	 * 
	 * @param agence1 : un tableau d'agence contenant le nouveau tableau d'agence
	 */
	public void setListeAgence(Agence[] agence1) {
		listeAgence = agence1;
	}

	/**
	 * retourne le tableau d'agence
	 * 
	 * @return le tableau des agences de la b nque
	 */
	public Agence[] getListeAgence() {
		return listeAgence;
	}

	/**
	 * modifie le nombre d'agence
	 * 
	 * @param nbAgence1 : un entier repr�sentant le nouveau nombre d'agence
	 */
	public void setNbAgence(int nbAgence1) {
		nbAgence = nbAgence1;
	}

	/**
	 * retourne le nombre d'agence de la banque
	 * 
	 * @return un entier epr�sentant le nombre d'agence
	 */
	public int getNbAgence() {
		return nbAgence;

	}

	/**
	 * methode qui cr�e une nouvelle agence
	 * 
	 * @param in : le scanner
	 */
	public void creerAgence(Scanner in) {
		String codeAgence = "";
		String nomAgence;
		String adresseAgence;

		if (nbAgence > listeAgence.length - 1) {
			System.out.println("D�sol�, vous avez atteint le nombre maximum d'agences dans votre banque");
			return;
		}

		System.out.println("Entrer le code de l'agence (3 chiffres)");
		codeAgence = in.nextLine();
		// controle si le code agence est du format demand�(3 chiffres)
		if (!Controle.isNumerique(codeAgence, 3)) {
			System.out.println("D�sol�, le code agence entr� n'est pas du format demand� (3 chiffres)");
			return;
		}

		// controle si le code agence n'est pas d�j� utilis�
		if (!Controle.isUnique(codeAgence, listeCodeAgence())) {
			System.out.println("D�sol�, le code agence entr� existe d�j� dans la banque");
			return;
		}

		System.out.println("Entrer le nom de l'agence");
		nomAgence = in.nextLine();
		// controle si le nom est non vide, retour au menu si le nom est vide
		if (!Controle.isNonVide(nomAgence)) {
			System.out.println("D�sol�, le nom entr� est vide");
			return;
		}

		System.out.println("Entrer l'adresse de l'agence");
		adresseAgence = in.nextLine();
		// controle si l'adresse est non vide, retour au menu si nom vide
		if (!Controle.isNonVide(adresseAgence)) {
			System.out.println("D�sol�, l'adresse entr� est vide");
			return;
		}

		// creation de l'agence et ajout a la liste des agences de la banque
		Agence agence = new Agence(codeAgence, nomAgence, adresseAgence);
		listeAgence[nbAgence] = agence;
		nbAgence++;
	}

	/**
	 * Methode qui affiche la liste des agences de la banque
	 */
	public void afficherListeAgence() {
		for (int i = 0; i < nbAgence; i++) {
			System.out.println(listeAgence[i]);
		}
	}

	/**
	 * retourne la liste des identifiants clients de la banque(pour la verification
	 * que l'identifiant est unique)
	 * 
	 * @return un tableau de chaine de caractere repr�sentant les identifiants
	 *         existants
	 */
	public String[] listeIdentifiantClient() {
		int j = 0;
		int tailleListe = 0;
		for (int a = 0; a < nbAgence; a++) {
			for (int i = 0; i < listeAgence[a].getNbClient(); i++) {
				tailleListe++;
			}
		}
		String[] listeIdentifiantExistant = new String[tailleListe];
		for (int a = 0; a < nbAgence; a++) {
			for (int i = 0; i < listeAgence[a].getNbClient(); i++) {
				listeIdentifiantExistant[j] = listeAgence[a].getListeClient()[i].getIdentifiant();
				j++;
			}
		}
		return listeIdentifiantExistant;
	}

	/**
	 * retourne la liste des codes agences de la banque(pour la verification que le
	 * code agence est unique)
	 * 
	 * @return un tableau de chaine de caract�res repr�sentant les codes agences
	 *         existants
	 */
	public String[] listeCodeAgence() {
		String[] listeCodeAgenceExistant = new String[nbAgence];
		for (int i = 0; i < nbAgence; i++) {
			listeCodeAgenceExistant[i] = listeAgence[i].getCodeAgence();
		}
		return listeCodeAgenceExistant;
	}

	/**
	 * retourne la liste des numeros de comptes de la banque(pour la verification
	 * que le num�ro de compte est unique est unique)
	 * 
	 * @return un tableau de chaine de caract�res repr�sentant les num�ros de
	 *         comptes existants
	 */
	public String[] listeNumeroCompte() {
		int j = 0;
		int tailleListe = 0;
		for (int a = 0; a < nbAgence; a++) {
			for (int i = 0; i < listeAgence[a].getNbCompte(); i++) {
				tailleListe++;
			}
		}
		String[] listeCompteExistant = new String[tailleListe];
		for (int a = 0; a < nbAgence; a++) {
			for (int i = 0; i < listeAgence[a].getNbCompte(); i++) {
				listeCompteExistant[j] = listeAgence[a].getListeCompteBancaire()[i].getNumeroCompte();
				j++;
			}
		}
		return listeCompteExistant;
	}
}
