package objets;

import java.util.Scanner;


public class Agence {

	private String codeAgence;
	private String nomAgence;
	private String adresse;
	private CompteBancaire[] listeCompteBancaire;
	private int nbCompte;
	private Client[] listeClient;
	private int nbClient;

	/**
	 * constructeur de l'agence, initialisation de la taille du tableau des clients
	 * et des comptes bancaires � 20 et du nombre de clients et de comptes bancaires
	 * � 0
	 * 
	 * @param codeAgence1 : entier representant le code l'agence(3 chiffres)
	 * @param nomAgence1  : chaine de caract�res repr�sentant le nom de l'agence
	 * @param adresse1    : chaine de caract�res repr�sentant l'adresse de l'agence
	 */
	public Agence(String codeAgence1, String nomAgence1, String adresse1) {
		codeAgence = codeAgence1;
		nomAgence = nomAgence1;
		adresse = adresse1;
		nbCompte = 0;
		listeCompteBancaire = new CompteBancaire[20];
		nbClient = 0;
		listeClient = new Client[20];

	}

	/**
	 * retour du nom de l'agence
	 * 
	 * @return nom : une chaine de caract�res repr�sentant le nom de l'agence
	 */
	public String getNomAgence() {
		return nomAgence;
	}

	/**
	 * modifie le nom de l'agence
	 * 
	 * @param nomAgence1 : une chaine de caract�res repr�sentant le nouveau nom de
	 *                   l'agence
	 */
	public void setNomAgence(String nomAgence1) {
		nomAgence = nomAgence1;
	}

	/**
	 * retourne le code de l'agence
	 * 
	 * @return chaine de caract�res repr�sentant le nom de l'agence
	 */
	public String getCodeAgence() {
		return codeAgence;
	}

	/**
	 * modifie le code de l'agence
	 * 
	 * @param codeAgence1 : chaine de caract�res repr�sentant le nouveau code l'agence
	 */
	public void setCodeAgence(String codeAgence1) {
		codeAgence = codeAgence1;
	}

	/**
	 * retourne l'adresse de l'agence
	 * 
	 * @return : chaine de caract�res repr�sentant l'adresse de l'agence
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * modifie l'adresse de l'agence
	 * 
	 * @param adresse1 : une chaine de caract�res repr�sentant la nouvelle adresse
	 *                 de l'agence
	 */
	public void setAdresse(String adresse1) {
		adresse = adresse1;
	}

	/**
	 * retourne la listes des comptes bancaires
	 * 
	 * @return : un tableau repr�sentant les comptes bancaires de l'agence
	 */
	public CompteBancaire[] getListeCompteBancaire() {
		return listeCompteBancaire;
	}

	/**
	 * modifie la liste des comptes bancaires
	 * 
	 * @param compteBancaire1 :  un tableau des nouveaux Comptes bancaires
	 */
	public void setListeCompteBancaire(CompteBancaire[] compteBancaire1) {
		listeCompteBancaire = compteBancaire1;
	}

	/**
	 * retourne le nombre de compte de l'agence
	 * 
	 * @return : un entier representant le nombre de comptes de l'agence
	 */
	public int getNbCompte() {
		return nbCompte;
	}

	/**
	 * modifie le nombre de comptes de l'agence
	 * 
	 * @param nbCompte1 : entier repr�sentant le nouveau nombre de compte
	 */
	public void setNbCompte(int nbCompte1) {
		nbCompte = nbCompte1;
	}

	/**
	 * retourne la liste des clients de l'agnece
	 * 
	 * @return : un tableau de Client de l'agence
	 */
	public Client[] getListeClient() {
		return listeClient;
	}

	/**
	 * modifie la liste des clients de l'agence
	 * 
	 * @param listeClient1 : un tableau de Client repr�sentant la nouvelle liste de
	 *                     client de l'agence
	 */
	public void setListeClient(Client[] listeClient1) {
		listeClient = listeClient1;
	}

	/**
	 * retourne le nombre de clients de l'agence
	 * 
	 * @return : un entier repr�sentant le nombre de clients de l'agence
	 */
	public int getNbClient() {
		return nbClient;
	}

	/**
	 * modifie le nombre de clients de la banque
	 * 
	 * @param nbClient1 : un entier repr�sentant le nombre de client de l'agence
	 */
	public void setNbClient(int nbClient1) {
		nbClient = nbClient1;
	}

	/**
	 * retourne une chaine de carat�res repr�sentant l'Agence
	 */
	@Override
	public String toString() {
		return "Agence [codeAgence=" + codeAgence + ", nomAgence=" + nomAgence + ", adresse=" + adresse + "]";
	}

	/**
	 * Cr�ation d'un nouveau client dans l'agence, si les champs entr�s par
	 * l'utilisateur sont non valide retour au menu principal sans cr�ation d'un
	 * nouveau client. Si le nombre max de client atteint retour au menu.
	 * 
	 * @param in : le scanner
	 */
	public void creerClient(Scanner in, String[] listeIdentifiantExistant) {
		String identifiant = "";
		String nom = "";
		String prenom = "";
		String dateNaissance = "";
		String mail = "";

		// verification si le nombre maximum de clients est atteint
		if (nbClient > listeClient.length - 1) {
			System.out.println("D�sol�, vous avez atteint le nombre maximum de clients dans cette agence");
			return;
		}

		System.out.println("Entrer l'identifiant du client (2 lettres majuscules + 6 chiffres)");
		identifiant = in.nextLine();
		// controle si l'identifiant est valide, retour au menu si identifiant non
		// valide
		if (!Controle.isAlphaNumerique(identifiant, 8)) {
			System.out.println(
					"D�sol�, l'identifiant entr� n'est pas du format demand� (2 lettres majuscules + 6 chiffres)");
			return;
		}

		// controle si l'identifiant n'est pas d�j� utilis�
		if (!Controle.isUnique(identifiant, listeIdentifiantExistant)) {
			System.out.println("D�sol�, l'identifiant entr� existe d�j� dans la banque");
			return;
		}

		System.out.println("Entrer le nom du client");
		nom = in.nextLine();
		// controle si le nom est non vide, retour au menu si nom vide
		if (!Controle.isNonVide(nom)) {
			System.out.println("D�sol�, le nom entr� est vide");
			return;
		}

		System.out.println("Entrer le pr�nom du client");
		prenom = in.nextLine();
		// controle si le prenom est non vide, retour au menu si prenom vide
		if (!Controle.isNonVide(prenom)) {
			System.out.println("D�sol�, le pr�nom entr� est vide");
			return;
		}

		System.out.println("Entrer la date de naissance du client");
		dateNaissance = in.nextLine();
		// controle si la date de naissance est valide
		if (!Controle.isDate(dateNaissance)) {
			System.out.println("D�sol�, la date de naissance est non valide");
			return;
		}

		System.out.println("Entrer le mail du client");
		mail = in.nextLine();
		// controle si le mail est non vide, retour au menu si mail vide
		if (!Controle.isNonVide(mail)) {
			System.out.println("D�sol�, le mail entr� est vide");
			return;
		}

		// cr�ation du nouveau client, et ajout � la liste client de l'agence
		Client client = new Client(identifiant, nom, prenom, dateNaissance, mail);
		listeClient[nbClient] = client;
		nbClient++;
	}

	/**
	 * cr�ation d'un compte bancaire
	 * 
	 * @param client                    : le client du compte � cr�er
	 * @param in                        : le scanner
	 * @param listeNumeroCompteExistant : la liste des num�ros de comptes existants
	 */
	public void creerCompteBnacaire(Client client, Scanner in, String[] listeNumeroCompteExistant) {
		String numeroCompte = "";
		boolean decouvert = false;
		String solde;
		String reponseDecouvert;

		System.out.println("Entrer le num�ro du compte bancaire (11 chiffres)");
		numeroCompte = in.nextLine();
		// controle si le num�ro de compte est valide, retour au menu sinon
		// valide
		if (!Controle.isNumerique(numeroCompte, 11)) {
			System.out.println("D�sol�, le num�ro de compte entr� n'est pas du format demand� (11 chiffres)");
			return;
		}

		// controle si le numero n'est pas d�j� utilis�
		if (!Controle.isUnique(numeroCompte, listeNumeroCompteExistant)) {
			System.out.println("D�sol�, le num�ro entr� existe d�j� dans la banque");
			return;
		}

		System.out.println("Decouvert autoris� (o/n)");
		reponseDecouvert = in.nextLine();
		// controle si la reponse entr�e est 'o' ou 'n'
		if (!Controle.isOuiOuNon(reponseDecouvert)) {
			System.out.println("desol�, vous n'avez pas entr� une reponse correcte('o' :  oui, 'n' : non");
			return;
		}
		if (reponseDecouvert.charAt(0) == 'o') {
			decouvert = true;
		}

		System.out.println("Entrez le montant de votre fortune(nombre)");
		solde = in.nextLine();
		// controle si la reponse entr�e est un nombre
		if (!Controle.isDouble(solde)) {
			System.out.println("D�sol� vous n'avez pas entr� un nombre");
			return;
		}

		// cr�ation du compte et ajout a la liste des comptes bancaire de l'agence
		CompteBancaire compteBancaire = new CompteBancaire(numeroCompte, codeAgence, client, decouvert,
				Double.parseDouble(solde));
		listeCompteBancaire[nbCompte] = compteBancaire;
		nbCompte++;
	}

	/**
	 * Affiche la liste des clients de l'agence
	 */
	public void afficherListeClient() {
		for (int i = 0; i < nbClient; i++) {
			System.out.println(listeClient[i]);
		}
	}
}
