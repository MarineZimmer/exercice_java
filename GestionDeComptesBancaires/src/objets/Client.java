package objets;

public class Client {

	private String identifiant;
	private String nom;
	private String prenom;
	private String dateNaissance;
	private String mail;

	/**
	 * constructeur du Client
	 * 
	 * @param identifiant1   : chaine de caractères représentant l'identifiant du
	 *                       client(2 lettres majuscules + 6 chiffres)
	 * @param nom1           : chaine de caractères représentant le nom du client
	 * @param prenom1        : haine de caractères représentant le prénom du client
	 * @param dateNaissance1 : chaine de caractères représentant la date de
	 *                       naissance du client
	 * @param mail1          : chaine de caractères représentant le mail du client
	 */
	public Client(String identifiant1, String nom1, String prenom1, String dateNaissance1, String mail1) {
		identifiant = identifiant1;
		nom = nom1;
		prenom = prenom1;
		dateNaissance = dateNaissance1;
		mail = mail1;

	}

	/**
	 * retourne l'identifiant du client
	 * 
	 * @return identifiant : chaine de caractères représentant l'identifiant du
	 *         client
	 */
	public String getIdentifiant() {
		return identifiant;
	}

	/**
	 * modifie le nom du client
	 * 
	 * @param identifiant1 :chaine de caractères représentant le nouveau identifiant
	 *                     du client
	 */
	public void setIdentifiant(String identifiant1) {
		identifiant = identifiant1;
	}

	/**
	 * retourne le nom du client
	 * 
	 * @return nom : chaine de caractères représentant le nom du client
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * modifie le prenom du client
	 * 
	 * @param nom1 :chaine de caractères représentant le nouveau nom du client
	 */
	public void setNom(String nom1) {
		nom = nom1;
	}

	/**
	 * retourne le prenom du client
	 * 
	 * @return prenom : chaine de caractères représentant le prenom du client
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * modifie le prenom du client
	 * 
	 * @param prenom1 : chaine de caractères représentant le nouveau prenom du
	 *                client
	 */
	public void setPrenom(String prenom1) {
		prenom = prenom1;
	}

	/**
	 * retourne la date de naissance du client
	 * 
	 * @return date de naissance : chaine de caractères représentant la date de
	 *         naissance du client
	 */
	public String getDateNaissance() {
		return dateNaissance;
	}

	/**
	 * modifie la date de naissance du client
	 * 
	 * @param dateNaissance1 : chaine de caractères représentant la nouvelle date de
	 *                       naissance du client
	 */
	public void setDateNaissance(String dateNaissance1) {
		dateNaissance = dateNaissance1;
	}

	/**
	 * retourne le mail du client
	 * 
	 * @return mail : chaine de caractères représentant le mail du client
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * modifie l'adresse mail du client
	 * 
	 * @param mail1 : chaine de caractères représentant la nouvelle adresse mail du
	 *              client
	 */
	public void setMail(String mail1) {
		mail = mail1;
	}

	/**
	 * retourne une chaine de caractère representant un client
	 */
	@Override
	public String toString() {
		return "Client [identifiant=" + identifiant + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance="
				+ dateNaissance + ", mail=" + mail + "]";
	}

}
