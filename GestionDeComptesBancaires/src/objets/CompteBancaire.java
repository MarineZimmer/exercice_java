package objets;

public class CompteBancaire {
	private String numeroCompte;
	private String codeAgence;
	private Client client;
	private boolean decouvert;
	private double solde;

	/**
	 * constructeur du compte bancaire
	 * 
	 * @param numeroCompte1
	 * @param codeAgence1
	 * @param client1
	 * @param decouvert1
	 * @param solde1
	 */
	public CompteBancaire(String numeroCompte1, String codeAgence1, Client client1, boolean decouvert1, double solde1) {

		numeroCompte = numeroCompte1;
		codeAgence = codeAgence1;
		client = client1;
		decouvert = decouvert1;
		solde = solde1;
	}

	/**
	 * retourne le num�ro de compte(11 chiffres)
	 * 
	 * @return : une chaine de carat�res repr�sentant le numero de compte
	 */
	public String getNumeroCompte() {
		return numeroCompte;
	}

	/**
	 * modifie le num�ro de compte
	 * 
	 * @param numeroCompte1 : chaine de caract�res repr�sentant le nouveau num�ro de
	 *                      compte
	 */
	public void setNumeroCompte(String numeroCompte1) {

		numeroCompte = numeroCompte1;
	}

	/**
	 * retourne le code de l'agence o� se trouve le compte bancaire
	 * 
	 * @return : une chaine de caract�re repr�sentant le code de l'agence du compte
	 */
	public String getCodeAgence() {
		return codeAgence;

	}

	/**
	 * modifie le code de l'agence du compte
	 * 
	 * @param codeAgence1 : chaine de caract�res repr�sentant le nouveau code agence du compte
	 */
	public void setCodeAgence(String codeAgence1) {
		codeAgence = codeAgence1;
	}

	/**
	 * retourne le Client du compte
	 * 
	 * @return client : un objet Client repr�sentant le Client du compte
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * modifie le client du compte
	 * 
	 * @param client1 : un objet Client repr�sentant le nouveau Client du compte
	 */
	public void setClient(Client client1) {
		client = client1;
	}

	/**
	 * retourne si le decouvert est autoris� ou non
	 * 
	 * @return decouvert : un boolean repr�sentant si le d�couvert est autoris� ou
	 *         non
	 */
	public boolean isDecouvert() {
		return decouvert;
	}

	/**
	 * modifie l'autorisation de d�couvert du compte
	 * 
	 * @param decouvert1 : un boolean repr�sentant si la nouvelle autoridation de
	 *                   d�couvert
	 */
	public void setDecouvert(boolean decouvert1) {
		decouvert = decouvert1;

	}

	/**
	 * retourne le solde du compte
	 * 
	 * @return solde : un double repr�sentant le solde du compte
	 */
	public double getSolde() {
		return solde;
	}

	/**
	 * modifie le solde du compte
	 * 
	 * @param solde1 : un double repr�sentant le nouveau solde du compte
	 */
	public void setSolde(double solde1) {
		solde = solde1;
	}

	/**
	 * retourne une chaine de caract�re repr�sentant le compte
	 */
	@Override
	public String toString() {
		return "CompteBancaire [numeroCompte=" + numeroCompte + ", codeAgence=" + codeAgence + ", client="
				+ client.getIdentifiant() + ", decouvert=" + decouvert + ", solde=" + solde + "]";
	}
}
