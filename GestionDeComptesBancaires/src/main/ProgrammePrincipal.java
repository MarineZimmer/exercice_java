package main;

import java.util.Scanner;

import objets.Agence;
import objets.Banque;
import objets.Client;
import objets.CompteBancaire;
import objets.Controle;

public class ProgrammePrincipal {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in); // ici on d�clare un scanner unique pour tout le programme
		Banque banqueCda = new Banque();
		affichageMenu();
		while (choixMenu(banqueCda, in)) { // boucle pour afficher le menu jusqu'au choix 8
			affichageMenu();
		}
	}

	/**
	 * service qui affiche le menu
	 */
	static void affichageMenu() {
		System.out.println("--------------------------------------------------------------------");
		System.out.println("Menu :");
		System.out.println("1-Cr�er une agence");
		System.out.println("2-Cr�er un client");
		System.out.println("3-Cr�er un compte bancaire");
		System.out.println("4-Recherche de compte (num�ro de compte)");
		System.out.println("5-Recherche de client(Nom, Num�rode compte, identifiant de client)");
		System.out.println("6-Afficher la liste des comptes d�un client (identifiant client)");
		System.out.println("7-Imprimer les infos client (identifiant client)");
		System.out.println("8-Quitter le programme");
		System.out.println("--------------------------------------------------------------------");

	}

	/**
	 * service qui traite le choix de l'utilisateur
	 * 
	 * @param banque : Banque
	 * @param in     : Scanner
	 * @return true si le choix de l'utisateur est diff�rent de 8, false si le choix
	 *         de l'utisateur est de quitter
	 */
	static boolean choixMenu(Banque banque, Scanner in) {

		String choix;
		System.out.println("Quelle est votre choix ?");
		choix = in.nextLine();
		switch (choix) { // choix de l'utilisateur (autre que 8) qui renvoi sur d'autre methode
		case "1":
			banque.creerAgence(in);
			break;
		case "2":
			creerClientAgence(banque, in);
			break;
		case "3":
			creerCompteBancaire(banque, in);
			break;
		case "4":
			rechercheCompteClient(banque, in);
			break;
		case "5":
			rechercheClient(banque, in);
			break;
		case "6":
			affichageListeCompteClient(banque, in);
			break;
		case "7":
			imprimerInfosClient(banque, in);
			break;
		case "8":
			return false;

		default:
			System.out.println("Votre choix est incorrect, veuillez saisir un chiffre entre 1 et 8");
			break;

		}
		return true;

	}

	/**
	 * Creation d'un nouveau client dans une agence (si l'agence n'existe pas,
	 * l'identifiant d�j� utilis� ou saisie �ronn�e, retour au menu principal)
	 * 
	 * @param banque : la banque
	 * @param in     : le scannner
	 */
	static void creerClientAgence(Banque banque, Scanner in) {
		Agence agence = null;
		String codeAgence;
		if (banque.getNbAgence() == 0) {
			System.out.println(
					"D�sol� il n'y pas encore d'agence dans votre banque, veuillez cr�er une agence avant de cr�er un client");
			return;
		}
		banque.afficherListeAgence();
		System.out.println("Veuillez entrer le code agence dans laquelle vous voullez cr�er un nouveau client");
		codeAgence = in.nextLine();
		// si le code est unique, c'est que le code de l'agence entr� ne fait pas partit
		// des codes agences de la banque
		if (Controle.isUnique(codeAgence, banque.listeCodeAgence())) {
			System.out.println("D�sol�, cette agence n'existe pas.");
			return;
		}

		// parcours de la liste des agences de la banque pour chercher l'agence dans
		// laquelle on cr�e le nouveau client
		for (int i = 0; i < banque.getNbAgence(); i++) {
			if ((banque.getListeAgence()[i].getCodeAgence()).equals(codeAgence)) {
				agence = banque.getListeAgence()[i];
				break;
			}
		}

		// cr�ation du nouveau client dans l'agence souhait�e
		agence.creerClient(in, banque.listeIdentifiantClient());
	}

	/**
	 * cr�ation d'un compte Bancaire d'un client dans une agence (si l'agence
	 * n'existe pas, le client n'existe pas, le num�ro de compte d�j� utilis� ou
	 * saisie �ronn�e, retour au menu principal)
	 * 
	 * @param banque : la banque
	 * @param in     : le scanner
	 */

	static void creerCompteBancaire(Banque banque, Scanner in) {

		Agence agence = null;
		String codeAgence;

		// s'il n'y a pas d'agence, impossible de cr�er un compte. retour au menu
		if (banque.getNbAgence() == 0) {
			System.out.println("D�sol�, impossible d'ouvrir un compte sans agence");
			return;
		}

		// affichage de la liste des agences, pour choisir dans quelle agence le compte
		// est cr�� (via le code agence)
		banque.afficherListeAgence();
		System.out.println("Veuillez entrer le code de l'agence");
		codeAgence = in.nextLine();

		// si le code agence est unique, c'est que l'agence n'existe pas
		if (Controle.isUnique(codeAgence, banque.listeCodeAgence())) {
			System.out.println(
					"D�sol�, cette agence n'existe pas. Passez par la cr�ation de l'agence avant de cr�er le compte");
			return;
		}

		// recup�ration de l'agence dans laquelle le compte est cr��
		for (int i = 0; i < banque.getNbAgence(); i++) {
			if ((banque.getListeAgence()[i].getCodeAgence()).equals(codeAgence)) {
				agence = banque.getListeAgence()[i];
				break;
			}
		}

		Client client = null;
		String identifiant;
		// si il n'y a pas de clients dans l'agence, impossible d'ouvrir un compte
		if (agence.getNbClient() == 0) {
			System.out.println(
					"D�sol� il n'y pas de client dans l'agence. Cr�er d'abord un client dans cette agence avant de cr�er un compte");
			return;
		}

		// affichage de la liste des clients de l'agence, pour choisir dans le client du
		// compte � cr�er (via l'identifiant)
		agence.afficherListeClient();
		System.out.println("Veuillez entrer l'identifiant du client");
		identifiant = in.nextLine();

		// si l'identifiant est unique, c'est que le client n'existe pas
		if (Controle.isUnique(identifiant, banque.listeIdentifiantClient())) {
			System.out.println(
					"D�sol�, ce client n'existe pas. Passez par la cr�ation du client avant de cr�er le compte");
			return;
		}

		// recup�ration du client du compte bancaire � cr�er
		for (int i = 0; i < agence.getNbClient(); i++) {
			if (agence.getListeClient()[i].getIdentifiant().equals(identifiant)) {
				client = agence.getListeClient()[i];
				break;
			}
		}

		// controle si le client est pr�sent dans cette agence
		if (client == null) {
			System.out.println("D�sol�, ce client n'est pas dans cette agence.");
			return;
		}

		// si le client a d�j� 3 comptes, impossible de cr�er un nouveau compte
		if (ListeCompteClient(agence, client)[2] == null) {
			agence.creerCompteBnacaire(client, in, banque.listeNumeroCompte());
		} else {
			System.out.println("Le client a deja 3 comptes dans l'agence, impossible d'ouvrir un nouveau compte");
		}

	}

	/**
	 * retourne les comptes bancaires d'un client
	 * 
	 * @param agence : agence du client
	 * @param client : le client
	 * @return un tableau des comptes du client
	 */
	static CompteBancaire[] ListeCompteClient(Agence agence, Client client) {
		CompteBancaire[] comptesClient = new CompteBancaire[3];
		int nbComptes = 0;
		for (int i = 0; i < agence.getNbCompte(); i++) {
			if (agence.getListeCompteBancaire()[i].getClient().equals(client)) {
				comptesClient[nbComptes] = agence.getListeCompteBancaire()[i];
				nbComptes++;
			}
		}
		return comptesClient;
	}

	/**
	 * recherche d'un compte bancaire via le num�ro de compte
	 * 
	 * @param banque : la banque
	 * @param in     : le scanner
	 */
	static void rechercheCompteClient(Banque banque, Scanner in) {
		CompteBancaire compteBancaire;
		Agence agence;
		String numCompte = "";
		System.out.println("Entrer le numero de compte rechercher");
		numCompte = in.nextLine();

		// si le num�ro de compte rentr� est unique c'est que le compte bancaire
		// n'existe
		// pas
		if (Controle.isUnique(numCompte, banque.listeNumeroCompte())) {
			System.out.println("D�sol� ce compte bancaire n'existe pas");
			return;
		}

		// recherche du compte bancaire dans les agences de la banque
		for (int i = 0; i < banque.getNbAgence(); i++) {
			agence = banque.getListeAgence()[i];
			for (int j = 0; j < agence.getNbCompte(); j++) {
				compteBancaire = agence.getListeCompteBancaire()[j];
				if (compteBancaire.getNumeroCompte().equals(numCompte)) {
					System.out.println(compteBancaire);
					return;
				}
			}
		}
	}

	/**
	 * Recherche d'un client (par nom, numero de compte ou identifiant
	 * 
	 * @param banque : la banque
	 * @param in     : le scanner
	 */
	static void rechercheClient(Banque banque, Scanner in) {
		System.out.println(
				"Comment voulez vous rechercher le client : 1 : par nom, 2 :par numero de compte, 3 : par identifiant ");
		switch (in.nextLine()) {

		case "1":
			rechercheClientNom(banque, in);
			break;

		case "2":
			rechercheClientCompte(banque, in);
			break;
		case "3":
			rechercheClientIdentifiant(banque, in);
			break;
		default:
			System.out.println(
					"Votre choix est incorrect, veuillez saisir un chiffre entre 1 et 3 pour la recherche du client");
		}
	}

	/**
	 * Recherche des clients via le nom du client (possibilit� d'afficher plusieurs
	 * clients s'ils ont le m�me nom)
	 * 
	 * @param banque : la banque
	 * @param in     : le scanner
	 */
	static void rechercheClientNom(Banque banque, Scanner in) {
		Client client;
		Agence agence;
		String nom;
		System.out.println("Le nom � chercher");
		nom = in.nextLine();
		boolean trouver = false;

		// recherche du nom du client parmi les clients de la banque (possibilit�
		// d'afficher plusieurs clients s'ils ont le m�me nom)
		for (int i = 0; i < banque.getNbAgence(); i++) {
			agence = banque.getListeAgence()[i];
			for (int j = 0; j < agence.getNbClient(); j++) {
				client = agence.getListeClient()[j];
				if (nom.equals(client.getNom())) {
					System.out.println(client);
					trouver = true;
				}
			}
		}
		// si aucun client n'a �t� trouv�
		if (!trouver) {
			System.out.println("D�sol�, aucun client a ce nom parmi les clients de la banque");
		}
	}

	/**
	 * Recherche du client via l'identifiant
	 * 
	 * @param banque : la banque
	 * @param in     : le scanner
	 */
	static void rechercheClientIdentifiant(Banque banque, Scanner in) {
		Client client;
		Agence agence;
		String identifiant;
		System.out.println("L'identifiant � chercher");
		identifiant = in.nextLine();

		// si l'idenfiant client est unique, c'est qu'il n'y a pas de client avec cet
		// identifiant au sein de la banque
		if (Controle.isUnique(identifiant, banque.listeIdentifiantClient())) {
			System.out.println("D�sol�, aucun client possede cet identifiant");
			return;
		}

		// recherche du client via l'identifiant
		for (int i = 0; i < banque.getNbAgence(); i++) {
			agence = banque.getListeAgence()[i];
			for (int j = 0; j < agence.getNbClient(); j++) {
				client = agence.getListeClient()[j];
				if (identifiant.equals(client.getIdentifiant())) {
					System.out.println(client);
					return;
				}
			}
		}

	}

	/**
	 * recherche d'un client via le numero de compte
	 * @param banque	: la banque  	
	 * @param in		: le scanner
	 */
	static void rechercheClientCompte(Banque banque, Scanner in) {
		CompteBancaire compteBancaire;
		Agence agence;
		String numeroCompte;
		System.out.println("Le numero de compte du client � chercher");
		numeroCompte = in.nextLine();

		// si le num�ro de compte est unique, c'est qu'il n'y a pas de compte avec ce
		// num�ro au sein de la banque
		if (Controle.isUnique(numeroCompte, banque.listeNumeroCompte())) {
			System.out.println("D�sol�, aucun compte possede ce num�ro");
			return;
		}

		// recherche du client via le num�ro de compte
		for (int i = 0; i < banque.getNbAgence(); i++) {
			agence = banque.getListeAgence()[i];
			for (int j = 0; j < agence.getNbCompte(); j++) {
				compteBancaire = agence.getListeCompteBancaire()[j];
				if (numeroCompte.equals(compteBancaire.getNumeroCompte())) {
					System.out.println(compteBancaire.getClient());
					return;
				}
			}
		}

	}

	/**
	 * Recherche de la liste des comptes d'un client via son identifiant
	 * 
	 * @param banque : la banque
	 * @param in     : le scanner
	 */
	static void affichageListeCompteClient(Banque banque, Scanner in) {
		String identifiant = "";
		System.out.println("Entrer l'identifiant du client � afficher ");
		identifiant = in.nextLine();
		// si l'idenfiant client est unique, c'est qu'il n'y a pas de client avec cet
		// identifiant au sein de la banque
		if (Controle.isUnique(identifiant, banque.listeIdentifiantClient())) {
			System.out.println("D�sol�, aucun client possede cet identifiant");
			return;
		}

		Agence agence;
		Client client;
		CompteBancaire[] listeCompteClient;

		// parcours des agences
		for (int i = 0; i < banque.getNbAgence(); i++) {
			agence = banque.getListeAgence()[i];
			// parcours des clients de l'agence
			for (int j = 0; j < agence.getNbClient(); j++) {
				client = agence.getListeClient()[j];
				// si le client a le bon identifiant on recup�re la liste de ses comptes et on
				// les affiche
				if (identifiant.equals(client.getIdentifiant())) {
					listeCompteClient = ListeCompteClient(agence, client);
					System.out.println(client);
					for (int k = 0; k < listeCompteClient.length; k++) {
						if (listeCompteClient[k] == null) {
							return;
						}
						System.out.println(listeCompteClient[k]);
					}
				}
			}
		}
	}

	/**
	 * Affichage infos client
	 * 
	 * @param banque : la banque
	 * @param in     : le scanner
	 */
	static void imprimerInfosClient(Banque banque, Scanner in) {
		String identifiant = "";
		System.out.println("Entrer l'identifiant du client � afficher ");
		identifiant = in.nextLine();
		Agence agence;
		Client client = null;
		CompteBancaire[] comptesClient = new CompteBancaire[3];

		// si l'idenfiant client est unique, c'est qu'il n'y pas de client avec cet
		// identifiant au sein de la banque
		if (Controle.isUnique(identifiant, banque.listeIdentifiantClient())) {
			System.out.println("D�sol�, aucun client possede cet identifiant");
			return;
		}

		// parcours des agences
		for (int i = 0; i < banque.getNbAgence(); i++) {
			agence = banque.getListeAgence()[i];
			// parcours des clients de chaque agence
			for (int j = 0; j < agence.getNbClient(); j++) {
				// on veririfie si l'identifiant du client de l'agence correspond au client
				// recherch�
				if (identifiant.equals(agence.getListeClient()[j].getIdentifiant())) {
					client = agence.getListeClient()[j];
					comptesClient = ListeCompteClient(agence, client);
					break;
				}
			}
		}
		// Affichage de la fiche client
		System.out.println("Fiche client");
		System.out.println("Num�ro client : " + client.getIdentifiant());
		System.out.println("Nom : " + client.getNom());
		System.out.println("Prenom : " + client.getPrenom());
		System.out.println("Date de naissance :" + client.getDateNaissance());
		System.out.println("____________________________________________________");
		System.out.println("Liste de compte");
		System.out.println("____________________________________________________");
		System.out.println("Numero de compte" + "          " + "solde");
		System.out.println("____________________________________________________");
		for (int i = 0; i < comptesClient.length; i++) {
			if (comptesClient[i] != null) {
				System.out.print(comptesClient[i].getNumeroCompte() + "                " + comptesClient[i].getSolde()
						+ "            ");
				if (comptesClient[i].getSolde() >= 0) {
					System.out.println(":-)");
				} else {
					System.out.println(":-(");
				}
			}
		}
	}

}
