package fr.afpa.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.Map;
import java.util.Map.Entry;

import fr.afpa.controle.ControleClient;
import fr.afpa.controle.ControleConseiller;
import fr.afpa.controle.ControleSaisie;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;
import fr.afpa.ihm.IhmClient;
import fr.afpa.ihm.IhmCompte;
import fr.afpa.ihm.IhmPersonne;

public class ConseillerService {

	private static final String OUI_NON = "Entrer oui ou non";
	private static final String PERSONNE_INEXISTANT = "Erreur, personne inexistante";

	/**
	 * Service qui cree un conseiller
	 * 
	 * @return le Conseiller cree
	 */
	public Conseiller creerConseiller() {
		IhmPersonne ihmPersonne = new IhmPersonne();
		String nom;
		String prenom;
		String dateNaissance;
		String mail;
		String motDePasse;
		System.out.println("---------------  Creation Conseiller  -------------------------");

		nom = ihmPersonne.entrerNom();
		prenom = ihmPersonne.entrerPrenom();
		dateNaissance = ihmPersonne.entrerDateNaissance();
		mail = ihmPersonne.entrerMail();

		Conseiller conseiller = new Conseiller(nom, prenom,
				LocalDate.parse(dateNaissance, DateTimeFormatter.ofPattern("dd/MM/yyyy")), mail);

		motDePasse = ihmPersonne.entrerMotDePasse();

		CryptageService cs = new CryptageService();
		FichierService fs = new FichierService();
		motDePasse = cs.encoder(motDePasse);
		fs.ecritureFichier("ressources//fichierAuthentification.txt",
				conseiller.getLogin() + ";" + conseiller.getLogin() + ";" + motDePasse, true);
		System.out.println("--------------------------------------------------------");
		return conseiller;
	}

	/**
	 * service de creation d'un client
	 * 
	 * @param conseiller : le conseiller qui cree le client
	 * @param banque     : la Banque
	 * @return true si la creation du Client a bien ete faites
	 */
	public boolean creationClient(Conseiller conseiller, Banque banque) {
		if (conseiller == null) {
			System.out.println(PERSONNE_INEXISTANT);
			return false;
		}
		ClientService cs = new ClientService();
		Client client = cs.creerClient();
		if (client != null) {
			conseiller.getPortefeuilleClient().put(client.getIdentifiant(), client);
			IhmClient ihmClient = new IhmClient();
			ihmClient.affichageInfoClient(client);
			System.out.println("Creation de compte pour le nouveau client");
			CompteService compteService = new CompteService();
			Compte compte = compteService.creerCompte(client, banque);
			if (compte != null) {
				client.getListeComptes().put(compte.getNumeroCompte(), compte);
				IhmCompte ihmCompte = new IhmCompte();
				ihmCompte.affichageInfoCompte(compte);
				return true;
			}
		}
		return false;
	}

	/**
	 * Service de recherche d'un client au sein du portefeuille du conseiller
	 * 
	 * @param conseiller      : le conseiller
	 * @param typeRecherche   : le type de recherche du client(identifiant(1),
	 *                        nom(2), numero de compte(3))
	 * @param valeurRecherche : la valeur recherche
	 * @return : le premier Client correspondant a la recherche
	 */
	public Client rechercheClient(Conseiller conseiller, String typeRecherche, String valeurRecherche) {
		if (conseiller == null) {
			System.out.println(PERSONNE_INEXISTANT);
			return null;
		}
		Client client = null;
		if ("1".equals(typeRecherche)) {
			client = conseiller.getPortefeuilleClient().get(valeurRecherche);
		} else {
			for (Map.Entry<String, Client> client2 : conseiller.getPortefeuilleClient().entrySet()) {
				if (("2".equals(typeRecherche) && client2.getValue().getNom().equals(valeurRecherche))
						|| ("3".equals(typeRecherche)
								&& client2.getValue().getListeComptes().containsKey(valeurRecherche))) {
					client = client2.getValue();
				}
			}
		}
		if (client != null) {
			return client;
		}
		return null;
	}

	/**
	 * Service de cr�ation d'un nouveau compte pour un client existant
	 * 
	 * @param conseiller : le Conseiller qui cree le compte
	 * @param banque     : La banque dans laquelle le compte est cree
	 * @return trur si la creation de compte a ete faites
	 */
	public boolean creationCompte(Conseiller conseiller, Banque banque) {

		Client client = choixClient(conseiller);
		if (client != null) {
			CompteService cs = new CompteService();
			Compte compte = cs.creerCompte(client, banque);
			if (compte != null) {
				client.getListeComptes().put(compte.getNumeroCompte(), compte);
				IhmCompte ihmCompte = new IhmCompte();
				ihmCompte.affichageInfoCompte(compte);
				return true;
			}

		}
		return false;
	}

	/**
	 * Service qui imprime un releve de compte d'un client du conseiller
	 * 
	 * @param conseiller le Conseiller
	 */
	public void imprimerReleveCompte(Conseiller conseiller) {
		Client client = choixClient(conseiller);
		if (client != null) {
			ClientService cs = new ClientService();
			cs.imprimerReleveCompte(client);
		}
	}

	/**
	 * Service de consultation des operations de l'ensemble des comptes d'un client
	 * 
	 * @param conseiller : le conseiller qui effectue la demande
	 */
	public void consulterOperationsCompte(Conseiller conseiller) {
		Client client = choixClient(conseiller);
		if (client != null) {
			ClientService cs = new ClientService();
			cs.consulterOperationsCompte(client);
		}
	}

	/**
	 * Service d'alimentation d'un compte d'un client
	 * 
	 * @param conseiller : le conseiller qui effectue la demande
	 * @return true si l'operation a bien ete faites, false sinon
	 */
	public boolean alimenterCompte(Conseiller conseiller) {
		Client client = choixClient(conseiller);
		if (client == null) {
			return false;
		} else {
			ClientService cs = new ClientService();
			return cs.alimenterCompte(client);
		}
	}

	/**
	 * Service de retrait d'argent d'un compte d'un client
	 * 
	 * @param conseiller : le conseiller qui effectue la demande
	 * @return true si l'operation a bien ete faites
	 */
	public boolean retirerArgent(Conseiller conseiller) {
		Client client = choixClient(conseiller);
		if (client != null) {
			ClientService cs = new ClientService();
			return cs.retirerArgent(client);
		}
		return false;

	}

	/**
	 * Service de recherche d'un compte via le numero de compte dans le portefeuille
	 * client du conseiller
	 * 
	 * @param numeroCompte : numero de compte recherche
	 * @param conseiller   : le conseiller
	 * @return le compte correspondant a la recherche, null compte non trouve
	 */
	public Compte rechercheCompte(String numeroCompte, Conseiller conseiller) {
		if (conseiller == null) {
			System.out.println(PERSONNE_INEXISTANT);
			return null;
		}
		ClientService cs = new ClientService();
		for (Entry<String, Client> itClient : conseiller.getPortefeuilleClient().entrySet()) {
			Client client = itClient.getValue();
			Compte compte = cs.rechercheCompte(numeroCompte, client);
			if (compte != null) {
				return compte;
			}
		}
		return null;
	}

	/**
	 * Service de modification des infos Clients
	 * 
	 * 
	 * @param conseiller : le Conseiller qui modifie les inforamtions
	 * @return true si la modification a ete effectue
	 */
	public boolean modifierInfosClient(Conseiller conseiller) {
		Client client = choixClient(conseiller);
		if (client != null) {
			return modifierInfosClient(client);
		}
		return false;
	}

	/**
	 * Service de modification des infos Clients
	 * 
	 * @param client : le client qui modifie ses infos
	 * @return true si la modification a ete effectue
	 */
	public boolean modifierInfosClient(Client client) {
		if (client == null || !ControleClient.isActif(client)) {
			System.out.println("client inactif");
			return false;
		}
		Scanner in = new Scanner(System.in);
		String reponse;
		do {
			System.out.println("voulez vous modifier le nom du client? (oui ou non)");
			reponse = in.nextLine();
			if (ControleSaisie.isOuiNon(reponse)) {
				if ("oui".equalsIgnoreCase(reponse)) {
					do {
						System.out.println("Entrer le nouveau nom du client : ");
						reponse = in.nextLine();
						if (!ControleSaisie.validerNom(reponse)) {
							System.out.println("Le nom doit ne contenir que des lettres");
						} else {
							client.setNom(reponse);
							break;
						}

					} while (true);
				}
				break;
			} else {
				System.out.println(OUI_NON);
			}
		} while (true);

		do {
			System.out.println("voulez vous modifier le prenom du client? (oui ou non)");
			reponse = in.nextLine();
			if (ControleSaisie.isOuiNon(reponse)) {
				if ("oui".equalsIgnoreCase(reponse)) {
					do {
						System.out.println("Entrer le nouveau prenom du client : ");
						reponse = in.nextLine();
						if (!ControleSaisie.validerNom(reponse)) {
							System.out.println("Le prenom doit ne contenir que des lettres");
						} else {
							client.setPrenom(reponse);
							break;
						}
					} while (true);
				}
				break;
			} else {
				System.out.println(OUI_NON);
			}
		} while (true);

		do {
			System.out.println("voulez vous modifier le mail du client? (oui ou non)");
			reponse = in.nextLine();
			if (ControleSaisie.isOuiNon(reponse)) {
				if ("oui".equalsIgnoreCase(reponse)) {
					do {
						System.out.println("Entrer le nouveau mail du client : ");
						reponse = in.nextLine();
						if (!ControleSaisie.validerMail(reponse)) {
							System.out.println("Le mail est invalide");
						} else {
							client.setMail(reponse);
							break;
						}
					} while (true);
				}
				break;
			} else {
				System.out.println(OUI_NON);
			}
		} while (true);
		IhmClient ihmClient = new IhmClient();
		ihmClient.affichageInfoClient(client);
		return true;
	}

	/**
	 * Service qui renvoie le client du conseiller souhaitant faire une operation
	 * 
	 * @param conseiller : le Conseiller
	 * @return e client du conseiller souhaitant faire une operation ou null
	 */
	private Client choixClient(Conseiller conseiller) {
		if (conseiller == null) {
			System.out.println(PERSONNE_INEXISTANT);
			return null;
		}
		if (!ControleConseiller.hasClients(conseiller)) {
			System.out.println("Le conseiller n'a pas de client dans son portefeuille client");
			return null;
		}
		Scanner in = new Scanner(System.in);
		String valeurRecherche;
		Client client;
		String typeRecherche;
		do {
			System.out.println(
					"type de recherche du client : \n1 : via identifiant\n2 : via nom\n3 : via numero de compte");
			typeRecherche = in.nextLine();
			if (ControleSaisie.saisieInt(typeRecherche, 1, 3)) {
				break;
			} else {
				System.out.println("Entrer 1, 2 ou 3");
			}
		} while (true);

		do {
			System.out.println("Entrer la valeur recherche, ou 0 pour annuler : ");
			valeurRecherche = in.nextLine();
			if ("0".equals(valeurRecherche)) {
				System.out.println("annulation");
				return null;
			}

			client = rechercheClient(conseiller, typeRecherche, valeurRecherche);
			if (client != null) {
				break;
			} else {
				System.out.println("Le conseiller n'a pas ce client dans son portefeuille");
			}
		} while (true);
		return client;
	}

	/**
	 * retire un client choisi du portefeuille du conseiller log� vers celui d'un
	 * autre
	 * 
	 * @param conseiller : le conseiller qui effectue le changement de domiciliation
	 * @param banque     : la Banque
	 */
	public void changerDomiciliation(Conseiller conseiller, Banque banque) {
		Client client = choixClient(conseiller);

		BanqueService bs = new BanqueService();
		Scanner in = new Scanner(System.in);
		String idConseiller;
		Conseiller nouveauConseiller;
		do {
			System.out.println("Entrer l'identifiant du nouveau conseiller, ou 0 pour annuler");
			idConseiller = in.nextLine();

			if ("0".equals(idConseiller)) {
				return;
			}
			nouveauConseiller = bs.rechercheConseiller(idConseiller, banque);
			if (nouveauConseiller == null) {
				System.out.println("Ce conseiller n'est pas present au sein de cette agence !!!");
			} else {
				break;
			}
		} while (true);

		if (conseiller != null && client != null) {
			conseiller.getPortefeuilleClient().remove(client.getIdentifiant());
			nouveauConseiller.getPortefeuilleClient().put(client.getIdentifiant(), client);
		}

	}

	/**
	 * affichage des infos client dans une fiche classique
	 * 
	 * @param personne : le conseiller qui effectue la demande d'info
	 */
	public void consulterInfoClient(Conseiller personne) {
		Client client = choixClient(personne);

		if (client != null) {
			IhmClient ihmClient = new IhmClient();
			ihmClient.affichageInfoClient(client);
		}

	}

	/**
	 * service de virement
	 * 
	 * @param conseiller : le conseiller qui effectue le virement pour un client
	 * @param banque     : la Banque
	 */
	public void virement(Conseiller conseiller, Banque banque) {
		Client client = choixClient(conseiller);
		ClientService cs = new ClientService();
		cs.virement(client, banque);

	}

	/**
	 * affichage des infos sur le compte client dans la console
	 * 
	 * @param personne le conseiller qui effectu la consulatation d'infos
	 */
	public void consulterComptesClient(Conseiller personne) {
		Client client = choixClient(personne);

		if (client != null) {
			IhmClient ihmClient = new IhmClient();
			ihmClient.affichageFicheClient(client);
		}
	}

}
