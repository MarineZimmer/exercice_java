package fr.afpa.service;

import java.util.Map.Entry;
import java.util.Scanner;

import fr.afpa.controle.ControleAuthentification;
import fr.afpa.controle.ControleBanque;
import fr.afpa.controle.ControlePersonne;
import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.Personne;
import fr.afpa.ihm.IhmAgence;
import fr.afpa.ihm.IhmClient;
import fr.afpa.ihm.IhmConseiller;

public class BanqueService {

	private static final String BANQUE_INEXISTANTE = "Erreur, banque inexistante";
	private static final String BANQUE_PERSONNE_INEXISTANT = "Erreur, banque ou personne inexistante";

	/**
	 * service qui verifie si l'authentification est bonne
	 * 
	 * @param login      : une chaine de caract�res repr�sentant le login
	 * @param motDePasse : une chaine de caract�res repr�sentant le mot de passe
	 * @return true si l'authentification est r�ussite, false sinon
	 */
	public String authentification(String login, String motDePasse) {
		return ControleAuthentification.authentification(login, motDePasse);
	}

	/**
	 * Service qui retourne la Personne authentifier
	 * 
	 * @param identifiant : le login de la personne
	 * @param banque      : la Banque
	 * @return un Administrateur, Un Conseiller ou un Client ou null si la personne
	 *         n'est pas trouv�e
	 */
	public Personne personneAuthentifier(String identifiant, Banque banque) {
		if (banque == null) {
			System.out.println(BANQUE_INEXISTANTE);
			return null;
		}
		if (ControlePersonne.isAdminitrateur(identifiant)) {
			return banque.getAdministrateur();
		}
		if (ControlePersonne.isConseiller(identifiant)) {
			return rechercheConseiller(identifiant, banque);
		}
		if (ControlePersonne.isClient(identifiant)) {
			return rechercheClient(banque, "1", identifiant);
		}
		return null;
	}

	/**
	 * Service de creation agence
	 * 
	 * @param banque         : la Banque dans laquelle l'agence est creer
	 * @param administrateur : l'Administrateur qui cree l'agence
	 */
	public void creationAgence(Banque banque, Administrateur administrateur) {
		if (administrateur == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return;
		}
		AgenceService as = new AgenceService();
		Agence agence = as.creerAgence(banque);
		if (agence != null) {
			banque.getListeAgences().put(agence.getCodeAgence(), agence);
			IhmAgence ihmAgence = new IhmAgence();
			ihmAgence.affichageInfoAgence(agence);
		}
	}

	/**
	 * Service de creation d'un conseiller
	 * 
	 * @param banque         : la Banque dans laquelle le conseiller est creer
	 * @param administrateur : l'Administrateur qui cree le conseiller
	 */
	public void creationConseiller(Banque banque, Administrateur administrateur) {
		if (administrateur == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return;
		}
		if (!ControleBanque.agenceExiste(banque)) {
			System.out.println("Impossible d'embaucher un conseiller sans agence");
			return;
		}

		ConseillerService cs = new ConseillerService();
		String codeAgence;
		Agence agence;
		Scanner in = new Scanner(System.in);

		do {
			System.out
					.println("Entrer le code agence dans laquelle vous voulez creer le conseiller, ou 0 pour annuler");
			codeAgence = in.nextLine();
			if ("0".equals(codeAgence)) {
				return;
			}
			if (ControleBanque.codeAgenceUnique(banque, codeAgence)) {
				System.out.println("Le code agence n'existe pas au sein de la banque");
			} else {
				break;
			}
		} while (true);

		agence = rechercheAgence(banque, codeAgence);
		Conseiller conseiller = cs.creerConseiller();
		if (agence != null && conseiller != null) {
			agence.getListeConseillers().put(conseiller.getLogin(), conseiller);
			IhmConseiller ihmConseiller = new IhmConseiller();
			ihmConseiller.affichageInfoConseiller(conseiller);
		}
	}

	/**
	 * Service de creation d'un client
	 * 
	 * @param conseiller : le Conseiller ou l'administrateur qui cree le client
	 */
	public void creationClient(Banque banque, Conseiller conseiller) {
		if (conseiller == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return;
		}
		if (ControleBanque.agenceExiste(banque)) {
			if (conseiller instanceof Administrateur) {
				AdministrateurService as = new AdministrateurService();
				as.creationClient((Administrateur) conseiller, banque);
			} else {
				ConseillerService cs = new ConseillerService();
				cs.creationClient(conseiller, banque);
			}
		} else {
			System.out.println("Aucune agence existe, cr�ez une agence avec un conseiller avant un client");
		}
	}

	/**
	 * Service de creation compte
	 * 
	 * @param conseiller : le Conseilller ou l'administrateur qui cr�e le compte
	 * @return true si la creation de compte a bien ete faites, false sinon
	 */
	public boolean creationCompte(Conseiller conseiller, Banque banque) {
		if (conseiller == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return false;
		}
		if (conseiller instanceof Administrateur) {
			AdministrateurService am = new AdministrateurService();
			return am.creationCompte((Administrateur) conseiller, banque);
		} else {
			if (conseiller instanceof Conseiller) {
				ConseillerService cs = new ConseillerService();
				cs.creationCompte(conseiller, banque);
				return cs.creationCompte(conseiller, banque);
			}
		}
		return false;
	}

	/**
	 * service de recherche d'une agence via son code agence
	 * 
	 * @param banque     : la Banque ou on recherche l'agence
	 * @param codeAgence : le code agence de l'agence recherche
	 * @return l'agence recherche si elle existe , null sinon
	 */
	public Agence rechercheAgence(Banque banque, String codeAgence) {
		if (banque == null) {
			System.out.println(BANQUE_INEXISTANTE);
			return null;
		}
		return banque.getListeAgences().get(codeAgence);
	}

	/**
	 * Service de recherche d'un Conseiller dans la Banque, via login
	 * 
	 * @param login  : le login du Conseiller recherch�
	 * @param banque : la Banque
	 * @return un Conseiller si il est pr�sent dans la banque null sinon
	 */
	public Conseiller rechercheConseiller(String login, Banque banque) {
		if (banque == null) {
			System.out.println(BANQUE_INEXISTANTE);
			return null;
		}
		AgenceService as = new AgenceService();
		for (Entry<String, Agence> itAgence : banque.getListeAgences().entrySet()) {
			Agence agence = itAgence.getValue();
			Conseiller conseiller = as.rechercheConseiller(login, agence);
			if (conseiller != null) {
				return conseiller;
			}
		}
		return null;
	}

	/**
	 * Service de recherche d'un client au sein de la banque
	 * 
	 * @param banque          : la Banque
	 * @param typeRecherche   : le type de recherche(1,2 ou3) du
	 *                        client(identifiant(1), nom(2), numero de compte(3))
	 * @param valeurRecherche : la valeur recherche
	 * @return : le premier Client correspondant a la recherche
	 */
	public Client rechercheClient(Banque banque, String typeRecherche, String valeurRecherche) {
		if (banque == null) {
			System.out.println(BANQUE_INEXISTANTE);
			return null;
		}
		AgenceService as = new AgenceService();
		for (Entry<String, Agence> itAgence : banque.getListeAgences().entrySet()) {
			Agence agence = itAgence.getValue();
			Client client = as.rechercheClient(agence, typeRecherche, valeurRecherche);
			if (client != null) {
				return client;
			}
		}
		return null;
	}

	/**
	 * Service de recherche d'un compte dans la banque
	 * 
	 * @param numeroCompte : le numero compte
	 * @param banque       : la Banque
	 * @return un Compte si il existe dans la banque, null sinon
	 */
	public Compte rechercheCompte(String numeroCompte, Banque banque) {
		if (banque == null) {
			System.out.println(BANQUE_INEXISTANTE);
			return null;
		}
		AgenceService as = new AgenceService();
		for (Entry<String, Agence> itAgence : banque.getListeAgences().entrySet()) {
			Agence agence = itAgence.getValue();
			Compte compte = as.rechercheCompte(numeroCompte, agence);
			if (compte != null) {
				return compte;
			}
		}
		return null;
	}

	public void consulterInfoClient(Banque banque, Personne personne) {
		if (personne == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return;
		}

		if (personne instanceof Client) {
			IhmClient ihmClient = new IhmClient();
			ihmClient.affichageInfoClient((Client) personne);
			return;
		}
		if (personne instanceof Administrateur) {
			AdministrateurService adminsitrateurService = new AdministrateurService();
			adminsitrateurService.consulterInfoClient((Administrateur) personne, banque);
			return;
		}
		if (personne instanceof Conseiller) {

			ConseillerService conseillerService = new ConseillerService();
			conseillerService.consulterInfoClient((Conseiller) personne);

		}

	}

	public void consulterComptesClient(Banque banque, Personne personne) {
		if (personne == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return;
		}

		if (personne instanceof Client) {
			IhmClient ihmClient = new IhmClient();
			ihmClient.affichageFicheClient((Client) personne);
			return;
		}
		if (personne instanceof Administrateur) {
			AdministrateurService adminsitrateurService = new AdministrateurService();
			adminsitrateurService.consulterComptesClient((Administrateur) personne, banque);
			return;
		}
		if (personne instanceof Conseiller) {
			ConseillerService conseillerService = new ConseillerService();
			conseillerService.consulterComptesClient((Conseiller) personne);
		}

	}

	/**
	 * Service de consultation des operations de chaque compte d'un client
	 * 
	 * @param banque   : la Banque
	 * @param personne : la Personne qui utilise le menu(client,conseiller ou
	 *                 administrateur)
	 */
	public void consulterOperationsCompte(Banque banque, Personne personne) {
		if (personne == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return;
		}

		if (personne instanceof Client) {
			ClientService clientService = new ClientService();
			clientService.consulterOperationsCompte((Client) personne);
			return;
		}
		if (personne instanceof Administrateur) {
			AdministrateurService administrateurService = new AdministrateurService();
			administrateurService.consulterOperationsCompte((Administrateur) personne, banque);
			return;
		}
		if (personne instanceof Conseiller) {
			ConseillerService conseillerService = new ConseillerService();
			conseillerService.consulterOperationsCompte((Conseiller) personne);
		}
	}

	/**
	 * Service de virement @
	 */
	public boolean virement(Banque banque, Personne personne) {
		if (personne == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return false;
		}

		if (personne instanceof Client) {
			ClientService cs = new ClientService();
			cs.virement((Client) personne, banque);
			return true;
		}
		if (personne instanceof Administrateur) {
			AdministrateurService as = new AdministrateurService();
			as.virement((Administrateur) personne, banque);
			return true;
		}
		if (personne instanceof Conseiller) {
			ConseillerService cs = new ConseillerService();
			cs.virement((Conseiller) personne, banque);
			return true;
		}
		return false;

	}

	public Conseiller chercherConseiller(Client client, Banque banque) {
		if (banque == null) {
			System.out.println(BANQUE_INEXISTANTE);
			return null;
		}
		AgenceService as = new AgenceService();
		for (Entry<String, Agence> itAgence : banque.getListeAgences().entrySet()) {
			Agence agence = itAgence.getValue();
			Conseiller conseiller = as.rechercheConseillerClient(client, agence);
			if (conseiller != null) {
				return conseiller;
			}
		}
		return null;

	}

	/**
	 * Service qui imprime le releve d'un compte client(fichier .txt, dossier
	 * releveCompte
	 * 
	 * @param banque   : la Banque
	 * @param personne : la Personne qui utilise le menu(client,conseiller ou
	 *                 administrateur)
	 */
	public void imprimerReleveCompte(Banque banque, Personne personne) {
		if (personne == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return;
		}

		if (personne instanceof Client) {
			ClientService clientService = new ClientService();
			clientService.imprimerReleveCompte((Client) personne);
			return;
		}
		if (personne instanceof Administrateur) {
			AdministrateurService administrateurService = new AdministrateurService();
			administrateurService.imprimerReleveCompte((Administrateur) personne, banque);
			return;
		}
		if (personne instanceof Conseiller) {
			ConseillerService conseillerService = new ConseillerService();
			conseillerService.imprimerReleveCompte((Conseiller) personne);
		}

	}

	/**
	 * Service d'alimentation d'un compte
	 * 
	 * @param banque   : la Banque
	 * @param personne : la Personne qui utilise le menu(client,conseiller ou
	 *                 administrateur)
	 * @return true si la transaction a bien ete faites, false sinon
	 */
	public boolean alimenterCompte(Banque banque, Personne personne) {
		if (personne == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return false;
		}

		if (personne instanceof Client) {
			ClientService clientService = new ClientService();
			return clientService.alimenterCompte((Client) personne);
		}
		if (personne instanceof Administrateur) {
			AdministrateurService administrateurService = new AdministrateurService();
			return administrateurService.alimenterCompte((Administrateur) personne, banque);
		}
		if (personne instanceof Conseiller) {
			ConseillerService conseillerService = new ConseillerService();
			return conseillerService.alimenterCompte((Conseiller) personne);
		}
		return false;
	}

	/**
	 * Service de retrait d'argent
	 * 
	 * @param banque   : la Banque
	 * @param personne : la Personne qui utilise le menu(client,conseiller ou
	 *                 administrateur)
	 * @return true si la transaction a bien ete faites, false sinon
	 */
	public boolean retirerArgent(Banque banque, Personne personne) {
		if (personne == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return false;
		}

		if (personne instanceof Client) {
			ClientService clientService = new ClientService();
			return clientService.retirerArgent((Client) personne);
		}
		if (personne instanceof Administrateur) {
			AdministrateurService administrateurService = new AdministrateurService();
			return administrateurService.retirerArgent((Administrateur) personne, banque);
		}
		if (personne instanceof Conseiller) {
			ConseillerService conseillerService = new ConseillerService();
			return conseillerService.retirerArgent((Conseiller) personne);
		}
		return false;
	}

	public void changerDomiciliationCompte(Banque banque, Conseiller conseiller) {
		if (conseiller == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return;
		}
		if (conseiller instanceof Administrateur) {
			AdministrateurService administrateurService = new AdministrateurService();
			administrateurService.changerDomiciliation((Administrateur) conseiller, banque);
			return;
		}
		if (conseiller instanceof Conseiller) {
			ConseillerService conseillerService = new ConseillerService();
			conseillerService.changerDomiciliation(conseiller, banque);
		}

	}

	/**
	 * Service de modification des infos Clients
	 * 
	 * @param banque     : la Banque du client
	 * @param conseiller : le Conseiller qui modifie les inforamtions
	 * @return true si la modification a bien ete faites, false sinon
	 */
	public boolean modifierInfosClient(Banque banque, Conseiller conseiller) {
		if (conseiller == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return false;
		}

		if (conseiller instanceof Administrateur) {
			AdministrateurService administrateurService = new AdministrateurService();
			return administrateurService.modifierInfosClient((Administrateur) conseiller, banque);

		}
		if (conseiller instanceof Conseiller) {
			ConseillerService conseillerService = new ConseillerService();
			return conseillerService.modifierInfosClient((Conseiller) conseiller);

		}
		return false;
	}

	/**
	 * Service de desactivation d'un compte
	 * 
	 * @param banque         : la Banque
	 * @param administrateur : l'Administrateur qui desactive le compte
	 */
	public void desactivationCompte(Banque banque, Administrateur administrateur) {
		if (administrateur == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return;
		}
		AdministrateurService as = new AdministrateurService();
		as.desactiverCompte(banque);
	}

	public void desactivationClient(Banque banque, Administrateur administrateur) {
		if (administrateur == null || banque == null) {
			System.out.println(BANQUE_PERSONNE_INEXISTANT);
			return;
		}
		AdministrateurService as = new AdministrateurService();
		as.desactiverClient(administrateur, banque);
	}
}
