package fr.afpa.service;

import java.time.LocalDate;
import java.util.Scanner;

import fr.afpa.controle.ControleBanque;
import fr.afpa.controle.ControleClient;
import fr.afpa.controle.ControleCompte;
import fr.afpa.controle.ControleSaisie;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.CompteEpargne;
import fr.afpa.entite.LivretA;
import fr.afpa.ihm.IhmCompte;

public class CompteService {

	private static final String COMPTE_INACTIF = "Operation impossible le compte n'est pas actif";
	private static final String CHEMIN_DOSSIER_TRANSACTIONS_COMPTE = "ressources\\transactions\\transactionsCompte\\compte";

	/**
	 * service de creation d'un compte
	 * 
	 * @param client : le client du compte cree
	 * @param banque : la banque
	 * @return le Compte cree
	 */
	public Compte creerCompte(Client client, Banque banque) {
		String typeCompte;
		String decouvert;
		String numeroCompte;
		AleatoireService as = new AleatoireService();
		IhmCompte ihmCompte = new IhmCompte();

		if (!ControleClient.isActif(client)) {
			System.out.println("Le client n'est pas actif, impossible de cr�er un compte");
			return null;
		}
		if (ControleCompte.nbComptesActif(client) == 3) {
			System.out.println("Le client a d�j� 3 compte actifs!");
			return null;
		}
		if (ControleCompte.nbComptesActif(client) == 0) {
			System.out.println(
					"Le client n'a pas de comptes actifs\n---------------- Creation Compte Courant -------------------");
			typeCompte = "1";
		} else {
			typeCompte = ihmCompte.entrerTypeCompte();
			System.out.println("---------------- Creation Compte  -------------------");
		}

		decouvert = ihmCompte.entrerDecouvert();
		do {
			numeroCompte = as.aleatoireNombre(11);
		} while (!ControleBanque.numeroCompteUnique(banque, numeroCompte));

		Compte compte = null;
		switch (typeCompte) {
		case "1":
			compte = new CompteCourant(Boolean.parseBoolean(decouvert), numeroCompte);
			break;
		case "2":
			compte = new LivretA(Boolean.parseBoolean(decouvert), numeroCompte);
			break;
		case "3":
			compte = new CompteEpargne(Boolean.parseBoolean(decouvert), numeroCompte);
			break;
		default:
			compte = null;
		}

		if (compte != null) {
			FichierService fs = new FichierService();
			// efface le fichier des transactions du compte si il existe deja
			fs.ecritureFichier(CHEMIN_DOSSIER_TRANSACTIONS_COMPTE + compte.getNumeroCompte() + ".txt", "", false);
			alimenterCompte(compte);

		}
		return compte;
	}

	/**
	 * Service qui alimente un compte
	 * 
	 * @param compte : le compte a alimenter
	 * @return true si l'operation a ete effectue correctement
	 */
	public boolean alimenterCompte(Compte compte) {

		String montant;
		Scanner in = new Scanner(System.in);
		if (!compte.isActif()) {
			System.out.println(COMPTE_INACTIF);
			return false;
		}
		do {
			System.out.println("Quel montant voulez vous mettre sur votre compte : ");
			montant = in.nextLine();
			if (ControleSaisie.saisieFloat(montant) && Float.parseFloat(montant) >= 0) {
				break;
			} else {
				System.out.println("Entrer un nombre decimal positif ou nul !!!!!!!");
			}
		} while (true);

		return modificationSoldeCompte(Float.parseFloat(montant), compte);

	}

	/**
	 * Service qui retire de l'argent d'un compte
	 * 
	 * @param compte : le compte a alimenter
	 * @return true si l'operation est effectuee correctement
	 */
	public boolean retraitCompte(Compte compte) {
		String montant;
		Scanner in = new Scanner(System.in);
		if (!compte.isActif()) {
			System.out.println(COMPTE_INACTIF);
			return false;
		}
		do {
			System.out.println("Quel montant voulez vous retirer sur votre compte : ");
			montant = in.nextLine();
			if (ControleSaisie.saisieFloat(montant) && Float.parseFloat(montant) >= 0) {
				break;
			} else {
				System.out.println("Entrer un nombre decimal positif ou nul !!!!!!!");
			}
		} while (true);

		if (ControleCompte.retraitPossible(compte, Float.parseFloat(montant))) {
			modificationSoldeCompte(-Float.parseFloat(montant), compte);
			return true;
		} else {
			System.out.println("Impossible d'effectuer cette operation, vous n'�tes pas autorise de decouvert");
			return false;
		}

	}

	/**
	 * service qui permet d'ajouter ou enlever de l'argent � un compte le compte
	 * 
	 * @param montant : le montant ajouter ou retirer
	 * @param compte  : le compte ou l'operation est effectue
	 * @return true si l'operation est effectue correctement
	 */
	public boolean modificationSoldeCompte(float montant, Compte compte) {
		if (compte == null || !compte.isActif()) {
			System.out.println(COMPTE_INACTIF);
			return false;
		}

		compte.setSolde(compte.getSolde() + montant);
		ajoutTransactionFichierCompteEtBanque(montant, compte);
		return true;

	}

	/**
	 * Service qui ajoute une transaction dans les fichiers transactions
	 * 
	 * @param montant : montant de la transaction
	 * @param compte  : compte de la transaction
	 */
	private void ajoutTransactionFichierCompteEtBanque(float montant, Compte compte) {
		if (!compte.isActif()) {
			System.out.println(COMPTE_INACTIF);
			return;
		}
		FichierService fs = new FichierService();
		String ecritureFichier;
		if (montant > 0) {
			ecritureFichier = LocalDate.now() + ";" + "credit" + ";" + montant + ";" + compte.getNumeroCompte();
		} else if (montant < 0) {
			ecritureFichier = LocalDate.now() + ";" + "debit" + ";" + montant + ";" + compte.getNumeroCompte();
		} else {
			return;
		}
		fs.ecritureFichier(CHEMIN_DOSSIER_TRANSACTIONS_COMPTE + compte.getNumeroCompte() + ".txt", ecritureFichier,
				true);
		fs.ecritureFichier("ressources\\transactions\\transactionsBanque.txt", ecritureFichier, true);

	}

	/**
	 * Service qui calcul les frais du compte selon le type de compte
	 * 
	 * @param compte : le compte a calculer les frais
	 * @return les frais du compte calculer selon son type
	 */
	public float calculFrais(Compte compte) {
		if (compte instanceof LivretA) {
			return compte.getFrais() + (0.1f * compte.getSolde());
		}
		if (compte instanceof CompteEpargne) {
			return compte.getFrais() + (0.25f * compte.getSolde());
		}
		return compte.getFrais();
	}

	/**
	 * Service qui imprime un releve de compte (enregitre dans un fichier txt
	 * (ressources\releveCompte)
	 * 
	 * @param compte : le compte a imprimer le releve
	 * @param client : le client du compte
	 */
	public void imprimerReleveCompte(Compte compte, Client client) {
		if (client == null || compte == null) {
			System.out.println("Erreur, compte ou client non existant");
			return;
		}
		if (!compte.isActif() || !client.isActif()) {
			System.out.println("Compte ou client inactif");
			return;
		}
		String dateDebut;
		String dateFin;
		IhmCompte ihmCompte = new IhmCompte();

		dateDebut = ihmCompte.entrerDateDebutReleve();
		dateFin = ihmCompte.entrerDateFinReleve(dateDebut);

		FichierService fs = new FichierService();
		String[] transactions = fs.lecture(CHEMIN_DOSSIER_TRANSACTIONS_COMPTE + compte.getNumeroCompte() + ".txt");

		StringBuilder ecritureFichier = new StringBuilder();
		ecritureFichier.append("----------------- Releve de compte numero " + compte.getNumeroCompte()
				+ " ----------------------------\n\n");
		ecritureFichier.append("Date du releve de compte : du " + dateDebut + " au " + dateFin);
		ecritureFichier.append("\n\n");
		ecritureFichier.append("Client : " + client.getNom() + " " + client.getPrenom() + "\nIdentifiant : "
				+ client.getIdentifiant());
		ecritureFichier.append("\n\n");

		ecritureFichier.append(String.format("%20s", "date |"));
		ecritureFichier.append(String.format("%20s", "type |"));
		ecritureFichier.append(String.format("%20s", "valeur|") + "\n");

		for (int i = 0; i < transactions.length; i++) {
			String[] transaction = transactions[i].split(";");
			if (transaction.length == 4 && ControleCompte.dateOperation(transaction[0], dateDebut, dateFin)) {
				ecritureFichier.append(String.format("%20s", transaction[0] + " |"));
				ecritureFichier.append(String.format("%20s", transaction[1] + " |"));
				ecritureFichier.append(String.format("%20s", transaction[2] + " |") + "\n");
			}
		}

		ecritureFichier.append("\n Solde du compte : " + compte.getSolde());

		fs.ecritureFichier("ressources\\releveCompte\\releve" + compte.getNumeroCompte() + ".txt",
				ecritureFichier.toString(), false);
	}

}
