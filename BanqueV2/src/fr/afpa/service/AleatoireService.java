package fr.afpa.service;

import java.util.Random;

public class AleatoireService {

	/**
	 * service qui genere un nombre aleatoire
	 * 
	 * @param nbChiffres : le nombre de chiffres que le nombre doit avoir
	 * @return un nmbre aleatoire de la taille demande
	 */
	public String aleatoireNombre(int nbChiffres) {
		StringBuilder nombre = new StringBuilder();
		for (int i = 0; i < nbChiffres; i++) {
			nombre.append(new Random().nextInt(10));
		}
		return nombre.toString();
	}

}
