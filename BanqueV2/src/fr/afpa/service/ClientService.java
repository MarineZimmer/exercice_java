package fr.afpa.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map.Entry;
import java.util.Scanner;

import fr.afpa.controle.ControleClient;
import fr.afpa.controle.ControleCompte;
import fr.afpa.controle.ControleSaisie;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.ihm.IhmCompte;
import fr.afpa.ihm.IhmPersonne;

public class ClientService {

	private static final String CLIENT_INACTIF = "impossible d'effectuer cette operation client inactif";

	/**
	 * service de creation d'un client (nom,prenom,date de naissance, mail, mot de
	 * passse) + enregistrement login;motdepasse dans fichier authentification
	 * 
	 * @return le Client
	 */
	public Client creerClient() {
		String nom;
		String prenom;
		String dateNaissance;
		String mail;
		String motDePasse;
		IhmPersonne ihmPersonne = new IhmPersonne();
		System.out.println("--------------- Creation Client -------------------------");

		nom = ihmPersonne.entrerNom();
		prenom = ihmPersonne.entrerPrenom();
		dateNaissance = ihmPersonne.entrerDateNaissance();
		mail = ihmPersonne.entrerMail();
		Client client = new Client(nom, prenom,
				LocalDate.parse(dateNaissance, DateTimeFormatter.ofPattern("dd/MM/yyyy")), mail);

		CryptageService cs = new CryptageService();
		FichierService fs = new FichierService();
		motDePasse = ihmPersonne.entrerMotDePasse();
		motDePasse = cs.encoder(motDePasse);
		fs.ecritureFichier("ressources//fichierAuthentification.txt",
				client.getIdentifiant() + ";" + client.getLogin() + ";" + motDePasse, true);
		System.out.println("--------------------------------------------------------");
		return client;
	}

	/**
	 * Service d'impression d'un releve de compte
	 * 
	 * @param client : le client effectuant la demande
	 */
	public void imprimerReleveCompte(Client client) {
		if (!ControleClient.isActif(client)) {
			System.out.println(CLIENT_INACTIF);
			return;
		}
		Compte compte = choixCompte(client);
		if (compte != null) {
			CompteService compteService = new CompteService();
			compteService.imprimerReleveCompte(compte, client);
		}
	}

	/**
	 * service de consultations des operations des comptes du clients
	 * 
	 * @param client : le client qui souhaite consulter les operations de ces
	 *               comptes
	 */
	public void consulterOperationsCompte(Client client) {
		if (!ControleClient.isActif(client)) {
			System.out.println(CLIENT_INACTIF);
			return;
		}
		IhmCompte ihmCompte = new IhmCompte();
		for (Entry<String, Compte> compte : client.getListeComptes().entrySet()) {
			if (compte.getValue().isActif()) {
				ihmCompte.affichageOperations(compte.getValue());
			}
		}
	}

	/**
	 * Service alimentation d'un compte
	 * 
	 * @param client : le Client qui souhaite alimenter son compte
	 * @return true si la transaction a bien ete faites, false sinon
	 */
	public boolean alimenterCompte(Client client) {
		if (!ControleClient.isActif(client)) {
			System.out.println(CLIENT_INACTIF);
			return false;
		}
		Compte compte = choixCompte(client);
		if (compte != null) {
			CompteService compteService = new CompteService();
			return compteService.alimenterCompte(compte);
		}
		return false;
	}

	/**
	 * Service de retrait d'argent compte
	 * 
	 * @param client : le Client qui souhaite retirer de l'argent sur un compte
	 * @return true si la transaction a bien ete faites, false sinon
	 */
	public boolean retirerArgent(Client client) {
		if (!ControleClient.isActif(client)) {
			System.out.println(CLIENT_INACTIF);
			return false;
		}
		Compte compte = choixCompte(client);
		if (compte != null) {
			CompteService compteService = new CompteService();
			return compteService.retraitCompte(compte);
		}
		return false;
	}

	/**
	 * Choix du compte du client pour effectuer une operation
	 * 
	 * @param client : le Client
	 * @return le Compte sur lequel l'operation est effectue
	 */
	private Compte choixCompte(Client client) {
		if (client == null) {
			System.out.println("Erreur, client inexistant");
			return null;
		}
		if (!ControleClient.isActif(client)) {
			System.out.println(CLIENT_INACTIF);
			return null;
		}

		if (ControleCompte.nbComptesActif(client) == 0) {
			System.out.println("Aucun compte actif");
			return null;
		}
		Scanner in = new Scanner(System.in);
		String numeroCompte;
		Compte compte;
		do {
			System.out.println("Entrer le numero de compte, ou 0 pour annuler : ");
			numeroCompte = in.nextLine();
			if ("0".equals(numeroCompte)) {
				System.out.println("annulation");
				return null;
			}
			compte = rechercheCompte(numeroCompte, client);
			if (compte != null) {
				return compte;
			} else {
				System.out.println("Erreur ce compte n'appartient pas client");
			}
		} while (true);

	}

	/**
	 * Service de recherche d'un compte appartenant au client via le numero de
	 * compte
	 * 
	 * @param numeroCompte : numero du compte recherche
	 * @param client       : le Client que l'on recherche le compte
	 * @return : le compte correspondant a la recherche, null si compte pas trouve
	 */
	public Compte rechercheCompte(String numeroCompte, Client client) {
		if (!ControleClient.isActif(client)) {
			System.out.println(CLIENT_INACTIF);
			return null;
		}
		if (client == null) {
			return null;
		}
		return client.getListeComptes().get(numeroCompte);
	}

	public boolean virement(Client client, Banque banque) {
		Compte compte = choixCompte(client);
		Scanner in = new Scanner(System.in);
		String destinataire;
		String montant;
		BanqueService bs = new BanqueService();
		CompteService cs = new CompteService();
		if (client == null) {
			return false;
		}
		do {
			System.out.println("numero compte destinataire");
			destinataire = in.nextLine();
			if (ControleCompte.isCompteValide(destinataire, banque)) {
				break;
			} else {
				System.out.println("compte invalide, saisir un nouveau numero");
			}
		} while (true);
		Compte compteDestinataire = bs.rechercheCompte(destinataire, banque);
		do {
			System.out.println("Quel montant voulez vous virer ? : ");
			montant = in.nextLine();
			if (ControleSaisie.saisieFloat(montant) && Float.parseFloat(montant) >= 0) {
				break;
			} else {
				System.out.println("Entrer un nombre decimal positif ou nul !!!!!!!");
			}
		} while (true);

		if (ControleCompte.retraitPossible(compte, Float.parseFloat(montant))) {
			cs.modificationSoldeCompte(-Float.parseFloat(montant), compte);
			if (compteDestinataire != null) {
				cs.modificationSoldeCompte(Float.parseFloat(montant), compteDestinataire);
			} else {
				FichierService fs = new FichierService();
				String ecritureFichier = LocalDate.now() + ";" + "credit" + ";" + montant + ";" + destinataire;
				fs.ecritureFichier("ressources\\transactions\\transactionsBanque.txt", ecritureFichier, true);
			}
		} else {
			System.out.println("Impossible d'effectuer cette operation, vous n'�tes pas autorise de decouvert");
		}
		return true;

	}

}
