package fr.afpa.service;

import java.time.LocalDate;
import java.util.Scanner;

import fr.afpa.controle.ControleAgence;
import fr.afpa.controle.ControleBanque;
import fr.afpa.controle.ControleClient;
import fr.afpa.controle.ControleSaisie;
import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.Conseiller;
import fr.afpa.ihm.IhmClient;
import fr.afpa.ihm.IhmCompte;

public class AdministrateurService {

	/**
	 * service de creation d'un administrateur
	 * 
	 * @return un Administrateur
	 */
	public Administrateur creerAdministrateur() {

		Administrateur admin = new Administrateur("nomAdmin", "prenomAdmin", LocalDate.parse("1980-01-01"),
				"admin@banque.fr");
		FichierService fs = new FichierService();
		CryptageService cs = new CryptageService();
		fs.ecritureFichier("ressources//fichierAuthentification.txt",
				admin.getLogin() + ";" + admin.getLogin() + ";" + cs.encoder("admin"), false);
		return admin;
	}

	/**
	 * Service de creation d'un client
	 * 
	 * @param administrateur : l'Administrateur qui creer un client
	 * @param banque         :la Banque
	 */
	public void creationClient(Administrateur administrateur, Banque banque) {
		if (administrateur == null || banque == null) {
			System.out.println("Erreur, admistrateur ou banque inexistant  ");
			return;
		}
		Scanner in = new Scanner(System.in);
		String codeAgence;
		String idConseiller;
		BanqueService bs = new BanqueService();
		AgenceService as = new AgenceService();
		ConseillerService cs = new ConseillerService();

		do {
			System.out.println("Entrer le code agence dans laquelle vous voulez creer le client, ou 0 pour annuler");
			codeAgence = in.nextLine();
			if ("0".equals(codeAgence)) {
				return;
			}
			if (ControleBanque.codeAgenceUnique(banque, codeAgence)) {
				System.out.println("Le code agence n'existe pas au sein de la banque");
			} else {
				break;
			}
		} while (true);

		Agence agence = bs.rechercheAgence(banque, codeAgence);
		if (!ControleAgence.conseillerExiste(agence)) {
			System.out.println(
					"Aucun conseiller dans cette agence, veuillez creer un conseiller avant de creer un client dans cette agence");
			return;
		}

		do {
			System.out.println("Entrer l'identifiant du conseiller du nouveau client, ou 0 pour annuler");
			idConseiller = in.nextLine();
			if ("0".equals(idConseiller)) {
				return;
			}
			if (!ControleAgence.conseillerPresent(agence, idConseiller)) {
				System.out.println("Ce conseiller n'est pas present au sein de cette agence !!!");
			} else {
				break;
			}
		} while (true);

		Conseiller conseiller = as.rechercheConseiller(idConseiller, agence);
		cs.creationClient(conseiller, banque);
	}

	/**
	 * service de creation de compte
	 * 
	 * @param administrateur : l'administrateur qui creer un compte pour un client
	 * @param banque         : la Banque
	 * @return true si la creation du compte est faites et ajouter a la liste de
	 *         comptes du client, false sinon
	 */
	public boolean creationCompte(Administrateur administrateur, Banque banque) {
		if (administrateur == null || banque == null) {
			System.out.println("Erreur, admistrateur ou banque inexistant");
			return false;
		}
		Client client = choixClient(administrateur, banque);
		if (client != null) {
			CompteService cs = new CompteService();
			Compte compte = cs.creerCompte(client, banque);
			if (compte != null) {
				client.getListeComptes().put(compte.getNumeroCompte(), compte);
				IhmCompte ihmCompte = new IhmCompte();
				ihmCompte.affichageInfoCompte(compte);
				return true;
			}
		}
		return false;
	}

	/**
	 * service d'impression d'un releve compte
	 * 
	 * @param administrateur : l'Adminastreur qui utilise le menu
	 * @param banque         : la Banque
	 */
	public void imprimerReleveCompte(Administrateur administrateur, Banque banque) {
		Client client = choixClient(administrateur, banque);
		if (client != null) {

			ClientService cs = new ClientService();
			cs.imprimerReleveCompte(client);
		}
	}

	/**
	 * Service de consulatation des operations de l'ensemble des comptes d'un client
	 * 
	 * @param administrateur : l'Adminastreur qui utilise le menu
	 * @param banque         : la Banque
	 */
	public void consulterOperationsCompte(Administrateur administrateur, Banque banque) {
		Client client = choixClient(administrateur, banque);
		if (client != null) {
			ClientService cs = new ClientService();
			cs.consulterOperationsCompte(client);
		}
	}

	/**
	 * Service d'alimentation d'un compte
	 * 
	 * @param administrateur : l'Adminastreur qui utilise le menu
	 * @param banque         : la Banque
	 * @return true si la transaction a bien ete faites, false sinon
	 */
	public boolean alimenterCompte(Administrateur administrateur, Banque banque) {
		Client client = choixClient(administrateur, banque);
		if (client != null) {
			ClientService cs = new ClientService();
			return cs.alimenterCompte(client);
		}
		return false;
	}

	/**
	 * Service de retrait d'argent
	 * 
	 * @param administrateur : l'Adminastreur qui utilise le menu
	 * @param banque         : la Banque
	 * @return true si la transaction a bien ete faites, false sinon
	 */
	public boolean retirerArgent(Administrateur administrateur, Banque banque) {
		Client client = choixClient(administrateur, banque);
		if (client != null) {
			ClientService cs = new ClientService();
			return cs.retirerArgent(client);
		}
		return false;
	}

	/**
	 * Service de desactivation d'un compte
	 * 
	 * @param administrateur : l'administrateur qui desactive un compte
	 * @param banque         : la Banque
	 * @return true si la transaction a bien ete faites, false sinon
	 */
	public boolean desactiverCompte(Banque banque) {
		if (!ControleBanque.hasCompte(banque)) {
			System.out.println("Pas de comptes dans la banque");
			return false;
		}
		Scanner in = new Scanner(System.in);
		String numeroCompte;
		Compte compte;

		do {
			System.out.println("Entrer le numero de compte, ou 0 pour annuler : ");
			numeroCompte = in.nextLine();
			if ("0".equals(numeroCompte)) {
				System.out.println("Annulation");
				return false;
			}
			BanqueService bs = new BanqueService();
			compte = bs.rechercheCompte(numeroCompte, banque);
			if (compte != null) {
				if ((compte instanceof CompteCourant)) {
					System.out.println("impossible de desactiver un compte courant");
					break;
				}
				compte.setActif(false);
				return true;
			} else {
				System.out.println(" Numero de compte incorrect");
			}
		} while (true);
		return false;
	}

	/**
	 * change le client du portefeuille d'un conseiller vers un autre a partir des
	 * log administrateur
	 * 
	 * @param administrateur : l'Administrateur qui change la domiciliation du
	 *                       client
	 * @param banque         : la Banque
	 */
	public void changerDomiciliation(Administrateur administrateur, Banque banque) {
		Client choix = choixClient(administrateur, banque);
		if (choix == null) {
			return;
		}
		if (!choix.isActif()) {
			System.out.println("Client desactive");
			return;
		}
		BanqueService bs = new BanqueService();
		Conseiller ancienConseiller = bs.chercherConseiller(choix, banque);
		Scanner in = new Scanner(System.in);
		String idConseiller;
		Conseiller nouveauConseiller;

		do {
			System.out.println("Entrer l'identifiant du nouveau conseiller, ou 0 pour annuler");
			idConseiller = in.nextLine();

			if ("0".equals(idConseiller)) {
				return;
			}
			nouveauConseiller = bs.rechercheConseiller(idConseiller, banque);
			if (nouveauConseiller == null) {
				System.out.println("Ce conseiller n'est pas present au sein de cette agence !!!");
			} else {
				break;
			}
		} while (true);

		if (ancienConseiller != null) {
			ancienConseiller.getPortefeuilleClient().remove(choix.getIdentifiant());
			nouveauConseiller.getPortefeuilleClient().put(choix.getIdentifiant(), choix);
		}

	}

	/**
	 * desactive un client sans le supprimer, annule toute action possible sur le
	 * client et ses comptes
	 * 
	 * @param administrateur : l'Administrateur
	 * @param banque         : la Banque
	 * @return true si la desactivation a bien ete faites, false sinon
	 */
	public boolean desactiverClient(Administrateur administrateur, Banque banque) {
		Client choix = choixClient(administrateur, banque);
		if (choix == null) {
			return false;
		}
		if (ControleClient.isActif(choix)) {
			choix.setActif(false);
			return true;
		}
		return false;
	}

	/**
	 * Service de modification des infos Client
	 * 
	 * @param banque         : la Banque du client
	 * @param administrateur : le administrateur qui modifie les inforamtions
	 * @return true si la modification a ete faites
	 */
	public boolean modifierInfosClient(Administrateur administrateur, Banque banque) {
		Client client = choixClient(administrateur, banque);
		if (client != null) {
			ConseillerService cs = new ConseillerService();
			return cs.modifierInfosClient(client);
		}
		return false;
	}

	/**
	 * Service qui renvoie le client souhaitant faire une operation, recherche du
	 * client vai identififant, nom , ou numero compte
	 * 
	 * @param administrateur : l'Adminastreur qui utilise le menu
	 * @return le client recherche, ou null
	 */
	private Client choixClient(Administrateur administrateur, Banque banque) {
		if (administrateur == null || banque == null) {
			System.out.println("Erreur, admistrateur ou banque inexistant");
			return null;
		}
		System.out.println(banque.getListeAgences().size());
		if (!ControleBanque.hasClient(banque)) {
			System.out.println("Pas de client dans la banque");
			return null;
		}
		Scanner in = new Scanner(System.in);
		String identifiant;
		Client client;
		String typeRecherche;
		do {
			System.out.println(
					"type de recherche du client : \n1 : via identifiant\n2 : via nom\n3 : via numero de compte");
			typeRecherche = in.nextLine();
			if (ControleSaisie.saisieInt(typeRecherche, 1, 3)) {
				break;
			} else {
				System.out.println("Entrer 1, 2 ou 3");
			}
		} while (true);

		do {
			System.out.println("Entrer la valeur recherche, ou 0 pour annuler : ");
			identifiant = in.nextLine();
			if ("0".equals(identifiant)) {
				System.out.println("annulation");
				return null;
			}
			BanqueService bs = new BanqueService();
			client = bs.rechercheClient(banque, typeRecherche, identifiant);
			if (client != null) {
				break;
			} else {
				System.out.println("aucun client correspond a la recherche");
			}
		} while (true);
		return client;
	}

	public void consulterInfoClient(Administrateur personne, Banque banque) {
		Client client = choixClient(personne, banque);

		if (client != null) {
			IhmClient ihmClient = new IhmClient();
			ihmClient.affichageInfoClient(client);
		}

	}

	/**
	 * virement a partir du log admin
	 * 
	 * @param administrateur
	 * @param banque
	 */
	public void virement(Administrateur administrateur, Banque banque) {
		Client client = choixClient(administrateur, banque);
		ClientService cs = new ClientService();
		cs.virement(client, banque);

	}

	public void consulterComptesClient(Administrateur personne, Banque banque) {
		Client client = choixClient(personne, banque);

		if (client != null) {
			IhmClient ihmClient = new IhmClient();
			ihmClient.affichageFicheClient(client);
		}

	}

}
