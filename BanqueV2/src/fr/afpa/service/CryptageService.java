package fr.afpa.service;

public class CryptageService {

	/**
	 * service pour encoder un mot de passe
	 * 
	 * @param motDePasse : mot de passe a encoder
	 * @return mot de passe coder
	 */
	public String encoder(String motDePasse) {
		return new StringBuilder(motDePasse).reverse().toString();
	}

	/**
	 * service pour decoder un mot de passe
	 * 
	 * @param motDePasse : mot de passe a decoder
	 * @return mot de passe decoder
	 */
	public String decoder(String motDePasse) {
		return new StringBuilder(motDePasse).reverse().toString();
	}

}
