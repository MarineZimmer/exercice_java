package fr.afpa.service;

import java.util.Map.Entry;
import java.util.Scanner;

import fr.afpa.controle.ControleBanque;
import fr.afpa.controle.ControleSaisie;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;

public class AgenceService {

	/**
	 * Service de creation d'une agence
	 * 
	 * @return l'Agence Creer
	 */
	public Agence creerAgence(Banque banque) {
		String nomAgence;
		String adresse;
		String codeAgence;
		AleatoireService as = new AleatoireService();

		Scanner in = new Scanner(System.in);
		System.out.println("---------------  Creation Agence  -------------------------");
		do {
			System.out.println("Entrer nom de l'agence");
			nomAgence = in.nextLine();
			if (ControleSaisie.validerNom(nomAgence)) {
				break;
			} else {
				System.out.println("Entrer que des lettres !!!!!");
			}
		} while (true);

		do {
			System.out.println("Entrer l'adresse de l'agence");
			adresse = in.nextLine();
			if (ControleSaisie.validerAdresse(adresse)) {
				break;
			} else {
				System.out.println("Entrer une adresse valide !!!!!");
			}
		} while (true);

		do {
			codeAgence = as.aleatoireNombre(3);
		} while (!ControleBanque.codeAgenceUnique(banque, codeAgence));
		System.out.println("---------------------------------------------------------");
		return new Agence(nomAgence, adresse, codeAgence);
	}

	/**
	 * service de recherche d'un conseiller dans une agence ,via login
	 * 
	 * @param login : le login du conseiller recherche
	 * @return : le conseiller si il est trouve, null sinon
	 */
	public Conseiller rechercheConseiller(String login, Agence agence) {
		if (agence == null) {
			return null;
		}
		return agence.getListeConseillers().get(login);
	}

	/**
	 * Service de recherche d'un client au sein d'une agence
	 * 
	 * @param agence          : agence dans laquelle la recherche est faite
	 * @param typeRecherche   : le type de recherche du client(identifiant(1),
	 *                        nom(2), numero de compte(3))
	 * @param valeurRecherche : la valeur recherche
	 * @return : le premier Client correspondant a la recherche
	 */
	public Client rechercheClient(Agence agence, String typeRecherche, String valeurRecherche) {
		if (agence == null) {
			return null;
		}
		ConseillerService cs = new ConseillerService();
		for (Entry<String, Conseiller> itConseiller : agence.getListeConseillers().entrySet()) {
			Conseiller conseiller = itConseiller.getValue();
			Client client = cs.rechercheClient(conseiller, typeRecherche, valeurRecherche);
			if (client != null) {
				return client;
			}
		}
		return null;
	}

	/**
	 * service de recherche d'un compte via le numero
	 * 
	 * @param numeroCompte : numero de compte recherche
	 * @param agence       : agence dans laquelle la recherche est faite
	 * @return le Compte recherche si trouve , false sinon
	 */
	public Compte rechercheCompte(String numeroCompte, Agence agence) {
		if (agence == null) {
			return null;
		}
		ConseillerService cs = new ConseillerService();
		for (Entry<String, Conseiller> itConseiller : agence.getListeConseillers().entrySet()) {
			Conseiller conseiller = itConseiller.getValue();
			Compte compte = cs.rechercheCompte(numeroCompte, conseiller);
			if (compte != null) {
				return compte;
			}
		}
		return null;
	}

	public Conseiller rechercheConseillerClient(Client client, Agence agence) {
		if (client == null || agence == null) {
			return null;
		}
		for (Entry<String, Conseiller> itConseiller : agence.getListeConseillers().entrySet()) {
			Conseiller conseiller = itConseiller.getValue();
			if (conseiller.getPortefeuilleClient().containsKey(client.getIdentifiant())) {
				return conseiller;
			}
		}
		return null;
	}

}
