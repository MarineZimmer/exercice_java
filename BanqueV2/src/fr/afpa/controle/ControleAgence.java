package fr.afpa.controle;

import fr.afpa.entite.Agence;

public class ControleAgence {

	private ControleAgence() {
		super();
	}

	/**
	 * Controle si des conseillers existent au sein de l'agence
	 * 
	 * @param agence l'Agence ou on veut controler si des conseillers sont presents
	 * @return true s'il y a des conseillers , false sinon
	 */
	public static boolean conseillerExiste(Agence agence) {
		return !agence.getListeConseillers().isEmpty();
	}

	/**
	 * Controle si un conseiller existe au sein d'une agence
	 * 
	 * @param agence       : l'Agence a controler
	 * @param idConseiller : l'identifiant du conseiller a rechercher
	 * @return true si le conseiller existe au sein de l'agence false sinon
	 */
	public static boolean conseillerPresent(Agence agence, String idConseiller) {
		return agence.getListeConseillers().containsKey(idConseiller);
	}

}
