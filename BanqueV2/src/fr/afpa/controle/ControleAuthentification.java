package fr.afpa.controle;

import fr.afpa.service.CryptageService;
import fr.afpa.service.FichierService;

public class ControleAuthentification {

	private ControleAuthentification() {
		super();
	}

	/**
	 * 
	 * service qui vérifie si l'authentification est bonne
	 * 
	 * @param login      : une chaine de caractères représentant le login
	 * @param motDePasse : une chaine de caractères représentant le mot de passe
	 * @return identifiant si l'authentification est réussite, null sinon
	 */
	public static String authentification(String login, String motDePasse) {
		FichierService fs = new FichierService();
		CryptageService cs = new CryptageService();
		String[] lignes = fs.lecture("ressources//fichierAuthentification.txt");
		for (int i = 0; i < lignes.length; i++) {
			if (lignes[i] != null) {
				String[] ligne = lignes[i].split(";");
				if (ligne.length == 3 && ligne[1].equals(login) && ligne[2].equals(cs.encoder(motDePasse))) {
					return ligne[0];
				}
			}
		}
		return null;
	}

}
