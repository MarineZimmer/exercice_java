package fr.afpa.controle;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ControleSaisie {

	private ControleSaisie() {
		super();
	}

	/**
	 * controle si la date est valide, et si elle est apr�s la dateDebut si on
	 * souhaite une date apres une certaine date
	 * 
	 * @param date      : la date a controler
	 * @param dateDebut : la date limite, chaine vide si il n'a pas de date limite
	 * @return true si la date est valide est apr�s la date de debut(si il y a une
	 *         date limite) et avant ou egale a la date du jour, false sinon
	 */
	public static boolean isDate(String date, String dateDebut) {
		try {
			LocalDate date1 = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			if (!"".equals(dateDebut)) {
				LocalDate date2 = LocalDate.parse(dateDebut, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				if (date1.isBefore(date2)) {
					return false;
				}
			}
			if (date1.isAfter(LocalDate.now())) {
				return false;
			}

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Comtrole si le nom est valide
	 * 
	 * @param nom : le nom a controler
	 * @return true si le nom est valide
	 */
	public static boolean validerNom(String nom) {
		return nom.matches("^[A-Za-z- ]+$");
	}

	/**
	 * Controle si le mail est vailde
	 * 
	 * @param mail mail a controler
	 * @return true si le mail est valide, false sinon
	 */
	public static boolean validerMail(String mail) {
		String regexMailPartie1 = "\\w(\\.?[\\w-]+)*";
		String regexMailPartie2 = "[\\w&&[^_]]{3,13}";
		String regexMailPartie3 = "[\\w&&[^_]]{2,3}";
		String rejexMail = "^" + regexMailPartie1 + "@" + regexMailPartie2 + "\\." + regexMailPartie3 + "$";
		return mail.matches(rejexMail);
	}

	/**
	 * Controle si le mot de passe est valide
	 * 
	 * @param motDePasse : le mot de passe a controler
	 * @return true si le mot de passe est valide false sinon
	 */
	public static boolean validerMotDePasse(String motDePasse) {
		return motDePasse.matches("\\S{4,}$");
	}

	/**
	 * Controle si la chaine de caracteres est un int compris entre le min et le max
	 * inclus
	 * 
	 * @param nombre : chaine de caracteres a tester
	 * @param min    : int representant le minimum
	 * @param max    :int representant le maximum
	 * @return true la chaine est un int compris entre le min et le max inclus,
	 *         false sinon
	 */
	public static boolean saisieInt(String nombre, int min, int max) {
		try {
			int n = Integer.parseInt(nombre);
			if (n < min || n > max) {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Controle si la chaine en parametre est un float
	 * 
	 * @param nombre : chaine a tester
	 * @return true si la chaine en parametre est un float
	 */
	public static boolean saisieFloat(String nombre) {
		try {
			Float.parseFloat(nombre);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Controle si la chaine est un boolean
	 * 
	 * @param reponse :chaine a tester
	 * @return true si la chaine est "true" ou "false", false sinon
	 */
	public static boolean saisieBoolean(String reponse) {
		try {
			Boolean.parseBoolean(reponse);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Controle si l'adressse est valide
	 * 
	 * @param adresse adresse a controler
	 * @return true si l'adresse est valide
	 */
	public static boolean validerAdresse(String adresse) {
		return adresse.length() > 0;
	}

	/**
	 * Controle si la chaine de caractere est eagle a oui ou non
	 * 
	 * @param reponse : la chaine de caractere a tester
	 * @return true si reponse est egale a oui ou non
	 */
	public static boolean isOuiNon(String reponse) {
		return "oui".equalsIgnoreCase(reponse) || "non".equalsIgnoreCase(reponse);
	}
}
