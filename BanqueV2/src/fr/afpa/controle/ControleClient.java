package fr.afpa.controle;

import fr.afpa.entite.Client;

public class ControleClient {

	private ControleClient() {
		super();
	}

	/**
	 * Controle si un client est actif
	 * 
	 * @param client : le client a controler
	 * @return true si le client est actif, false sinon
	 */
	public static boolean isActif(Client client) {
		return client.isActif();
	}

}
