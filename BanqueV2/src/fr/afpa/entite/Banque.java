package fr.afpa.entite;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import fr.afpa.service.AdministrateurService;

public class Banque implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HashMap<String, Agence> listeAgences;
	private Administrateur administrateur;

	public Banque() {
		super();
		this.listeAgences = new HashMap<String, Agence>();
		AdministrateurService as = new AdministrateurService();
		this.administrateur = as.creerAdministrateur();
	}

	public Map<String, Agence> getListeAgences() {
		return listeAgences;
	}

	public void setListeAgence(Map<String, Agence> listeAgences) {
		this.listeAgences = (HashMap<String, Agence>) listeAgences;
	}

	public Administrateur getAdministrateur() {
		return administrateur;
	}

	public void setAdministrateur(Administrateur administrateur) {
		this.administrateur = administrateur;
	}

	@Override
	public String toString() {
		return "Banque [listeAgences=" + listeAgences + ", administrateur=" + administrateur + "]";
	}

}
