package fr.afpa.entite;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Agence implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codeAgence;
	private String nomAgence;
	private String adresse;
	private HashMap<String, Conseiller> listeConseillers;
	private static int nbAgence;

	public Agence() {
		super();
	}

	public Agence(String nomAgence, String adresse, String codeAgence) {
		super();
		this.nomAgence = nomAgence;
		this.adresse = adresse;
		this.codeAgence = codeAgence;
		this.listeConseillers = new HashMap<String, Conseiller>();
	}

	public String getCodeAgence() {
		return codeAgence;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public void setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Map<String, Conseiller> getListeConseillers() {
		return listeConseillers;
	}

	public void setListeConseillers(Map<String, Conseiller> listeConseillers) {
		this.listeConseillers = (HashMap<String, Conseiller>) listeConseillers;
	}

	public static int getNbAgence() {
		return nbAgence;
	}

	public static void setNbAgence(int nbAgence) {
		Agence.nbAgence = nbAgence;
	}

	@Override
	public String toString() {
		return "\nAgence [codeAgence=" + codeAgence + ", nomAgence=" + nomAgence + ", adresse=" + adresse
				+ ", listeConseillers=" + listeConseillers + "]";
	}

	@Override
	public int hashCode() {
		return Integer.parseInt(codeAgence);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && codeAgence != null) {
			return this.codeAgence.equals(((Agence) obj).getCodeAgence());
		}
		return false;
	}

}
