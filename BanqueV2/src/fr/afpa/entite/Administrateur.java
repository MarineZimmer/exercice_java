package fr.afpa.entite;

import java.io.Serializable;
import java.time.LocalDate;

public class Administrateur extends Conseiller implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int nbAdmin;

	public Administrateur() {
		super();
	}

	public Administrateur(String nom, String prenom, LocalDate dateNaissance, String mail) {
		super(nom, prenom, dateNaissance, mail);
		this.setLogin("ADM" + String.format("%02d", ++nbAdmin));
	}

	@Override
	public int hashCode() {
		return Integer.parseInt(this.getLogin().substring(3));
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return "Administrateur [toString()=" + super.toString() + "]";
	}

}
