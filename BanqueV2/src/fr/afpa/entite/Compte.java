package fr.afpa.entite;

import java.io.Serializable;

public class Compte implements Serializable {

	private float solde;
	private float frais;
	private boolean decouvert;
	private String numeroCompte;
	private boolean actif;

	public Compte() {
		super();
	}

	public Compte(boolean decouvert, String numeroCompte) {
		super();
		this.decouvert = decouvert;
		this.actif = true;
		this.frais = 25;
		this.numeroCompte = numeroCompte;
		this.solde = 0;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public float getFrais() {
		return frais;
	}

	public void setFrais(float frais) {
		this.frais = frais;
	}

	public boolean isDecouvert() {
		return decouvert;
	}

	public void setDecouvert(boolean decouvert) {
		this.decouvert = decouvert;
	}

	public String getNumeroCompte() {
		return numeroCompte;
	}

	public boolean isActif() {
		return actif;
	}

	public void setActif(boolean actif) {
		this.actif = actif;
	}

	@Override
	public int hashCode() {
		return Integer.parseInt(numeroCompte);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && numeroCompte != null) {
			return this.solde == ((Compte) obj).getSolde() && this.frais == ((Compte) obj).getFrais()
					&& this.numeroCompte.equals(((Compte) obj).getNumeroCompte())
					&& this.decouvert == ((Compte) obj).isDecouvert() && this.actif == ((Compte) obj).isActif();
		}
		return false;
	}

	@Override
	public String toString() {
		return "\nCompte [solde=" + solde + ", frais=" + frais + ", decouvert=" + decouvert + ", numeroCompte="
				+ numeroCompte + ", actif=" + actif + "]";
	}

}
