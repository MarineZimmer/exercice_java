package fr.afpa.entite;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class Client extends Personne implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String identifiant;
	private HashMap<String, Compte> listeComptes;
	private boolean actif;

	private static int nbClient;

	public Client() {
		super();
	}

	public Client(String nom, String prenom, LocalDate dateNaissance, String mail) {
		super(nom, prenom, dateNaissance, mail);

		this.listeComptes = new HashMap<String, Compte>();
		this.actif = true;
		this.identifiant = "" + this.getNom().toUpperCase().charAt(0) + this.getPrenom().toUpperCase().charAt(0)
				+ String.format("%06d", ++nbClient);
		this.setLogin(String.format("%010d", nbClient));
	}

	public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	public Map<String, Compte> getListeComptes() {
		return listeComptes;
	}

	public void setListeComptes(Map<String, Compte> listeComptes) {
		this.listeComptes = (HashMap<String, Compte>) listeComptes;
	}

	public boolean isActif() {
		return actif;
	}

	public void setActif(boolean actif) {
		this.actif = actif;
	}

	public static int getNbClient() {
		return nbClient;
	}

	public static void setNbClient(int nbClient) {
		Client.nbClient = nbClient;
	}

	@Override
	public String toString() {
		return "\nClient [" + super.toString() + " identifiant=" + identifiant + ", actif=" + actif + ", listeComptes="
				+ listeComptes + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			return super.equals((Personne) obj) && this.identifiant.equals(((Client) obj).getIdentifiant())
					&& this.actif == ((Client) obj).isActif();
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Integer.parseInt(this.getIdentifiant().substring(2));
	}

}
