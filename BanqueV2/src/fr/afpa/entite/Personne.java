package fr.afpa.entite;

import java.io.Serializable;
import java.time.LocalDate;

public class Personne implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String login;
	private String nom;
	private String prenom;
	private LocalDate dateNaissance;
	private String mail;

	public Personne() {
		super();
	}

	public Personne(String nom, String prenom, LocalDate dateNaissance, String mail) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.mail = mail;
		login = "";
	}

	public String getLogin() {
		return login;
	}

	protected void setLogin(String login) {
		this.login = login;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public LocalDate getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(LocalDate dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public String toString() {
		return "Personne [Login=" + login + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance
				+ ", mail=" + mail + "]";
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null)
			return false;
		return dateNaissance.equals(((Personne) obj).getDateNaissance()) && mail.equals(((Personne) obj).getMail())
				&& nom.equals(((Personne) obj).getNom()) && prenom.equals(((Personne) obj).getPrenom())
				&& this.login.equals(((Personne) obj).getLogin());
	}

	@Override
	public int hashCode() {
		return login.hashCode();
	}

}
