package fr.afpa.entite;

import java.io.Serializable;

public class CompteEpargne extends Compte implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CompteEpargne() {
		super();
	}

	public CompteEpargne(boolean decouvert, String numeroCompte) {
		super(decouvert, numeroCompte);
	}

	@Override
	public String toString() {
		return "CompteEpargne [toString()=" + super.toString() + "]";
	}

}
