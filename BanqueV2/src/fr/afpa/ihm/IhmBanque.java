package fr.afpa.ihm;

import java.util.Map.Entry;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;

public class IhmBanque {

	/**
	 * Affichage info de la Banque ainsi que la liste de ces agences
	 * 
	 * @param banque : la Banque dont on souhaite afficher les infos
	 */
	public void affichageInfoBanque(Banque banque) {
		IhmAgence ihmAgence = new IhmAgence();
		System.out.println("-------------------- Infos Banque --------------------------------");
		System.out.println("Administrateur : " + banque.getAdministrateur().getNom());

		for (Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			ihmAgence.affichageInfoAgence(agence.getValue());
		}
		System.out.println("-----------------------------------------------------------------");
	}

}
