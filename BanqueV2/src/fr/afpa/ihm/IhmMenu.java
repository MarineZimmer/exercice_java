package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controle.ControleClient;
import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.Personne;
import fr.afpa.service.BanqueService;

public class IhmMenu {

	/**
	 * Menu authentification
	 * 
	 * @param banque : la Banque
	 * @return la Personne authentifi�e
	 */
	public Personne menuAuthentification(Banque banque) {
		Scanner in = new Scanner(System.in);
		String login;
		String motDePasse;
		String identifiant;
		BanqueService bs = new BanqueService();
		do {
			System.out.println("Q : quitter\nLogin : ");
			login = in.nextLine();
			if ("Q".equals(login)) {
				return null;
			}
			System.out.println("Mot de passe : ");
			motDePasse = in.nextLine();
			identifiant = bs.authentification(login, motDePasse);
			if (identifiant != null) {
				break;
			} else {
				System.out.println("authentification non valide!!!");
			}
		} while (true);
		return bs.personneAuthentifier(identifiant, banque);
	}

	public void affichageMenu(Personne personne) {

		System.out.println();
		System.out.println("--------------------------   MENU   ---------------------------------");
		System.out.println("Menu :");
		System.out.println("1 - Consulter informations client");
		System.out.println("2 - Consulter comptes client");
		System.out.println("3 - Consulter operations de chaque compte");
		System.out.println("4 - Faire un virement");
		System.out.println("5 - Imprimer un relev� d'un compte");
		System.out.println("6 - Alimenter un compte");
		System.out.println("7 - Retirer de l'argent ");

		if (personne instanceof Conseiller) {
			System.out.println("\n8 - Creer un compte");
			System.out.println("9 - Creer un client");
			System.out.println("10 - Changer la domiciliation d'un client");
			System.out.println("11 - Modifier informations d'un client");
		}

		if (personne instanceof Administrateur) {
			System.out.println("\n12 - Creer une agence");
			System.out.println("13 - Creer un conseiller");
			System.out.println("14 - Desactiver un compte");
			System.out.println("15 - Desactiver un client");
		}

		System.out.println("0 - Quitter le programme");
		System.out.println("--------------------------------------------------------------------");

	}

	/**
	 * service qui affiche et traite les Menus authentification et utilisateur
	 * 
	 * @param banque : la Banque
	 */
	public void menu(Banque banque) {

		do {
			Personne personne = menuAuthentification(banque);
			if (personne != null) {
				if (personne instanceof Client && !ControleClient.isActif((Client) personne)) {
					System.out.println("Client inactif");
					continue;
				}
				do {
					affichageMenu(personne);
					if (!traitementChoixMenu(personne, banque)) {
						break;
					}
				} while (true);
			} else {
				break;
			}
		} while (true);
	}

	/**
	 * Service qui traite le choix de l'utilisateur
	 * 
	 * @param personne : l'utilisateur en cours (Client, Conseiller ou
	 *                 Administrateur
	 * @param banque   ; la Banque
	 * @return true s'il n'a pas choisit de quitter, false sinon
	 */
	private boolean traitementChoixMenu(Personne personne, Banque banque) {

		Scanner in = new Scanner(System.in);
		String choix = "";
		System.out.println("\nVotre choix : ");
		choix = in.nextLine();
		if ("0".equals(choix)) {
			return false;
		}
		BanqueService bs = new BanqueService();

		switch (choix) {
		case "1":
			bs.consulterInfoClient(banque, personne);
			return true;
		case "2":
			bs.consulterComptesClient(banque, personne);
			return true;
		case "3":
			bs.consulterOperationsCompte(banque, personne);
			return true;
		case "4":
			bs.virement(banque, personne);
			return true;
		case "5":
			bs.imprimerReleveCompte(banque, personne);
			return true;
		case "6":
			bs.alimenterCompte(banque, personne);
			return true;
		case "7":
			bs.retirerArgent(banque, personne);
			return true;
		default:
		}
		if (personne instanceof Conseiller) {
			switch (choix) {
			case "8":
				bs.creationCompte((Conseiller) personne, banque);
				return true;
			case "9":
				bs.creationClient(banque, (Conseiller) personne);
				return true;
			case "10":
				bs.changerDomiciliationCompte(banque, (Conseiller) personne);
				return true;
			case "11":
				bs.modifierInfosClient(banque, (Conseiller) personne);
				return true;
			default:
			}
		}
		if (personne instanceof Administrateur) {
			switch (choix) {
			case "12":
				bs.creationAgence(banque, (Administrateur) personne);
				return true;
			case "13":
				bs.creationConseiller(banque, (Administrateur) personne);
				return true;
			case "14":
				bs.desactivationCompte(banque, (Administrateur) personne);
				return true;
			case "15":
				bs.desactivationClient(banque, (Administrateur) personne);
				return true;
			default:
			}
		}
		System.out.println("Choix invalide");
		return true;
	}
}
