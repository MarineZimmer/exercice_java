package fr.afpa.ihm;

import java.util.Map.Entry;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Conseiller;

public class IhmAgence {

	/**
	 * Afiichage des infos d'une agence, ainsi que les conseillers present au sein
	 * de l'agence
	 * 
	 * @param agence : l'agence dont on souhaite afficher les infos
	 */
	public void affichageInfoAgence(Agence agence) {
		System.out.println("-------------------- Infos Agence--------------------------------");
		System.out.println("Nom : " + agence.getNomAgence());
		System.out.println("Code agence : " + agence.getCodeAgence());
		System.out.println("Adresse : " + agence.getAdresse());
		for (Entry<String, Conseiller> conseiller : agence.getListeConseillers().entrySet()) {
			System.out.println("\tConseiller : " + conseiller.getValue().getNom() + "identifiant : "
					+ conseiller.getKey() + "nb Client : " + conseiller.getValue().getPortefeuilleClient().size());
		}
		System.out.println("-----------------------------------------------------------------");
	}
}
