package fr.afpa.ihm;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import fr.afpa.controle.ControleSaisie;
import fr.afpa.entite.Compte;
import fr.afpa.service.CompteService;
import fr.afpa.service.FichierService;

public class IhmCompte {

	/**
	 * affichage des info d'un compte
	 * 
	 * @param compte le compte que l'on veut afficher les infos
	 */
	public void affichageInfoCompte(Compte compte) {
		CompteService cs = new CompteService();
		System.out.println("-------------------- Infos Compte  ---------------------------");
		System.out.println("numero de compte : " + compte.getNumeroCompte());
		System.out.println("solde : " + compte.getSolde());
		System.out.println("decouvert autorise : " + compte.isDecouvert());
		System.out.println("actif : " + compte.isActif());
		System.out.println("frais  : " + cs.calculFrais(compte));
		System.out.println("-----------------------------------------------------------------");
	}

	/**
	 * Affichage de l'ensemble des operation d'un compte
	 * 
	 * @param compte : le Compte a afficher les operations
	 */
	public void affichageOperations(Compte compte) {
		FichierService fs = new FichierService();
		String[] transactions = fs
				.lecture("ressources\\transactions\\transactionsCompte\\compte" + compte.getNumeroCompte() + ".txt");
		StringBuilder operations = new StringBuilder();
		operations.append(
				"----------------- compte numero " + compte.getNumeroCompte() + " ----------------------------\n\n");
		operations.append("\n\n");

		operations.append(String.format("%20s", "date |"));
		operations.append(String.format("%20s", "type |"));
		operations.append(String.format("%20s", "valeur|") + "\n");

		for (int i = 0; i < transactions.length; i++) {
			String[] transaction = transactions[i].split(";");
			if (transaction.length == 4) {
				operations.append(String.format("%20s", transaction[0] + " |"));
				operations.append(String.format("%20s", transaction[1] + " |"));
				operations.append(String.format("%20s", transaction[2] + " |") + "\n");
			}
		}
		operations.append("------------------------------------------------------------------------------------\n\n");
		System.out.println(operations);
	}

	/**
	 * ihm qui demande entrer le type de compte a creer
	 * 
	 * @return le type de compte e creer (1 : compte courant, 2 : livret A, 3 :
	 *         compte epargne logement)
	 */
	public String entrerTypeCompte() {
		String typeCompte;
		Scanner in = new Scanner(System.in);
		do {
			System.out.println(
					"Quel type de compte voulez vous cr�er : \n1 : Compte courant\n2 : Livret A\n3 : Plan Epargne logement");
			typeCompte = in.nextLine();
			if (ControleSaisie.saisieInt(typeCompte, 1, 3)) {
				break;
			} else {
				System.out.println("Entrer 1, 2 ou 3");
			}
		} while (true);
		return typeCompte;
	}

	/**
	 * ihm qui demande si le compte est autorise de decouvert
	 * 
	 * @return true si le compte est autorise de decouvert false sinon
	 */
	public String entrerDecouvert() {
		Scanner in = new Scanner(System.in);
		String decouvert;
		do {
			System.out.println("Decouvert autorise (true ou false) : ");
			decouvert = in.nextLine();
			if (ControleSaisie.saisieBoolean(decouvert)) {
				break;
			} else {
				System.out.println("Entrer true ou false.");
			}
		} while (true);
		return decouvert;
	}

	/**
	 * ihm qui demande la date de debut du releve
	 * 
	 * @return la date de debut du releve
	 */
	public String entrerDateDebutReleve() {
		String dateDebut;
		Scanner in = new Scanner(System.in);
		do {
			System.out.println("Entrer la date de debut du releve (format dd/MM/yyyyy) : ");
			dateDebut = in.nextLine();
			if (ControleSaisie.isDate(dateDebut, "")) {
				break;
			} else {
				System.out.println("Entrer une date valide");
			}
		} while (true);
		return dateDebut;

	}

	/**
	 * ihm qui demande la date de fin du releve
	 * 
	 * @return la date de fin du releve
	 */
	public String entrerDateFinReleve(String dateDebut) {
		String dateFin;
		Scanner in = new Scanner(System.in);
		do {
			System.out.println("Entrer la date de fin du releve (format dd/MM/yyyyy) apres de " + dateDebut + " : ");

			dateFin = in.nextLine();
			if (ControleSaisie.isDate(dateFin, dateDebut)) {
				break;
			} else {
				System.out.println("Entrer une date valide");
			}
		} while (true);
		return dateFin;
	}
}
