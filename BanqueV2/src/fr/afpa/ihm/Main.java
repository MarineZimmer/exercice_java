package fr.afpa.ihm;

import java.time.LocalDate;
import java.util.Scanner;

import fr.afpa.controle.ControleSaisie;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.LivretA;
import fr.afpa.service.ChargementService;
import fr.afpa.service.CompteService;
import fr.afpa.service.CryptageService;
import fr.afpa.service.FichierService;

public class Main {

	public static void main(String[] args) {

		Banque banque = null;

		FichierService fs = new FichierService();

		Object objBanque = fs.lireObjetFichier("ressources\\sauvegarde\\banque.txt");

		// chargement de la banque et initialisation des variables statique des classes
		// conseiller et client (pour l'autoincrementation)
		if (objBanque instanceof Banque) {
			banque = (Banque) objBanque;
			ChargementService chargementService = new ChargementService();
			chargementService.initialisationVariableStatique(banque);
		}

		// creation d'une nouvelle Banque, si le chargement n'a pas ete effectue
		if (banque == null || banque.getAdministrateur() == null) {
			System.out.println("----- Creation nouvelle banque -----------");
			banque = new Banque();
			System.out.println(
					"Voulez vous cr�ez : \n 1 : une banque vide\n2: une banque initialise avec une agence, un conseiller, un client et 2 comptes");
			Scanner in = new Scanner(System.in);
			String choix;
			do {
				choix = in.nextLine();
				if (ControleSaisie.saisieInt(choix, 1, 2)) {
					break;
				} else {
					System.out.println("Entrer 1 ou 2");
				}
			} while (true);
			if ("2".equals(choix)) {
				initialisationBanque(banque);
			}
		}

		IhmMenu ihmMenu = new IhmMenu();
		ihmMenu.menu(banque);
		fs.ecrireObjetFichier(banque, "ressources\\sauvegarde\\banque.txt");
	}

	/**
	 * Fonction initialisation de la banque (creation d'une agence avec un
	 * conseiller, un client et 2 comptes)
	 * 
	 * @param banque : la Banque que l'on initialise
	 */
	public static void initialisationBanque(Banque banque) {
		FichierService fs = new FichierService();
		CompteService cs = new CompteService();
		String numeroCompte = "00000000001";
		String idConseiller = "CO0002";
		String idClient = "CC000001";
		String dateNaissance = "1980-01-01";
		String codeAgence = "001";
		CryptageService cryptageService = new CryptageService();
		banque.getListeAgences().put(codeAgence, new Agence("agence une", "1 rue agence une", "001"));
		banque.getListeAgences().get(codeAgence).getListeConseillers().put(idConseiller, new Conseiller("conseiller un",
				"conseiller un", LocalDate.parse(dateNaissance), "conseilerun@banque.fr"));
		fs.ecritureFichier("ressources\\fichierAuthentification.txt",
				idConseiller + ";" + idConseiller + ";" + cryptageService.encoder("cons1"), true);
		Client client = new Client("client un", "client un", LocalDate.parse(dateNaissance), "clientun@banque.fr");
		banque.getListeAgences().get(codeAgence).getListeConseillers().get(idConseiller).getPortefeuilleClient()
				.put(idClient, client);
		fs.ecritureFichier("ressources\\fichierAuthentification.txt",
				"CC000001;0000000001;" + cryptageService.encoder("client1"), true);

		Compte compte = new CompteCourant(true, numeroCompte);

		fs.ecritureFichier("ressources\\transactions\\transactionsCompte\\compte" + compte.getNumeroCompte() + ".txt",
				"", false);

		banque.getListeAgences().get(codeAgence).getListeConseillers().get(idConseiller).getPortefeuilleClient()
				.get(idClient).getListeComptes().put(numeroCompte, compte);

		cs.modificationSoldeCompte(100, banque.getListeAgences().get(codeAgence).getListeConseillers().get(idConseiller)
				.getPortefeuilleClient().get(idClient).getListeComptes().get(numeroCompte));

		numeroCompte = "00000000002";
		Compte compte2 = new LivretA(false, numeroCompte);

		fs.ecritureFichier("ressources\\transactions\\transactionsCompte\\compte" + compte2.getNumeroCompte() + ".txt",
				"", false);

		banque.getListeAgences().get(codeAgence).getListeConseillers().get(idConseiller).getPortefeuilleClient()
				.get(idClient).getListeComptes().put(numeroCompte, compte2);

	}

}
