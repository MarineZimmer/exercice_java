package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controle.ControleSaisie;

public class IhmPersonne {

	/**
	 * ihm pour demander d'entrer un nom
	 * 
	 * @return le nom dans le bon format
	 */
	public String entrerNom() {
		String nom;
		Scanner in = new Scanner(System.in);
		do {
			System.out.println("Entrer le nom  : ");
			nom = in.nextLine();
			if (!ControleSaisie.validerNom(nom)) {
				System.out.println("Le nom doit ne contenir que des lettres.");
			} else {
				break;
			}

		} while (true);
		return nom;
	}

	/**
	 * ihm pour demander d'entrer un prenom
	 * 
	 * @return le prenom dans le bon format
	 */
	public String entrerPrenom() {
		String prenom;
		Scanner in = new Scanner(System.in);
		do {
			System.out.println("Entrer le prenom  : ");
			prenom = in.nextLine();
			if (!ControleSaisie.validerNom(prenom)) {
				System.out.println("Le prenom doit ne contenir que des lettres.");
			} else {
				break;
			}

		} while (true);
		return prenom;
	}

	/**
	 * ihm pour demander d"entrer la date de naissance
	 * 
	 * @return la date de naissance
	 */
	public String entrerDateNaissance() {
		String dateNaissance;
		Scanner in = new Scanner(System.in);
		do {
			System.out.println("Entrer la date de naissance  (format dd/MM/yyyy) : ");
			dateNaissance = in.nextLine();
			if (!ControleSaisie.isDate(dateNaissance, "")) {
				System.out.println("La date est non valide (format dd/MM/yyyy).");
			} else {
				break;
			}

		} while (true);
		return dateNaissance;
	}

	/**
	 * ihm pour demander une addresse mail
	 * 
	 * @return une adresse mail avec un format valide
	 */
	public String entrerMail() {
		String mail;
		Scanner in = new Scanner(System.in);
		do {
			System.out.println("Entrer le mail :");
			mail = in.nextLine();
			if (!ControleSaisie.validerMail(mail)) {
				System.out.println("Le mail est non valide");
			} else {
				break;
			}
		} while (true);
		return mail;
	}

	/**
	 * ihm qui demande de creer un mot de passe
	 * 
	 * @return le mot de passe
	 */
	public String entrerMotDePasse() {
		String motDePasse;
		Scanner in = new Scanner(System.in);
		do {
			System.out.println("Entrer le mot de passe :");
			motDePasse = in.nextLine();
			if (!ControleSaisie.validerMotDePasse(motDePasse)) {
				System.out.println("Mot de passe invalide minimum 4 caracteres, les espaces sont interdit ");
			} else {
				break;
			}
		} while (true);
		return motDePasse;
	}
}
