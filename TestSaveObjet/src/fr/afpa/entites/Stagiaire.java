package fr.afpa.entites;

import java.io.Serializable;

public class Stagiaire implements Serializable {
	
	
	private static final long serialVersionUID = -3690442560943086002L;

	private String nom;
	private int age;
	private boolean admis;
	private float note;

	public Stagiaire() {
		super();
	}

	@Override
	public String toString() {
		return "Stagiaire [nom=" + nom + ", age=" + age + ", admis=" + admis + ", note=" + note + "]";
	}

	public Stagiaire(String nom, int age, boolean admis, float note) {
		super();
		this.nom = nom;
		this.age = age;
		this.admis = admis;
		this.note = note;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public boolean isAdmis() {
		return admis;
	}

	public void setAdmis(boolean admis) {
		this.admis = admis;
	}

	public float getNote() {
		return note;
	}

	public void setNote(float note) {
		this.note = note;
	}

}
