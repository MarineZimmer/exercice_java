package fr.afpa.main;

import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import fr.afpa.entite.Compte;
import fr.afpa.services.FichierService;

public class Main {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		ArrayList<Compte> listeComptes = new ArrayList<Compte>();
		for (int i = 0; i < 2; i++) {
			System.out.println("solde : ");
			float solde = in.nextFloat();
			System.out.println("frais : ");
			float frais = in.nextFloat();
			System.out.println("decouvert : ");
			boolean decouvert = in.nextBoolean();
			System.out.println("numero de compte : ");
			String numeroCompte = in.next();
			System.out.println("actif : ");
			boolean actif = in.nextBoolean();
			Compte compte = new Compte(decouvert, numeroCompte);
			compte.setActif(actif);
			compte.setFrais(frais);
			compte.setSolde(solde);
			listeComptes.add(compte);
		}

		String chemin = "sauvegarde.txt";
		FileOutputStream fileOutputStream;
		DataOutputStream dataouput = null;
		try {
			fileOutputStream = new FileOutputStream(chemin);
			dataouput = new DataOutputStream(fileOutputStream);
			for (Compte compte2 : listeComptes) {
				dataouput.writeFloat(compte2.getSolde());
				dataouput.writeFloat(compte2.getFrais());
				dataouput.writeBoolean(compte2.isDecouvert());
				dataouput.writeUTF(compte2.getNumeroCompte());
				dataouput.writeBoolean(compte2.isActif());
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (dataouput != null) {
				try {
					dataouput.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		ArrayList<Compte> listeComptes2 = new ArrayList<Compte>();
		FileInputStream fileInputStream;
		DataInputStream dataInput = null;
	
			try {
				fileInputStream = new FileInputStream(chemin);
				dataInput = new DataInputStream(fileInputStream);
				while(true) {
				float solde = dataInput.readFloat();
				float frais = dataInput.readFloat();
				boolean decouvert = dataInput.readBoolean();
				String numeroCompte = dataInput.readUTF();
				boolean actif = dataInput.readBoolean();
				
				Compte compte = new Compte(decouvert, numeroCompte);
				compte.setActif(actif);
				compte.setFrais(frais);
				compte.setSolde(solde);
				listeComptes2.add(compte);
				}
			}catch (EOFException e) {
				System.out.println(e.getMessage());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				if (dataInput != null) {
					try {
						dataInput.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
		for (Compte compte : listeComptes2) {
			System.out.println(compte);
		}
		
	}
}