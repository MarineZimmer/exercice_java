package fr.afpa.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FichierService {

	/**
	 * methode qui lit un fichier et qui retourne un tableau de chaine de caract�res
	 * representant les lignes du fichier
	 * 
	 * @param cheminFichier : chaine de caract�res repr�sentant le chemin absolue du
	 *                      fichier
	 * @return un tableau de chaine de caract�res repr�sentant les lignes du fichier
	 */
	public static String[] lecture(String cheminFichier) {
		String[] lignes;
		int nbLignes = 0;
		int i = 0;
		try {
			FileReader fr = new FileReader(cheminFichier);
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				br.readLine();
				nbLignes++;
			}
			br.close();
		} catch (Exception e) {
			System.out.println("Erreur " + e);
		}
		lignes = new String[nbLignes];
		try {
			FileReader fr = new FileReader(cheminFichier);
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				lignes[i] = br.readLine();
				i++;
			}
			br.close();
		} catch (Exception e) {
			System.out.println("Erreur " + e);
		}
		return lignes;

	}

	/**
	 * methode qui �crire dans un fichier
	 * 
	 * @param chemin          : chemin du fichier dans lequels on veut �crire
	 * @param ecritureFichier : lignes que l'on veut ajouter
	 * @param ajouter         : true si on veut ajouter, false si on veut �craser le
	 *                        fichier
	 */
	public void ecritureFichier(String chemin, String ecritureFichier, boolean ajouter) {

		try (FileWriter fw = new FileWriter(chemin, ajouter); BufferedWriter bw = new BufferedWriter(fw)) {
			bw.write(ecritureFichier);
			bw.newLine();
		} catch (FileNotFoundException e) {
			System.out.println("fichier non trouv�  ");
		} catch (IOException e) {
			System.out.println("Erreur");
		}

	}

	/**
	 * Service qui ecrit un objet dans un fichier
	 * 
	 * @param obj    : objet a ecrire
	 * @param chemin : chemin du fichier
	 */
	public void ecrireObjetFichier(Object obj, String chemin) {

		try (FileOutputStream fichier = new FileOutputStream(chemin);
				ObjectOutputStream objet = new ObjectOutputStream(fichier)) {
			objet.writeObject(obj);
		} catch (FileNotFoundException e) {
			System.out.println("fichier non trouv�   ");
		} catch (IOException e) {
			System.out.println("Erreur  ");
		}

	}

	/**
	 * service qui retourne un objet stocke dans un fichier
	 * 
	 * @param chemin : le chemin du fichier ou l'objet est stocke
	 * @return : l'objet lu
	 */
	public Object lireObjetFichier(String chemin) {
		Object obj = null;
		try (FileInputStream fichier = new FileInputStream(chemin);
				ObjectInputStream objetStream = new ObjectInputStream(fichier)) {
			obj = objetStream.readObject();
			return obj;
		} catch (FileNotFoundException e) {
			System.out.println("sauvegarde non trouvee creation d'une nouvelle banque ");
		} catch (IOException e) {
			System.out.println("Erreur ");
		} catch (ClassNotFoundException e) {
			System.out.println("Erreur ");
		}
		return null;

	}
	
	
	
}
