package fr.afpa.donjon.entite;

import java.util.Random;

public class Salle {
	private static int largeur;
	private static int hauteur;
	private int coordonneeX;
	private int coordonneeY;
	private int nbObjet;
	private int nbMonstre;
	private boolean mur;
	
	private boolean depH=true;
	private boolean depB=true;
	private boolean depG=true;
	private boolean depD=true;

	public Salle() {
		super();
	}

	public Salle(int coordonneeX, int coordonneeY, int nbObjet, int nbMonstre) {
		super();
		this.coordonneeX = coordonneeX;
		this.coordonneeY = coordonneeY;
		this.nbObjet = nbObjet;
		this.nbMonstre = nbMonstre;
		this.mur = (this.coordonneeX == 0 && this.coordonneeY == 1) || (this.coordonneeX == 0 && this.coordonneeY == 5)
				|| (this.coordonneeX == 1 && this.coordonneeY == 3) || (this.coordonneeX == 2 && this.coordonneeY == 0)
				|| (this.coordonneeX == 2 && this.coordonneeY == 1);
	//	this.mur=new Random().nextBoolean();
		if((this.coordonneeX == 0 && this.coordonneeY == 0)||(this.coordonneeX == 2 && this.coordonneeY == 5)) {
			this.mur=false;
		}
	this.depH=!((this.coordonneeX == 2 && this.coordonneeY == 5) ||(this.coordonneeX == 2 && this.coordonneeY == 4)); 
	this.depB=!((this.coordonneeX == 1 && this.coordonneeY == 5) ||(this.coordonneeX == 1 && this.coordonneeY == 4)); 
	
	}

	/**
	 * @return the largeur
	 */
	public static int getLargeur() {
		return largeur;
	}

	/**
	 * @param largeur the largeur to set
	 */
	public static void setLargeur(int largeur) {
		Salle.largeur = largeur;
	}

	/**
	 * @return the hauteur
	 */
	public static int getHauteur() {
		return hauteur;
	}

	/**
	 * @param hauteur the hauteur to set
	 */
	public static void setHauteur(int hauteur) {
		Salle.hauteur = hauteur;
	}

	/**
	 * @return the coordonneeX
	 */
	public int getCoordonneeX() {
		return coordonneeX;
	}

	/**
	 * @param coordonneeX the coordonneeX to set
	 */
	public void setCoordonneeX(int coordonneeX) {
		this.coordonneeX = coordonneeX;
	}

	/**
	 * @return the coordonneeY
	 */
	public int getCoordonneeY() {
		return coordonneeY;
	}

	/**
	 * @param coordonneeY the coordonneeY to set
	 */
	public void setCoordonneeY(int coordonneeY) {
		this.coordonneeY = coordonneeY;
	}

	/**
	 * @return the nbObjet
	 */
	public int getNbObjet() {
		return nbObjet;
	}

	/**
	 * @param nbObjet the nbObjet to set
	 */
	public void setNbObjet(int nbObjet) {
		this.nbObjet = nbObjet;
	}

	/**
	 * @return the nbMonstre
	 */
	public int getNbMonstre() {
		return nbMonstre;
	}

	/**
	 * @param nbMonstre the nbMonstre to set
	 */
	public void setNbMonstre(int nbMonstre) {
		this.nbMonstre = nbMonstre;
	}

	public String affichage(int ligne) {
		if (mur) {
			if (ligne == 0) {
				return String.format("%-"+largeur+"s", "*****");
			}
			if (ligne == 1 ||ligne == 2 || ligne == 0) {
				return String.format("%-"+largeur+"s", "|   |");
			}
			if (ligne == 3) {
				return String.format("%-"+largeur+"s", "*****");
			}
			return String.format("%-"+largeur+"s", "");

		} else {
			if (ligne == 0) {
				return String.format("%-"+largeur+"s", "P");
			}
			if (ligne == 1) {
				if(nbMonstre>0)
				return String.format("%-"+largeur+"s", nbMonstre+"M");
			}
			if (ligne == 2) {
				if(nbObjet>0)
				return String.format("%-"+largeur+"s", nbObjet+"O");
			}
			if (ligne == 3) {
				if(!depB)
				return String.format("%-"+largeur+"s", "*****");
			}
			return String.format("%-"+largeur+"s", "");
		}
	}

}
