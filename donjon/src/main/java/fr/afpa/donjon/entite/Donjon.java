package fr.afpa.donjon.entite;

public class Donjon {
	private Salle[][] donjon;

	public Donjon(int hauteur, int largeur) {
		super();
		donjon = new Salle[hauteur][largeur];
	}

	/**
	 * @return the donjon
	 */
	public Salle[][] getDonjon() {
		return donjon;
	}

	/**
	 * @param donjon the donjon to set
	 */
	public void setDonjon(Salle[][] donjon) {
		this.donjon = donjon;
	}
	
	public void affichage() {
		System.out.println("################################");
		for (int i = 0; i < donjon.length; i++) {
			StringBuilder builder1 = new StringBuilder().append("#");
			StringBuilder builder2 = new StringBuilder().append("#");;
			StringBuilder builder3 = new StringBuilder().append("#");
			StringBuilder builder4 = new StringBuilder().append("#");
			for (int j = 0; j < donjon[0].length; j++) {
					builder1.append(donjon[i][j].affichage(0));
					builder2.append(donjon[i][j].affichage(1));
					builder3.append(donjon[i][j].affichage(2));
					builder4.append(donjon[i][j].affichage(3));
			}
			builder1.append("#");
			builder2.append("#");
			builder3.append("#");
			builder4.append("#");
			System.out.println(builder1);
			System.out.println(builder2);
			System.out.println(builder3);
			System.out.println(builder4);
		}
		System.out.println("################################");
	}

}
