package fr.afpa.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MainTriTableau {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] tab = new int[20];
		for (int i = 0; i < tab.length; i++) {
			tab[i] = new Random().nextInt(100);
		}

		System.out.println(Arrays.toString(tab));
		triTableau(tab);
		System.out.println("Tableau tri�");
		System.out.println(Arrays.toString(tab));

	}

	/**
	 * Tri le tableau d'entiers pass� en param�tre(le nombre pair dans l'odre
	 * croissant, suivi des nombre impair dans l'odre croissant
	 * 
	 * @param tab : tableau entiers � trier
	 */
	public static void triTableau(int[] tab) {
		List<Integer> pair = new ArrayList<Integer>();
		List<Integer> impair = new ArrayList<Integer>();
		int i;
		for (i = 0; i < tab.length; i++) {
			if (tab[i] % 2 == 0) {
				pair.add(tab[i]);
			} else {
				impair.add(tab[i]);
			}
		}
		Collections.sort(pair);
		Collections.sort(impair);
		i = 0;
		for (Integer integer : pair) {
			tab[i] = integer;
			i++;
		}
		for (Integer integer : impair) {
			tab[i] = integer;
			i++;
		}
	}
}
