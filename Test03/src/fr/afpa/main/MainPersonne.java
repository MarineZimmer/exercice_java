package fr.afpa.main;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

import fr.afpa.objets.Adresse;
import fr.afpa.objets.Pays;
import fr.afpa.objets.Personne;
import fr.afpa.objets.Saisie;

public class MainPersonne {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		List<Personne> listePersonnes = new ArrayList<Personne>();
		ajoutPersonnes(listePersonnes);
		listePersonnes.add(creerPersonnes(in));
		

		afficherNPersonne(listePersonnes, in);

		LocalDate date = LocalDate.parse("21/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy"));

		System.out.println("\nNaissance apr�s : " + date);
		naissanceApres(listePersonnes, date);

		// tri du tableau par nom ou prenom ou distance
		trierListePersonnes(listePersonnes, in);

		System.out.println("\nTableau trier selon critere : ");
		afficherNPersonne(listePersonnes, in);

		// recherche via nom ou prenom ou nom et prenom
		System.out.println("recherche : ");
		recherchePersonne(listePersonnes,in);
		
		
		Vector<Personne> v = new Vector<Personne>();
		v.add(creerPersonnes(in));
		ajoutPersonnes(v);
		afficherNPersonne(v, in);
		
		LocalDate date2 = LocalDate.parse("21/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		System.out.println("\nNaissance apr�s : " + date2);
		naissanceApres(v, date2);

		// tri du tableau par nom ou prenom ou distance
		trierListePersonnes(v, in);

		System.out.println("\nTableau trier selon critere : ");
		afficherNPersonne(v, in);

		// recherche via nom ou prenom ou nom et prenom
		System.out.println("recherche : ");
		recherchePersonne(v,in);
		
		Personne.setComparaison("nom");
		HashSet<Personne> hashSet = new HashSet<Personne>();
		hashSet.addAll(listePersonnes);
		System.out.println(listePersonnes.size() + " " + hashSet.size());
		//Personne.setComparaison("nom");
		//HashSet<Personne> hashSet2 = new HashSet<Personne>();
		//hashSet2.addAll(hashSet);
		//System.out.println(hashSet2.contains(new Personne("nom","nom")));
		//System.out.println(hashSet2.contains(new Personne("nom","prenom")));
		afficherHashSet(hashSet, in);
		trierHashSet(listePersonnes,hashSet, in);
		rechercheHashMap(hashSet, in);
	}

	
	


	private static void afficherHashSet(HashSet<Personne> hashSet, Scanner in) {
		Iterator it = hashSet.iterator();
		while(it.hasNext()) {
			((Personne)it.next()).affichage();
			if(it.hasNext()) {
				System.out.println("------------------------------------------------------------------------------");
			}
		}
	}
	
	private static void rechercheHashMap(HashSet<Personne> hashSet, Scanner in) {
		System.out.println("critere de recherche (nom , prenom)");
	      Personne.setComparaison(in.nextLine());
	      System.out.println("Entrer la valeur recherch�e");
	      String recherche=in.nextLine();
	  
			
		Iterator it = hashSet.iterator();
		while(it.hasNext()) {
			Personne p = ((Personne)it.next());
			/*if (p.hashCode()== (new Personne(recherche,recherche)).hashCode()) {
				System.out.println("trouv�");
			p.affichage();
			return;
			}*/
			if (p.equals(new Personne(recherche,recherche))) {
				System.out.println("trouv�");
			p.affichage();
			return;
			}
		}
		System.out.println("pas trouv�");
	}

	/**
	 * tri du tableau de personnes 
	 * 
	 * @param listePersonnes : tableau � trier
	 * @param in : le scanner pour la saisie
	 */
	public static void trierHashSet(List list,HashSet<Personne> hashSet, Scanner in) {
		System.out.println("choix du critere de tri (nom, prenom, distance) :");
		Personne.setComparaison(in.nextLine());
		
		
		/*HashSet<Personne> hashTemp= new HashSet<Personne>();
		hashTemp.addAll(hashSet);
		afficherHashSet(hashTemp, in);*/
		List<Personne> listTemp = new ArrayList<Personne>();
		listTemp.addAll(hashSet);
		Collections.sort(listTemp);
		afficherNPersonne(listTemp, in);
		
	}



	/**
	 * recherche personne via nom ou prenom
	 * @param listePersonnes
	 * @param in
	 */
	public static void recherchePersonne(List<Personne> listePersonnes, Scanner in) {
		System.out.println("critere de recherche (nom , prenom)");
      Personne.setComparaison(in.nextLine());
      System.out.println("Entrer la valeur recherch�e");
      String recherche=in.nextLine();
     for (Personne personne : listePersonnes) {
    	if(personne.equals(new Personne(recherche,recherche))) {
			personne.affichage();
			System.out.println("-------------------------------------------------------------------");
			return;
		}
	}
     System.out.println("pas trouv�");
		
	}

	/**
	 * tri du tableau de personnes 
	 * 
	 * @param listePersonnes : tableau � trier
	 * @param in : le scanner pour la saisie
	 */
	public static void trierListePersonnes(List<Personne> listePersonnes, Scanner in) {
		System.out.println("choix du critere de tri (nom, prenom, distance) :");
		Personne.setComparaison(in.nextLine());
		Collections.sort(listePersonnes); // tri selon la valeur de Personne.comparaison
	}

	/**
	 * affiche les personnes naient apr�s la date pass�e en parametre
	 * 
	 * @param listePersonnes : liste de personnes
	 * @param date           : date
	 */
	public static void naissanceApres(List<Personne> listePersonnes, LocalDate date) {
		int n = 0;
		for (Personne personne : listePersonnes) {

			if (date != null && personne.getDateNaissance() != null && personne.getDateNaissance().isAfter(date)) {
				if (n != 0) {
					System.out.println("---------------------------------------------------------------");
				}
				n++;
				personne.affichage();

			}
		}
	}

	/**
	 * affiche le nombre de personnes que l'utilisateur d�sire affficher
	 * 
	 * @param listePersonnes : liste de personnes
	 * @param in             : le scanner pour la saisie
	 */
	public static void afficherNPersonne(List<Personne> listePersonnes, Scanner in) {
		System.out.println("Combien de personnes � afficher : ");
		int nb = Integer.parseInt(Saisie.saisieChiffres(in, -1));
		if (nb > listePersonnes.size()) {
			nb = listePersonnes.size();
		}

		Iterator it = listePersonnes.iterator();
		while (it.hasNext() && nb != 0) {
			((Personne) it.next()).affichage();
			nb--;
			if (it.hasNext() && nb != 0) {
				System.out.println("-----------------------------------------------------------------");
			}
		}
	}

	/**
	 * cr�e une personne selon les choix de l'utilisateur
	 * 
	 * @param in : le scanner pour la saisie
	 * @return la Personne cr��
	 */
	public static Personne creerPersonnes(Scanner in) {
		String nom;
		String prenom;
		LocalDate dateNaissance;
		String numeroTel;
		String email;
		int distanceDomicileFormation;

		Adresse adresse;
		Pays pays;
		String ville;
		String codePostale;
		String libelleRue;
		String numero;
		String code;
		String nomPays;

		System.out.print("Entrer nom : ");
		nom = Saisie.saisieAlphabetique(in);
		System.out.print("Entrer prenom : ");
		prenom = Saisie.saisieAlphabetique(in);
		System.out.print("Entrer date de naissance (dd/MM/yyyy) : ");
		dateNaissance = Saisie.saisieDate(in);
		System.out.print("Entrer numero de telephone(10 chiffres) : ");
		numeroTel = Saisie.saisieChiffres(in, 10);
		System.out.print(
				"Entrer email ([3 caract�res alphab�tiques]. [de 5 � 12 caract�res alphanum�rique]@ [de 4 � 8 caract�res alphanum�rique]. [de 2 � 3 caract�res alphab�tique) : ");
		email = Saisie.saisieEmail(in);

		System.out.print("Informations pays de r�sidence :\n Entrer code pays (2 majuscules)");
		code = Saisie.saisieMajuscules(in, 2);
		System.out.print("Entrer nom du pays : ");
		nomPays = Saisie.saisieAlphabetique(in);
		pays = new Pays(code, nomPays);

		System.out.print("Informations adresse :\n Entrer le numero de rue (uniquement des chiffres) ");
		numero = Saisie.saisieChiffres(in, -1);
		System.out.print("Entrer nom de la rue : ");
		libelleRue = Saisie.saisieRue(in);
		System.out.print("Entrer code postale (5 chiffres) : ");
		codePostale = Saisie.saisieChiffres(in, 5);
		System.out.print("Entrer la ville : ");
		ville = Saisie.saisieAlphabetique(in);
		adresse = new Adresse(numero, libelleRue, codePostale, ville, pays);

		System.out.print("Entrer la distance entre le domicile et le lieu de formation en m�tres (sans virgules) : ");
		distanceDomicileFormation = Integer.parseInt(Saisie.saisieChiffres(in, -1));

		return new Personne(nom, prenom, dateNaissance, email, numeroTel, distanceDomicileFormation, adresse);

	}

	/**
	 * ajout de 9 personnes a la liste
	 * 
	 * @param listePersonnes liste de personnes
	 */
	public static void ajoutPersonnes(List<Personne> listePersonnes) {
		Pays pays = new Pays("FR", "France");
		Adresse adresse = new Adresse("123", "rue machin", "59000", "Lille", pays);
		LocalDate date = LocalDate.parse("21/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		listePersonnes
				.add(new Personne("fayac", "null", date, "abc.abcdeddf@abcde.com", "0123456789", 11223, adresse));

		adresse = new Adresse("12443", "rue mhachin", "59000", "Lille", pays);
		date = LocalDate.parse("21/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		listePersonnes
				.add(new Personne("marine", "zzzbbb", date, "abc.abcdeddf@abcde.com", "0123456789", 12223, adresse));

		adresse = new Adresse("123", "rue magchin", "59000", "Lille", pays);
		date = LocalDate.parse("14/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		listePersonnes
				.add(new Personne("alexendra", "abab", date, "abc.abcdeddf@abcde.com", "0123456789", 1323, adresse));

		adresse = new Adresse("123", "rue machyyin", "59000", "Lille", pays);
		date = LocalDate.parse("21/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		listePersonnes.add(new Personne("astelia", "bac", date, "abc.abcdeddf@abcde.com", "0123456789", 123, adresse));

		adresse = new Adresse("1653", "rue macyyhin", "59000", "Lille", pays);
		date = LocalDate.parse("22/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		listePersonnes.add(new Personne("hakim", "gre", date, "abc.abcdeddf@abcde.com", "0123456789", 123, adresse));

		adresse = new Adresse("123", "rue machin", "59000", "Lille", pays);
		date = LocalDate.parse("24/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		listePersonnes.add(new Personne("ali", "rtrt", date, "abc.abcdeddf@abcde.com", "0123456789", 17717, adresse));

		adresse = new Adresse("123", "rue machin", "59000", "Lille", pays);
		date = LocalDate.parse("21/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		listePersonnes.add(new Personne("gaetan", "rtre", date, "abc.abcdeddf@abcde.com", "0123456789", 1273, adresse));

		adresse = new Adresse("123", "rue machin", "59000", "Lille", pays);
		date = LocalDate.parse("21/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		listePersonnes
				.add(new Personne("guillaume", "artef", date, "abc.abcdeddf@abcde.com", "0123456789", 114723, adresse));

		adresse = new Adresse("123", "rue machin", "59000", "Lille", pays);
		date = LocalDate.parse("27/10/2000", DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		listePersonnes.add(new Personne("julien", "bbbf", date, "abc.abcdeddf@abcde.com", "0123456789", 1417, adresse));

	}

}
