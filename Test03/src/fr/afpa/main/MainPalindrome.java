package fr.afpa.main;

public class MainPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		isPalindrome("LOL");
		isPalindrome("KAYAK");
		isPalindrome("ELLE");
		isPalindrome("accecba");

	}

	/**
	 * affiche VRAI si la chaine est un palindrome, FAUX sinon
	 * @param chaine : chaine de caract�res � tester
	 */
	public static void isPalindrome(String chaine) {
		for (int i = 0; i < chaine.length() / 2; i++) {
			if (chaine.charAt(i) != chaine.charAt(chaine.length() - 1 - i)) {
				System.out.println("FAUX");
				return;
			}
		}
		System.out.println("VRAI");
	}
}
