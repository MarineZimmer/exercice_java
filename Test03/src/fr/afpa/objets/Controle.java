package fr.afpa.objets;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Controle {

	public static boolean isAlpahabetique(String chaine) {
		String rejex = "^[A-Za-z \\-]+$";
		return chaine.matches(rejex);
	}

	public static boolean isDate(String chaine) {
		try {
			LocalDate date = LocalDate.parse(chaine, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static boolean isChiffres(String numero) {
		String rejex = "^[0-9]+$";
		return numero.matches(rejex);
	}

	public static boolean isMail(String email) {
		String rejex = "^[a-zA-Z]{3}\\.[a-zA-Z0-9]{5,12}@[a-zA-Z0-9]{4,8}\\.[a-zA-Z]{2,3}$";
		return email.matches(rejex);
	}

	public static boolean isMajuscules(String code) {
		String rejex = "^[A-Z]+$";
		return code.matches(rejex);
	}
}