package fr.afpa.objets;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Saisie {

	public static String saisieAlphabetique(Scanner in) {
		String chaine;
		chaine = in.nextLine();
		while (!Controle.isAlpahabetique(chaine)) {
			System.out.println("Entrer une chaine alphabetique valide (caracteres alphabetiques(espace et - autoris�)");
			chaine = in.nextLine();
		}
		return chaine;
	}

	public static LocalDate saisieDate(Scanner in) {
		String chaine;
		chaine = in.nextLine();
		while (!Controle.isDate(chaine)) {
			System.out.println("Entrer une date valide (dd//MM//yyyy)");
			chaine = in.nextLine();
		}
		LocalDate date=LocalDate.parse(chaine, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		return date;
	}

	public static String saisieChiffres(Scanner in,int nb) {
		String numero = in.nextLine();
		while (!(Controle.isChiffres(numero) &&  (nb==-1 || numero.length()==nb))) {
			System.out.println("Entrer uniquement des chiffres sans espace et le bon nombre demand�");
			numero = in.nextLine();
		}
		return numero;
		
	}

	public static String saisieEmail(Scanner in) {
		String email;
		email = in.nextLine();
		while (!Controle.isMail(email)) {
			System.out.println("Entrer un mail valide ([3 caract�res alphab�tiques]. [de 5 � 12 caract�res alphanum�rique]@ [de 4 � 8 caract�res alphanum�rique]. [de 2 � 3 caract�res alphab�tique] )");
			email = in.nextLine();
		}
		return email;
	}

	public static String saisieMajuscules(Scanner in, int nb) {
		String code = in.nextLine();
		while (!(Controle.isMajuscules(code) && code.length()==nb)) {
			System.out.println("Entrer " + nb + " Majuscules");
			code = in.nextLine();
		}
		return code;
	}

	public static String saisieRue(Scanner in) {
		String rue = in.nextLine();
		while (rue.length()==0) {
			System.out.println("Entrer un nom rue nom vide");
			rue = in.nextLine();
		}
		return rue;
	}
}
