package fr.afpa.objets;

import java.time.LocalDate;
import java.util.Comparator;

public class Personne implements Comparable {
	private String nom;
	private String prenom;
	private LocalDate dateNaissance;
	private String email;
	private String numeroTel;
	private int distanceDomicileFormation;
	private Adresse adresse;

	private static String comparaison;

	public Personne() {
	}

	public Personne(String nom, String prenom, LocalDate dateNaissance, String email, String numeroTel,
			int distanceDomicileFormation, Adresse adresse) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.email = email;
		this.numeroTel = numeroTel;
		this.distanceDomicileFormation = distanceDomicileFormation;
		this.adresse = adresse;
		comparaison = "";
	}

	public Personne(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = null;
		this.email = "";
		this.numeroTel = "";
		//this.distanceDomicileFormation = 0;
		this.adresse = null;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public LocalDate getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(LocalDate dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumeroTel() {
		return numeroTel;
	}

	public void setNumeroTel(String numeroTel) {
		this.numeroTel = numeroTel;
	}

	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance + ", email=" + email
				+ ", numeroTel=" + numeroTel + ", distanceDomicileFormation=" + distanceDomicileFormation + ", adresse="
				+ adresse + "]";
	}

	public int getDistanceDomicileFormation() {
		return distanceDomicileFormation;
	}

	public void setDistanceDomicileFormation(int distanceDomicileFormation) {
		this.distanceDomicileFormation = distanceDomicileFormation;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public static String getComparaison() {
		return comparaison;
	}

	public static void setComparaison(String comparaison) {
		Personne.comparaison = comparaison;
	}

	

	@Override
	public int hashCode() {
		if("nom".equals(comparaison))
			return nom.charAt(0);
		if("prenom".equals(comparaison))
			return prenom.hashCode();
		return nom.charAt(0);
		//return nom.hashCode()+prenom.hashCode()+distanceDomicileFormation;
	}

	@Override
	public boolean equals(Object obj) {
		if ("nom".equals(comparaison))
			return this.nom.equals(((Personne) obj).getNom());
		if ("prenom".equals(comparaison))
			return this.prenom.equals(((Personne) obj).getPrenom());

		// equals nom/prenom/distance
		return this.nom.equals(((Personne) obj).getNom()) && this.prenom.equals(((Personne) obj).getPrenom());
		//&&this.distanceDomicileFormation==((Personne) obj).getDistanceDomicileFormation();
	}

	@Override
	public int compareTo(Object o) {
		if ("nom".equals(comparaison))
			return this.nom.compareTo(((Personne) o).getNom());
		if ("prenom".equals(comparaison))
			return this.prenom.compareTo(((Personne) o).getPrenom());
		if ("distance".equals(comparaison))
			return this.distanceDomicileFormation - ((Personne) o).getDistanceDomicileFormation();

		// comparaison nom/prenom/distance
		if (this.nom.compareTo(((Personne) o).getNom()) != 0)
			return this.nom.compareTo(((Personne) o).getNom());
		if (this.prenom.compareTo(((Personne) o).getPrenom()) != 0)
			return this.prenom.compareTo(((Personne) o).getPrenom());
		return this.distanceDomicileFormation - ((Personne) o).getDistanceDomicileFormation();
	}

	public void affichage() {
		System.out.println("hashcode : " + hashCode());
		System.out.println("nom : " + nom);
		System.out.println("Prenom : " + prenom);
		System.out.println("Distance : " + distanceDomicileFormation);
		System.out.println("tel : " + numeroTel);
		System.out.println("mail : " + email);
		System.out.println("date de naissance : " + dateNaissance);
		System.out.println("adresse : " + adresse);
	}

}
