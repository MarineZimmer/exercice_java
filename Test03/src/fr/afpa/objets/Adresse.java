package fr.afpa.objets;

public class Adresse {
	private String numero;
	private String libelleRue;
	private String codePostale;
	private String Ville;
	private Pays pays;
	
	public Adresse(String numero, String libelleRue, String codePostale, String ville, Pays pays) {
		this.numero = numero;
		this.libelleRue = libelleRue;
		this.codePostale = codePostale;
		Ville = ville;
		this.pays = pays;
	}
	public Adresse() {
	}
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getLibelleRue() {
		return libelleRue;
	}
	public void setLibelleRue(String libelleRue) {
		this.libelleRue = libelleRue;
	}
	public String getCodePostale() {
		return codePostale;
	}
	public void setCodePostale(String codePostale) {
		this.codePostale = codePostale;
	}
	public String getVille() {
		return Ville;
	}
	public void setVille(String ville) {
		Ville = ville;
	}
	public Pays getPays() {
		return pays;
	}
	public void setPays(Pays pays) {
		this.pays = pays;
	}
	@Override
	public String toString() {
		return "Adresse [numero=" + numero + ", libelleRue=" + libelleRue + ", codePostale=" + codePostale + ", Ville="
				+ Ville + ", pays=" + pays + "]";
	}
	
	
	
	
	
}
