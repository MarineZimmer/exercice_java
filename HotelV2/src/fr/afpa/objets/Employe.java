package fr.afpa.objets;



import java.util.Scanner;

public class Employe {
	private String login;
	private String nom;
	private String prenom;

	/**
	 * @param login
	 * @param nom
	 * @param prenom
	 */
	public Employe(String login, String nom, String prenom) {
		this.login = login;
		this.nom = nom;
		this.prenom = prenom;
	}
	/** modifie le login de l employe
	*
	* @param login_ : le login de l employe
	*/
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * retourne le nouveau login de l employe
	 *
	 * @return : une chaine de caractere correspondant au login de l employe
	 */
	public String getlogin() {
		return login;
	}
	/** modifie le nom de l employe
	*
	* @param nom_ : le nom de l employe
	*/
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * retourne le nouveau nom de l employe
	 *
	 * @return : une chaine de caractere correspondant au nom de l employe
	 */
	public String getNom() {
		return nom;
	}
	/** modifie le prenom de l employe
	*
	* @param prenom_ : le prenom de l employe
	*/
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * retourne le nouveau prenom de l employe
	 *
	 * @return : une chaine de caractere correspondant au prenom de l employe
	 */
	public String getprenom() {
		return prenom;
	}

	
	/**
	 * Cr�ation d'un nouveau Client au sein de l'hotel
	 * 
	 * @param in    : Le Scanner pour la saisie utilisateur
	 * @param hotel : L'hotel dans lequel est cr�� le nouvaeu client
	 * @return une chaine de caract�res repr�sentant le login du nouveau client
	 */
	public Client creationNouveauClient(Scanner in,String[] listeLoginExistant) {
		String login1;
		String nom1;
		String prenom1;
		String mail;

		System.out.println("--------    Cr�ation nouveau client  -------------");
		// saisie des informations concernant le client
		System.out.println("Entrer le login du client (10 chiffres)");
		login1 = Saisie.saisieLoginClient(in);
		while (!Controle.isUnique(login1, listeLoginExistant)) { // controle si le login n'est pas d�j� existant
			System.out.println("Ce login existe d�j� dans notre hotel, veuillez choisir un notre login");
			login1 = Saisie.saisieLoginClient(in);
		}

		System.out.println("Entrer le nom client (uniquement des lettres)");
		nom1 = Saisie.saisieAlphabetic(in);

		System.out.println("Entrer le prenom client (uniquement des lettres)");
		prenom1 = Saisie.saisieAlphabetic(in);

		System.out.println("Entrer le mail client (adresse valide)");
		mail = Saisie.saisieMail(in);
		System.out.println("-----------------------------------------------");
		return new Client(login1, nom1, prenom1, mail);

	}


	

}
