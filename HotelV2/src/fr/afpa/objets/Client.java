package fr.afpa.objets;

public class Client {

	private String login;
	private String nom;
	private String prenom;
	private String mail;
	private int nombreDeReservations;
	
	/**
	 * @param login
	 * @param nom
	 * @param prenom
	 * @param mail
	 */
	public Client(String login, String nom, String prenom, String mail) {
		this.login = login;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		nombreDeReservations=0;
	}
	
	
	@Override
	public String toString() {
		return "Client [login=" + login + ", nom=" + nom + ", prenom=" + prenom + "]";
	}

	/** modifie le login du client
	*
	* @param login_ : le login du client
	*/
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * retourne le nouveau login du client
	 *
	 * @return : une chaine de caractere correspondant au login du client
	 */
	public String getLogin() {
		return login;

	}
	/** modifie le nom du client
	*
	* @param nom_ : le nom du client
	*/
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * retourne le nouveau nom du client de la chambre
	 *
	 * @return : une chaine de caractere correspondant au nom du client
	 */
	public String getNom() {
		return nom;
	}
	/** modifie le prenom du client de la chambre
	*
	* @param prenom_ : le prenom du client
	*/
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * retourne le nouveau prenom du client de la chambre
	 *
	 * @return : une chaine de caractere correspondant au prenom du client
	 */
	public String getPrenom() {
		return prenom;
	}
	/** modifie le mail du client
	*
	* @param mail_ : le mail du client
	*/
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * retourne le nouveau mail du client
	 *
	 * @return : une chaine de caractere correspondant au mail du client
	 */
	public String getMail() {
		return mail;
	}
	/** modifie le nombre de reservations du client
	*
	* @param nombresDeReservations : le nombre de reservations du client
	*/
	public void setNombreDeReservations(int nombreDeReservations) {
		this.nombreDeReservations = nombreDeReservations;
	}
	/**
	 * retourne le nouveau numero de reservations du client
	 *
	 * @return : un entier correspondant au numero de reservations du client
	 */
	public int getNombreDeReservations() {
		return nombreDeReservations;
	}

	

}
