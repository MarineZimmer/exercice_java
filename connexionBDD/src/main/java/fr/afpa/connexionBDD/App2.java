package fr.afpa.connexionBDD;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Properties;


public class App2 
{
    public static void main( String[] args )
    {
       
    		//connexion bdd
     		Connection conn = ConnexionRequeteBdd.connect();
    
     		String requeteEmpPre= "SELECT * FROM emp where nom = ? and prenom = ?";
     		//requete afficcher emp
     		
			String requeteEmp="SELECT * FROM emp";
     		ResultSet resultatEmp = ConnexionRequeteBdd.executionRequeteSelect(requeteEmp, conn);
         	ConnexionRequeteBdd.afficherEmp(resultatEmp);
         	
         	PreparedStatement preStatement;
         	try {
				preStatement = conn.prepareStatement(requeteEmpPre);
				preStatement.setString(1, "LEROY");
				ResultSet resultatEmp2 = preStatement.executeQuery();
				ConnexionRequeteBdd.afficherEmp(resultatEmp2);
	         	} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
         	
     		
     	/*	//requete  afficher serv
     		String requeteServ="SELECT * FROM serv";
     		ResultSet resultatServ = ConnexionRequeteBdd.executionRequeteSelect(requeteServ, conn);
     		ConnexionRequeteBdd.afficherServ(resultatServ);
     		
     		//requete ajout service
     		String requeteAjoutService = "INSERT INTO serv values ('8','CDA','ROUBAIX')";
         	System.out.println(ConnexionRequeteBdd.executionRequeteUpdate(requeteAjoutService, conn));
         		
         	
         	
         	resultatServ = ConnexionRequeteBdd.executionRequeteSelect(requeteServ, conn);
     		ConnexionRequeteBdd.afficherServ(resultatServ);
     		
     		//requete ajout emp
      		String requeteAjoutEmp = "insert into emp values ('1510', 'MOYEN', 'Toto', null,'1000', '1999-12-12', (select round(cast(avg(sal) as numeric),2) from emp e), null,1);";
      		ConnexionRequeteBdd.executionRequeteUpdate(requeteAjoutEmp, conn);
      		
      		requeteAjoutEmp="insert into emp values ('202', 'ZIMMER', 'MARINE', 'PROGRAMMEUR','1500', now(), 40000, 5000,8)";
      		ConnexionRequeteBdd.executionRequeteUpdate(requeteAjoutEmp, conn);
      		
      		
      		resultatEmp = ConnexionRequeteBdd.executionRequeteSelect(requeteEmp, conn);
      		ConnexionRequeteBdd.afficherEmp(resultatEmp);
      		
      		//update emp
      		String requeteModifService = "UPDATE emp SET noserv='8' where nom LIKE 'M%'";
      		System.out.println(ConnexionRequeteBdd.executionRequeteUpdate(requeteModifService, conn));
      		
      		
      		//delete emp
      		String requeteDeleteEmp = "DELETE FROM emp WHERE sal>10000";
      		System.out.println(ConnexionRequeteBdd.executionRequeteUpdate(requeteDeleteEmp, conn));
      		
      		
      		
      		resultatEmp = ConnexionRequeteBdd.executionRequeteSelect(requeteEmp, conn);
      		ConnexionRequeteBdd.afficherEmp(resultatEmp);*/
    }
}
