package fr.afpa.connexionBDD;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
     // TODO Auto-generated method stub
     		// Charger et configurer le driver de la base de données.
     		String driverName = "org.postgresql.Driver";
     		try {
				Class.forName(driverName);
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
     		// Infos de connection
     		String url = "jdbc:postgresql://localhost:5432/TestCDA";
     		Properties props = new Properties();
     		props.setProperty("user", "postgres");
     		props.setProperty("password", "admin");
     		// props.setProperty("ssl","true");
     		// Réaliser la connexion et l'authentification à la base de données.
     		Connection conn = null;
			try {
				conn = DriverManager.getConnection(url, props);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
     		// Deuxiéme façon pour Réaliser la connexion et l'authentification à la base de
     		// données.
     		// Connection conn =
     		// DriverManager.getConnection("jdbc:postgresql://localhost:5432/TestCDA","postgres","admin");
     		// String url =
     		// "jdbc:postgresql://localhost/testCDA?user=postgre&password=admin";
     		// ResultSet : Objet pour Parcourir les informations retournées
     		// par la base de données dans le cas d'une sélection de données
     		ResultSet resultats = null;
     		// Requette à executer
     		String requete = "SELECT * FROM serv";
     		try {
     			// Statement : Contenir la requête SQL et la transmettre à la base de données.
     			Statement stmt = conn.createStatement();
     			// La fonction executeQuery permet d'executer la requêtte
     			resultats = stmt.executeQuery(requete);
     			// Parcourir le résultat et afficher les données
     			while (resultats.next()) {
     				System.out.println();
     				System.out.print(resultats.getInt(1));
     				System.out.print("     " + resultats.getString("service"));
     				System.out.print("     " + resultats.getString("ville"));
     			}
     			
     			ResultSet resultatEmp = null;
         		String requeteEmp="SELECT * FROM emp";
         		resultatEmp=stmt.executeQuery(requeteEmp);
         		while (resultatEmp.next()) {
         			System.out.println();
     				System.out.print(resultatEmp.getInt(1));
     				System.out.print("     " + resultatEmp.getString(2));
     				System.out.print("     " + resultatEmp.getString(3));

     				System.out.print("     " + resultatEmp.getString(4));

     				System.out.print("     " + resultatEmp.getString(5));

     				System.out.print("     " + resultatEmp.getDate(6));

     				System.out.print("     " + resultatEmp.getFloat(7));
     				System.out.print("     " + resultatEmp.getFloat(8));
     				System.out.print("     " + resultatEmp.getInt(9));
					
				}
     		} catch (SQLException e) {
     			e.printStackTrace();
     		} finally {
     			if (conn != null)
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
     		}
     		
     		/*ResultSet resultatEmpDelete = null;
     		String requeteDelete="DELETE FROM emp WHERE emploi='SECRETAIRE' ";
     		try {
				conn = DriverManager.getConnection(url, props);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
     		try {
     			
     			Statement stmt = conn.createStatement();
     			

     			stmt.execute(requeteDelete);
     			
     			ResultSet resultatEmp = null;
         		String requeteEmp="SELECT * FROM emp WHERE emploi='SECRETAIRE'";
         		resultatEmp=stmt.executeQuery(requeteEmp);
         		while (resultatEmp.next()) {
         			System.out.println();
     				System.out.print(resultatEmp.getInt(1));
     				System.out.print("     " + resultatEmp.getString(2));
     				System.out.print("     " + resultatEmp.getString(3));

     				System.out.print("     " + resultatEmp.getString(4));

     				System.out.print("     " + resultatEmp.getString(5));

     				System.out.print("     " + resultatEmp.getDate(6));

     				System.out.print("     " + resultatEmp.getFloat(7));
     				System.out.print("     " + resultatEmp.getFloat(8));
     				System.out.print("     " + resultatEmp.getInt(9));
					
				}
     		} catch (SQLException e) {
     			e.printStackTrace();
     		} finally {
     			if (conn != null)
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
     		}*/
     		
     		
    }
}
