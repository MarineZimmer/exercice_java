package fr.afpa.connexionBDD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;





public class ConnexionRequeteBdd {
	
	
	public static Connection connect() {
		// Charger et configurer le driver de la base de données.
 		String driverName = "org.postgresql.Driver";
 		try {
			Class.forName(driverName);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
 		
 		// Infos de connection
 		String url = "jdbc:postgresql://localhost:5432/TestCDA";
 		Properties props = new Properties();
 		props.setProperty("user", "postgres");
 		props.setProperty("password", "admin");

 		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url, props);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return conn;
	}
	public static ResultSet executionRequeteSelect(String requete, Connection conn) {
		ResultSet resultats = null;
		try {
			Statement stmt = conn.createStatement();
			resultats = stmt.executeQuery(requete);
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return resultats;
	}

	public static void afficherEmp(ResultSet resultatEmp) {
		System.out.println();
		try {
			System.out.println();
			System.out.print(String.format("%-15s", " | ID"));
			System.out.print(String.format("%-15s", " | NOM"));
			System.out.print(String.format("%-15s", " | PRENOM"));
			System.out.print(String.format("%-15s", " | SUPERIEUR"));
			System.out.print(String.format("%-15s", " | date embauche"));
			System.out.print(String.format("%-15s", " | salaire"));
			System.out.print(String.format("%-15s", " | comm"));
			System.out.print(String.format("%-15s", " | no service"));
			System.out.print(
					"\n--------------------------------------------------------------------------------------------------------------------------");
			while (resultatEmp.next()) {
				System.out.println();
				System.out.print(String.format("%-15s", " | " + resultatEmp.getInt(1)));
				System.out.print(String.format("%-15s", " | " + resultatEmp.getString(2)));
				System.out.print(String.format("%-15s", " | " + resultatEmp.getString(3)));
				System.out.print(String.format("%-15s", " | " + resultatEmp.getString(5)));
				System.out.print(String.format("%-15s", " | " + resultatEmp.getDate(6)));
				System.out.print(String.format("%-15s", " | " + resultatEmp.getFloat(7)));
				System.out.print(String.format("%-15s", " | " + resultatEmp.getFloat(8)));
				System.out.print(String.format("%-15s", " | " + resultatEmp.getInt(9)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void afficherServ(ResultSet resultatServ) {
		System.out.println();
		try {
			while (resultatServ.next()) {
				System.out.println();

				System.out.print(resultatServ.getInt(1));
				System.out.print("     " + resultatServ.getString("service"));
				System.out.print("     " + resultatServ.getString("ville"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static int executionRequeteUpdate(String requeteInsert, Connection conn) {
		try {
			Statement stmt = conn.createStatement();
			return stmt.executeUpdate(requeteInsert);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
		
	}
}
