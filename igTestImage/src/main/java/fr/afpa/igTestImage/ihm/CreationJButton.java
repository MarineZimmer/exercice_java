package fr.afpa.igTestImage.ihm;

import java.awt.event.ActionListener;

import javax.swing.JButton;

public class CreationJButton extends  JButton{

	public CreationJButton(String nom,String actionCommand, ActionListener actionListener) {
		super(nom);
		if(!"".equals(actionCommand)) {
			setActionCommand(actionCommand);
		}
		if(actionListener!=null) {
		addActionListener(actionListener);
		}
		
	}
	

}
