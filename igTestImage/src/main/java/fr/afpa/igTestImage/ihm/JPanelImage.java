package fr.afpa.igTestImage.ihm;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class JPanelImage extends JPanel {
	public BufferedImage imageFond;
	public int y;

	public JPanelImage(String fileName)  {
		setLayout(null);
		y=0;
		try {
			imageFond = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	//	g.drawImage(imageFond, 0, 0, null);
		g.drawImage(imageFond.getSubimage(0, y, this.getHeight(), this.getHeight()), 0, 0, null);
		
	}
}
