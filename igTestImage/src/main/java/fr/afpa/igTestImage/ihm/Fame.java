package fr.afpa.igTestImage.ihm;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fr.afpa.igTestImage.metiers.CreationActionListener;

public class Fame extends JFrame implements KeyListener,Runnable{

	private ActionListener actionListener;
	private JPanel jpanel;
	private JPanelImage fond;
	private JPanel meteo;

	public Fame(String title) throws HeadlessException {
		super(title);
		setBounds(200, 200, 400, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		actionListener = new CreationActionListener();
		// setResizable(false);

		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		fond = new JPanelImage("ressources/fond3.jpg");
		this.addKeyListener(this);
		inititialisation();

	}

	private void inititialisation() {
		setJMenuBar(new BarreMenu(actionListener));
		jpanel = new JPanel();
		jpanel.setSize(50, 50);
		jpanel.setBackground(Color.RED);
		
		meteo = new JPanel();
		meteo.setSize(25, 25);
		meteo.setLocation(0,0 );
		meteo.setBackground(Color.RED);
		
		fond.add(jpanel);
		fond.add(meteo);
		jpanel.addKeyListener(this);

		getContentPane().add(fond);
	}

	@Override
	public void keyPressed(KeyEvent e) {
	System.out.println(e.getKeyCode());
	if(e.getKeyCode()==39 && jpanel.getX()<this.getWidth()-jpanel.getWidth()) {
		jpanel.setLocation(jpanel.getX()+4, jpanel.getY());
	}
	if(e.getKeyCode()==37 && jpanel.getX()>0) {
		jpanel.setLocation(jpanel.getX()-4, jpanel.getY());
	}
	if(e.getKeyCode()==38 && jpanel.getY()>0) {
		jpanel.setLocation(jpanel.getX(), jpanel.getY()-4);
	}
	if(e.getKeyCode()==40 && jpanel.getY()<this.getHeight()-jpanel.getHeight()-50) {
		jpanel.setLocation(jpanel.getX(), jpanel.getY()+4);
	}
	this.repaint();
	if(jpanel.getX()==meteo.getX())
		System.out.println("BOOM");

		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run() {
		int t= 500;
	while(true) {
		meteo.setLocation(meteo.getX(), meteo.getY()+2);
		fond.y+=10;
		this.repaint();
		if(fond.y>fond.imageFond.getHeight()-fond.getHeight()-50)
			fond.y=0;
		if(meteo.getY()>this.getHeight()-meteo.getHeight()-10)
			meteo.setLocation(meteo.getX()+20, 0);
		if(jpanel.getX()==meteo.getX())
			jpanel.setBackground(Color.CYAN);
		else
			jpanel.setBackground(Color.RED);
		if(t>10)
			t-=5;
		//System.out.println(t);
		try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	}

}
