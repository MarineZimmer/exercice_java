package fr.afpa.gesper.dao.persistances;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.afpa.gesper.dao.entites.Personne;
import fr.afpa.gesper.utils.HibernateUtils;

public class PersonneDao {

	/**
	 * sauvegarde une personne dans la bdd
	 * 
	 * @param personne : la personne à sauvegarder
	 * @return true si la sauvegarde a été éffectuée, false sinon
	 */
	public boolean savePersonne(Personne personne) {
		Session s = null;
		Transaction tx = null;
		boolean retour = false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.save(personne);
			tx.commit();
			retour = true;
		} catch (Exception e) {

			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {

				}
			}
		} finally {
			if (s != null) {
				s.close();
			}
		}
		return retour;
	}

	/**
	 * supprime une personne de la bdd, ainsi que son compte et ses adresses
	 * 
	 * @param personne la personne à supprimer
	 * @return true si la personne a été supprimée, false sinon
	 */
	public boolean deletePersonne(Personne personne) {
		Session s = null;
		Transaction tx = null;
		boolean retour = false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.delete(personne);
			tx.commit();
			retour = true;
		} catch (Exception e) {

			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {

				}
			}
		} finally {
			if (s != null) {
				s.close();
			}
		}
		return retour;
	}

	/**
	 * renvoie la personne correspondant à l'identifiant en paramètre
	 * 
	 * @param login : l'id de la personne recherchée
	 * @return la personne correspondant à l'id entré en paramètre, ou null si elle
	 *         n'est pas présente en bdd
	 */
	public Personne getPersonne(String login) {
		Session s = null;
		Personne personne = null;
		try {
			s = HibernateUtils.getSession();
			Query q = s.createQuery("from Personne where login=:login");

			q.setParameter("login", login);
			personne = (Personne) q.getSingleResult();
		} catch (Exception e) {
			personne = null;
		} finally {
			if (s != null) {
				s.close();
			}
		}

		return personne;
	}

	/**
	 * renvoie la liste de toutes les personnes
	 * 
	 * @return une liste de toutes les personnes
	 */
	public List<Personne> getAllPersonnes() {
		Session s = HibernateUtils.getSession();
		Query q = s.createQuery("from Personne");
		List<Personne> liste = (List<Personne>) q.list();
		s.close();
		return liste;
	}

	/**
	 * mets à jour les informations d'une personne en bdd
	 * 
	 * @param personne : la personne à mettre à jour
	 * @return true si la mise à jour a été effectué
	 */
	public boolean updatePersonne(Personne personne) {
		Transaction tx = null;
		Session s = null;
		boolean retour = false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.update(personne);
			tx.commit();
			retour = true;
		} catch (Exception e) {

			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {

				}
			}
		} finally {
			if (s != null) {
				s.close();
			}
		}
		return retour;
	}
}
