package fr.afpa.gesper.metier.services;

import java.util.List;

import fr.afpa.gesper.metier.entites.Compte;
import fr.afpa.gesper.metier.entites.Personne;
import fr.afpa.gesper.dtos.PersonneDto;

public class PersonneService {

	/**
	 * service de création d'une personne
	 * 
	 * @param nom    le nom de la personne
	 * @param prenom le prenom de la personne
	 * @param compte le compte de la personne
	 * @return true si la personne a été créée
	 */
	public boolean creationPersonne(String nom, String prenom, Compte compte) {
		Personne personne = new Personne(nom, prenom);
		personne.setCompte(compte);
		return new PersonneDto().savePersonne(personne);
	}

	/**
	 * service de récupération de toutes les personnes
	 * 
	 * @return une liste de toutes les personnes
	 */
	public List<Personne> getAllPersonnes() {
		return new PersonneDto().getAllPersonnes();
	}

	/**
	 * service de récupération d'une personne
	 * 
	 * @param login l'identifiant de la personne recherchée
	 * @return la personne recherchée, null si aucune personne a été trouvé
	 */
	public Personne getPersonne(String login) {
		return new PersonneDto().getPersonne(login);
	}

	/**
	 * service de suppression d'une personne
	 * 
	 * @param personne la personne à supprimer
	 * @return true si la personne a été supprimé,false sinon
	 */
	public boolean deletePersonne(Personne personne) {
		if (personne != null) {
			return new PersonneDto().deletePersonne(personne);
		}
		return false;
	}

	/**
	 * sevice de modification d'une personne
	 * 
	 * @param nom      : le nouveau nom (chaine vide si nom non modifié)
	 * @param prenom   : le nouveau prenom (chaine vide si nom non modifié)
	 * @param personne : la personne à modifier
	 * @return true si la modification de la personne a été effectué, false sinon
	 */
	public boolean modifierPersonne(String nom, String prenom, Personne personne) {
		if (!"".equals(nom)) {
			personne.setNom(nom);
		}
		if (!"".equals(prenom)) {
			personne.setPrenom(prenom);
		}
		return new PersonneDto().updatePersonne(personne);

	}

}
