package fr.afpa.gesper.controles;

public class ControleSaisie {

	private ControleSaisie() {
		super();
	}

	/**
	 * controle si la chaine de caractères en paramètre est non vide
	 * 
	 * @param str : la chaine à controler
	 * @return true si la chaine de caractères est non vide, false sinon
	 */
	public static boolean saisieNonVide(String str) {
		str.replaceAll(" ", "");
		return str.length() > 0;
	}

	/**
	 * controle si la chaine de caractères en paramètre est un nombre
	 * 
	 * @param str : la chaine à controler
	 * @return true si la chaine de caractères est un nombre, false sinon
	 */
	public static boolean saisieInt(String str) {
		try {
			Integer.parseInt(str);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
