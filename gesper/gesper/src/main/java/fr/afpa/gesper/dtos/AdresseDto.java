package fr.afpa.gesper.dtos;

import fr.afpa.gesper.dao.entites.Adresse;
import fr.afpa.gesper.dao.entites.Personne;
import fr.afpa.gesper.dao.persistances.AdresseDao;

public class AdresseDto {

	/**
	 * lien entre le service adresse et le dto adresse pour la sauvegarde d'une
	 * adresse
	 * 
	 * @param adresse : l'adresse à sauvegarder
	 * @return true si l'adresse a été sauvegardée, false sinon
	 */
	public boolean saveAdresse(fr.afpa.gesper.metier.entites.Adresse adresse,
			fr.afpa.gesper.metier.entites.Personne personne) {
		Adresse adressePersistante = adresseToAdresseDao(adresse, personne);
		return new AdresseDao().saveAdresse(adressePersistante);

	}

	/**
	 * lien entre le service adresse et le dto adresse pour la mise à jour d'une
	 * adresse
	 * 
	 * @param adresse : l'adresse à mettre à jour
	 * @return true si l'adresse a été mise à jour, false sinon
	 */
	public boolean updateAdresse(fr.afpa.gesper.metier.entites.Adresse adresse,
			fr.afpa.gesper.metier.entites.Personne personne) {
		Adresse adresseDao = adresseToAdresseDao(adresse, personne);
		return new AdresseDao().updateAdresse(adresseDao);
	}

	/**
	 * lien entre le service adresse et le dto adresse pour la suppression d'une
	 * adresse
	 * 
	 * @param personne : l'adresse à supprimer
	 * @return true si l'adresse a été supprimé, false sinon
	 */
	public boolean deleteAdresse(fr.afpa.gesper.metier.entites.Adresse adresse,
			fr.afpa.gesper.metier.entites.Personne personne) {
		Adresse adresseDao = adresseToAdresseDao(adresse, personne);
		return new AdresseDao().deleteAdresse(adresseDao);
	}

	/**
	 * transforme une adresse persistante en adresse metier
	 * 
	 * @param personneDao : la adresse persistante à transformer
	 * @return une adresse métier
	 */
	private fr.afpa.gesper.metier.entites.Adresse adresseDaoToAdresse(Adresse adresseDao) {
		fr.afpa.gesper.metier.entites.Adresse adresse = new fr.afpa.gesper.metier.entites.Adresse(adresseDao.getIdAd(),
				adresseDao.getNumero(), adresseDao.getRue(), adresseDao.getVille());
		return adresse;

	}

	/**
	 * transforme une adresse metier en adresse persistante
	 * 
	 * @param adresse : l'adresse metier à transformer
	 * @return une adresse persistante
	 */
	private Adresse adresseToAdresseDao(fr.afpa.gesper.metier.entites.Adresse adresse,
			fr.afpa.gesper.metier.entites.Personne personne) {
		Personne personneDao = new PersonneDto().personneToPersonneDao(personne);
		Adresse adresseDao = new Adresse(adresse.getNumero(), adresse.getRue(), adresse.getVille(), personneDao);
		if (adresse.getId() != 0) {
			adresseDao.setIdAd(adresse.getId());
		}
		return adresseDao;

	}

}
