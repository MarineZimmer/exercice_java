package fr.afpa.gesper.ihms;

import fr.afpa.gesper.controles.ControleSaisie;
import fr.afpa.gesper.metier.entites.Compte;
import fr.afpa.gesper.metier.entites.Personne;
import fr.afpa.gesper.main.App;
import fr.afpa.gesper.metier.services.CompteService;

public class CompteIhm {

	/**
	 * ihm de création d'un compte
	 * @return le compte créé
	 */
	public Compte creationCompte() {
		String login;
		String mdp;
		System.out.print("LOGIN : ");
		login=App.getIn().nextLine();
		while(!ControleSaisie.saisieNonVide(login)) {
			System.out.print("Entrer un login non vide : ");
			login=App.getIn().nextLine();
		}
		System.out.print("MDP : ");
		mdp=App.getIn().nextLine();
		while(!ControleSaisie.saisieNonVide(mdp)) {
			System.out.print("Entrer un mot de passe non vide : ");
			mdp=App.getIn().nextLine();
		}
		return new CompteService().creationCompte(login,mdp);
		
	}

	/**
	 * ihm de modification d'un compte
	 * @param personne la personne qui souhaite modifier son compte (uniquement le mot de passe)
	 */
	public static void modifInfoCompte(Personne personne) {
	
		String mdp;
		
		System.out.print("Entrer le nouveau mot de passe (si vous ne souhaitez pas modifier le numero appuyer sur la touche entrée): ");
		
		mdp=App.getIn().nextLine();
		if(new CompteService().modificationCompte(mdp, personne)) {
			System.out.println("La modification a bien été prise en compte");
		}else {
			System.out.println("Erreur lors de la modification");
		}
		 
		
	}

}
