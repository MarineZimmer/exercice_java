package fr.afpa.gesper.metier.entites;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Personne {

	private int id;
	private String nom;
	private String prenom;

	private List<Adresse> listeAdresse = new ArrayList<Adresse>();

	private Compte compte;

	public Personne(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}

	public Personne() {
		super();
	}

	public Personne(int id, String nom, String prenom) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
	}

}
