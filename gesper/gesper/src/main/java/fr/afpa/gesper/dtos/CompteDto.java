package fr.afpa.gesper.dtos;



import fr.afpa.gesper.dao.entites.Compte;
import fr.afpa.gesper.dao.persistances.CompteDao;

public class CompteDto {

	/**
	 * lien entre le service compte et le dto compte pour la sauvegarde d'un compte
	 * @param compte : le compte à sauvegarder
	 * @return true si le compte a été sauvegardé, false sinon
	 */
	public boolean saveCompte(fr.afpa.gesper.metier.entites.Compte compte) {
		Compte compteDao = compteToCompteDao(compte);
		return new CompteDao().save(compteDao);
	}

	/**
	 * lien entre le service compte et le dto compte pour la mise à jour d'un compte
	 * @param compte : le compte à mettre à jour
	 * @return true si le compte a été mis à jour, false sinon
	 */
	public boolean updateCompte(fr.afpa.gesper.metier.entites.Compte compte) {
		Compte compteDao = compteToCompteDao(compte);
		return new CompteDao().updateCompte(compteDao);
	}

	/**
	 * transforme un compte persistant  en compte metier
	 * @param compteDao : le compte persistant à transformer
	 * @return le compte metier
	 */
	public  fr.afpa.gesper.metier.entites.Compte compteDaoToCompte(Compte compteDao) {
		fr.afpa.gesper.metier.entites.Compte compte = new fr.afpa.gesper.metier.entites.Compte(compteDao.getLogin(), compteDao.getMdp());
		return compte;
		
	}
	
	/**
	 * transforme un compte metier  en compte persistant
	 * @param compte : le compte metier à transformer
	 * @return un compte persistant 
	 */
	public  Compte compteToCompteDao(fr.afpa.gesper.metier.entites.Compte compte) {
		Compte compteDao = new Compte(compte.getLogin(), compte.getMdp());
		return compteDao;
		
	}
}
