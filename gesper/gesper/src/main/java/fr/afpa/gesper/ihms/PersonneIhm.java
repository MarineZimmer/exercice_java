package fr.afpa.gesper.ihms;

import java.util.List;



import fr.afpa.gesper.controles.ControleSaisie;
import fr.afpa.gesper.metier.entites.Compte;
import fr.afpa.gesper.metier.entites.Personne;
import fr.afpa.gesper.main.App;
import fr.afpa.gesper.metier.services.PersonneService;

public class PersonneIhm {

	/**
	 * ihm de creation d'une personne
	 * @param compte : le compte de la personne
	 */
	public static void creationPersonne(Compte compte) {
		String nom;
		String prenom;
		System.out.print("NOM : ");
		nom = App.getIn().nextLine();
		while (!ControleSaisie.saisieNonVide(nom)) {
			System.out.print("Entrer un nom non vide : ");
			nom = App.getIn().nextLine();
		}
		System.out.print("PRENOM : ");
		prenom = App.getIn().nextLine();
		while (!ControleSaisie.saisieNonVide(prenom)) {
			System.out.print("Entrer un prenom non vide : ");
			prenom = App.getIn().nextLine();
		}
		
		if (new PersonneService().creationPersonne(nom, prenom, compte)) {
			System.out.println("La personne a été créé");
		}else {
			System.out.println("Erreur dans la création de La personne");
		}
	}

	/**
	 * ihm de suppression d'une personne
	 */
	public static void suppressionPersonne() {
		boolean resultat = false;

		Personne personne = choixPersonne();
		if(personne!=null) {
		resultat = new PersonneService().deletePersonne(personne);
		}

		if (resultat) {
			System.out.println("La personne a été supprimé");
		}else {
			System.out.println("Erreur La personne n'existe pas ou il y a eu un problème lors de la suppression");
		}
	}

	/**
	 * ihm d'affichage de la liste de toutes les personnes
	 */
	public static void afficherAllPersonnes() {
		
		List<Personne> listePersonnes = new PersonneService().getAllPersonnes();
		listePersonnes.forEach(PersonneIhm::afficherPersonne);

	}

	/**
	 * ihm  d'affichage d'une personne
	 * @param personne
	 */
	public static void afficherPersonne(Personne personne) {
		System.out.println("---------------------------- Personne ----------------------------------------");
		System.out.println(
				" login : " + personne.getCompte().getLogin() + " Nom : " + personne.getNom() + " Prenom : " + personne.getPrenom());
		personne.getListeAdresse().forEach(AdresseIhm::afficherAdresse);
	

	}

	/**
	 *  ihm de choix d'une personne
	 * @return la personne choisie, null si l'id saisit ne correspond à aucune personne
	 */
	public static Personne choixPersonne() {
		String login;
		System.out.print("Entrer le login de la personne : ");
		login = App.getIn().nextLine();
		return new PersonneService().getPersonne(login);
	
	}

	/**
	 * ihm de modification d'une personne
	 * @param personne
	 */
	public static void modifInfoPersonne(Personne personne) {
		String nom;
		String prenom;
		System.out.print("Entrer le nouveau nom (si vous ne souhaitez pas modifier le nom appuyer sur la touche entrée): ");
		nom = App.getIn().nextLine();
		System.out.print("Entrer le nouveau prenom (si vous ne souhaitez pas modifier le nom appuyer sur la touche entrée): ");
		prenom = App.getIn().nextLine();
	
		if(new PersonneService().modifierPersonne(nom, prenom, personne)) {
			System.out.println("La modification a bien été prise en compte");
		}else {
			System.out.println("Erreur lors de la modification");
		}
		
	}
}
