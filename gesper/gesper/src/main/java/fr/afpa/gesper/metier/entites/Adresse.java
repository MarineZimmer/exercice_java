package fr.afpa.gesper.metier.entites;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Adresse {

	private int id;
	private int numero;
	private String rue;
	private String ville;

	public Adresse(int numero, String rue, String ville) {
		super();
		this.numero = numero;
		this.rue = rue;
		this.ville = ville;
	}

	public Adresse() {
		super();
	}

	public Adresse(int id, int numero, String rue, String ville) {
		super();
		this.id = id;
		this.numero = numero;
		this.rue = rue;
		this.ville = ville;
	}

}
