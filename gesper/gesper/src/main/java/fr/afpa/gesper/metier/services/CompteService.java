package fr.afpa.gesper.metier.services;

import fr.afpa.gesper.dtos.CompteDto;
import fr.afpa.gesper.metier.entites.Compte;
import fr.afpa.gesper.metier.entites.Personne;

public class CompteService {

	/**
	 * service de création d'un compte
	 * 
	 * @param login : le login du compte
	 * @param mdp   : le mot de passe du compte
	 * @return le compte créé
	 */
	public Compte creationCompte(String login, String mdp) {
		Compte compte = new Compte(login, mdp);
		if (new CompteDto().saveCompte(compte)) {
			return compte;
		}
		return null;

	}

	/**
	 * service de modification d'un compte
	 * 
	 * @param personne la personne propriétaire du compte
	 * @param login    le nouveau login (chaine vide si le login n'est pas modifié)
	 * @param mdp      le mot de passe (chaine vide si le mot de passe n'est pas
	 *                 modifié)
	 * @return true si la modification a été effectuée
	 */
	public boolean modificationCompte(String mdp, Personne personne) {

		Compte compte = new Compte(personne.getCompte().getLogin(), personne.getCompte().getMdp());
		if (!"".equals(mdp)) {
			compte.setMdp(mdp);
		}
		return new CompteDto().updateCompte(compte);

	}

}
