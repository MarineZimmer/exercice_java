package fr.afpa.gesper.dao.entites;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
public class Adresse {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private int idAd;
	@Column
	private int numero;
	@Column
	private String rue;
	@Column
	private String ville;

	@ToString.Exclude
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id_pers")
	private Personne pers;

	public Adresse(int numero, String rue, String ville, Personne pers) {
		super();
		this.numero = numero;
		this.rue = rue;
		this.ville = ville;
		this.pers = pers;
	}

	public Adresse() {
		super();
	}

	public Adresse(int idAd, int numero, String rue, String ville, Personne pers) {
		super();
		this.idAd = idAd;
		this.numero = numero;
		this.rue = rue;
		this.ville = ville;
		this.pers = pers;
	}

}
