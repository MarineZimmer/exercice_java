package fr.afpa.gesper.main;

import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.gesper.dao.entites.Adresse;
import fr.afpa.gesper.dao.entites.Compte;
import fr.afpa.gesper.dao.entites.Personne;
import fr.afpa.gesper.ihms.MenuIhm;
import fr.afpa.gesper.utils.HibernateUtils;

/**
 * Hello world!
 *
 */
public class App {
	private static Session session = null;
	private static Scanner in;

	/**
	 * @return the in
	 */
	public static Scanner getIn() {
		return in;
	}

	public static void main(String[] args) {
		in = new Scanner(System.in);
		init();
		do {
			MenuIhm.affichageMenu();
		} while (MenuIhm.traitementMenu(in.nextLine()));
	}

	public static void init() {
		session = HibernateUtils.getSession();
		Personne personne = new Personne();
		personne.setNom("DUPONT");
		personne.setPrenom("Pierre");

		Compte compte = new Compte();
		compte.setLogin("1");
		compte.setMdp("mdp1");

		personne.setCompte(compte);
		compte.setPersonne(personne);

		Adresse adresse = new Adresse();
		adresse.setNumero(01);
		adresse.setRue("rue truc");
		adresse.setVille("ROUBAIX");

		Adresse adresse2 = new Adresse();
		adresse2.setNumero(04);
		adresse2.setRue("rue bidule");
		adresse2.setVille("LILLE");

		personne.getListeAdresse().add(adresse);
		personne.getListeAdresse().add(adresse2);
		adresse.setPers(personne);
		adresse2.setPers(personne);

		Personne personne2 = new Personne();
		personne2.setNom("DURANT");
		personne2.setPrenom("Paul");

		Compte compte2 = new Compte();
		compte2.setLogin("2");
		compte2.setMdp("mdp2");

		personne2.setCompte(compte2);
		compte2.setPersonne(personne2);

		Adresse adressep2 = new Adresse();
		adressep2.setNumero(22);
		adressep2.setRue("rue machin");
		adressep2.setVille("ROUBAIX");

		Adresse adressep22 = new Adresse();
		adressep22.setNumero(22);
		adressep22.setRue("rue bidule");
		adressep22.setVille("LILLE");

		personne2.getListeAdresse().add(adressep2);
		personne2.getListeAdresse().add(adressep22);
		adressep2.setPers(personne2);
		adressep22.setPers(personne2);

		Transaction tx;
		tx = session.beginTransaction();

		session.save(compte);
		session.save(personne);
		personne.getListeAdresse().forEach(session::save);

		session.save(compte2);
		session.save(personne2);
		personne2.getListeAdresse().forEach(session::save);

		tx.commit();
		session.close();

	}
}
