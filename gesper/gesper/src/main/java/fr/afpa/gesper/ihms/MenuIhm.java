package fr.afpa.gesper.ihms;

import fr.afpa.gesper.metier.entites.Compte;
import fr.afpa.gesper.metier.entites.Personne;
import fr.afpa.gesper.main.App;

public class MenuIhm {

	/**
	 * affichage du menu principal
	 */
	public static void affichageMenu() {
		System.out.println("----------------------------  Menu  ----------------------------------------");
		System.out.println(" 1 : Créer un utilisateur");
		System.out.println(" 2 : Lister les  utilisateurs");
		System.out.println(" 3 : Modifier un utilisateur");
		System.out.println(" 4 : Supprimer un utilisateur");
		System.out.println(" 5 : Quitter");
		System.out.println("--------------------------------------------------------------------------");
	}

	/**
	 * traitement du choix du menu principal
	 * @param choix : le choix saisit
	 * @return true si le choix ne correspond pas à quitter false sinon
	 */
	public static boolean traitementMenu(String choix) {
		switch (choix) {
		case "1":
			creationUtilisateur();

			break;
		case "2":
			PersonneIhm.afficherAllPersonnes();

			break;
		case "3":
			modifInfoUtilisateur();

			break;
		case "4":
			PersonneIhm.suppressionPersonne();

			break;
		case "5":
			return false;

		default:
			break;
		}
		return true;
	}

	/**
	 * ihm de création d'un utilisateur
	 */
	private static void creationUtilisateur() {
		Compte compte = new CompteIhm().creationCompte();
		if(compte==null){
			System.out.println("desolé une erreur lors de la creation du compte, le login saisi existe déjà");
		}else {
		PersonneIhm.creationPersonne(compte);
		}

	}

	/**
	 * ihm de modification d'un utilisateur ou d'ajout d'une adresse
	 */
	private static void modifInfoUtilisateur() {
		Personne personne = PersonneIhm.choixPersonne();
		
		if (personne != null) {
			PersonneIhm.afficherPersonne(personne);
			System.out.println("1 : Modif infos ");
			System.out.println("2 : Ajout Adresse");
			switch (App.getIn().nextLine()) {
			case "1": modifInfo(personne);
				break;
			case "2":
				AdresseIhm.creationAdresse(personne);

				break;

			default:
				break;
			}
		}else {
			System.out.println("Ce login appartient à aucune personne");
		}

	}

	/**
	 * ihm de modifiaction d'un utilisateur (modification : info personne,info compte,adresse, suppression : adresse)
	 * @param personne la personne à modifier
	 */
	private static void modifInfo(Personne personne) {
		System.out.println("1 : Modifier info personne");
		System.out.println("2 : Modifier info compte");
		System.out.println("3 : Modifier Adresse");
		System.out.println("4 : Supprimer une adresse");
		switch (App.getIn().nextLine()) {
		case "1": PersonneIhm.modifInfoPersonne(personne);
			break;
		case "2":
			CompteIhm.modifInfoCompte(personne);
			break;
		case "3":
			AdresseIhm.modifierAdresse(personne);

			break;
		case "4":
			AdresseIhm.suppressionAdresse(personne);
			break;
		default:
			break;
		}
		
	}
}
