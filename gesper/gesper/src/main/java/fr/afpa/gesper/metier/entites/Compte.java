package fr.afpa.gesper.metier.entites;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class Compte {

	private String login;
	private String mdp;

	public Compte(String login, String mdp) {
		super();
		this.login = login;
		this.mdp = mdp;
	}

	public Compte() {
		super();
	}

}
