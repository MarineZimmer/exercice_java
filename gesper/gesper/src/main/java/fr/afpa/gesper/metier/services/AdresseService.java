package fr.afpa.gesper.metier.services;

import fr.afpa.gesper.controles.ControleSaisie;
import fr.afpa.gesper.metier.entites.Personne;
import fr.afpa.gesper.dtos.AdresseDto;
import fr.afpa.gesper.metier.entites.Adresse;

public class AdresseService {

	/**
	 * service de création d'une adresse
	 * 
	 * @param personne : la personne domiciliée à l'adresse
	 * @param numero   : le numero de la rue
	 * @param rue      : le nom de la rue
	 * @param ville    : la ville
	 */
	public boolean creationAdresse(Personne personne, int numero, String rue, String ville) {
		Adresse adresse = new Adresse(numero, rue, ville);
		return new AdresseDto().saveAdresse(adresse, personne);

	}

	/**
	 * service pour récupérer l'adresse d'une personne
	 * 
	 * @param id       : l'identifiant de l'adresse
	 * @param personne : la personne dont on souhaite récupérer une adresse
	 * @return l'adresse recherchée, null si aucune adresse a été trouvée
	 */
	public Adresse getAdressePersonne(int id, Personne personne) {
		for (Adresse adresse : personne.getListeAdresse()) {
			if (adresse.getId() == id) {
				return adresse;
			}
		}
		return null;
	}

	/**
	 * service de modification d'une adresse
	 * 
	 * @param numero  le nouveau numéro de la rue (chaine vide si le numéro n'est
	 *                pas modifié)
	 * @param rue     : le nouveau nom de la rue (chaine vide si le nom n'est pas
	 *                modifié)
	 * @param ville   : la nouvelle ville (chaine vide si la ville n'est pas
	 *                modifiée)
	 * @param adresse : l'adresse à modifier
	 * @return : true si la modification a été faite, false sinon
	 */
	public boolean modifierAdresse(String numero, String rue, String ville, Adresse adresse, Personne personne) {
		if (!"".equals(numero) && ControleSaisie.saisieInt(numero)) {
			adresse.setNumero(Integer.parseInt(numero));
		}
		if (!"".equals(rue)) {
			adresse.setRue(rue);
		}
		if (!"".equals(ville)) {
			adresse.setVille(ville);
		}
		return new AdresseDto().updateAdresse(adresse, personne);
	}

	public boolean supprimerAdresse(Personne personne, Adresse adresse) {
		if (adresse != null) {
			if (new AdresseDto().deleteAdresse(adresse, personne)) {
				personne.getListeAdresse().remove(adresse);
				return true;
			}
		}
		return false;
	}

}
