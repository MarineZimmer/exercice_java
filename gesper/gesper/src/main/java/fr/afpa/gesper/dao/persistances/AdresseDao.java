package fr.afpa.gesper.dao.persistances;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.gesper.dao.entites.Adresse;
import fr.afpa.gesper.utils.HibernateUtils;

public class AdresseDao {

	/**
	 * sauvegarde une nouvelle adresse dans la bdd
	 * 
	 * @param adresse : l'adresse à sauvegarder
	 * @return true si la sauvegarde a été éffectuée, false sinon
	 */
	public boolean saveAdresse(Adresse adresse) {
		Transaction tx = null;
		Session s = null;
		boolean retour = false;
		try {
		 s = HibernateUtils.getSession();
			tx = s.beginTransaction();

			s.save(adresse);
			tx.commit();
			retour =  true;
		} catch (Exception e) {
			
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
				
				}
			}
		}finally {
			if(s!=null) {
			s.close();
			}
		}
		return retour;
	}

	/**
	 * supprime une adresse de la bdd
	 * 
	 * @param adresse l' adresse à supprimer
	 * @return true si la adresse a été supprimée, false sinon
	 */
	public boolean deleteAdresse(Adresse adresse) {
		Transaction tx = null;
		Session s = null;
		boolean retour = false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.delete(adresse);
			tx.commit();
			retour =  true;
		} catch (Exception e) {
		
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
					
				}
			}
		}finally {
			if(s!=null) {
				s.close();
				}
		}
		return retour;

	}


	/**
	 * mets à jour les informations d'une adresse en bdd
	 * 
	 * @param adresse : l' adresse à mettre à jour
	 * @return true si la mise à jour a été effectué
	 */
	public boolean updateAdresse(Adresse adresse) {

		Transaction tx = null;
		Session s =null;
		boolean retour = false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.update(adresse);
			tx.commit();
			retour = true;
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} finally {
			if (s != null) {
				s.close();
			}
		}
		return retour;

	}

}
