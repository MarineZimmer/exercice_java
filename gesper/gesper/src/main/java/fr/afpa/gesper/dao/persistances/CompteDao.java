package fr.afpa.gesper.dao.persistances;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.gesper.dao.entites.Compte;
import fr.afpa.gesper.utils.HibernateUtils;

public class CompteDao {

	/**
	 * sauvegarde une nouveau compte dans la bdd
	 * 
	 * @param adresse : le compte à sauvegarder
	 * @return true si la sauvegarde a été éffectuée, false sinon
	 */
	public boolean save(Compte compte) {
		Transaction tx = null;
		Session s = null;
		boolean retour=false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.save(compte);
			tx.commit();
			retour =  true;
		} catch (Exception e) {
			
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
				
				}
			}
		}finally {
			if(s!=null) {
				s.close();
				}
		}
		return retour;

	}

	

	/**
	 * mets à jour les informations d'un compte en bdd
	 * 
	 * @param personne : la personne propriétaire du compte à mettre à jour
	 * @return true si la mise à jour du compte a été effectué
	 */
	public boolean updateCompte(Compte compte) {
		Session s = null;
		Transaction tx=null;
		boolean retour = false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.update(compte);
			tx.commit();
			retour =  true;
		} catch (Exception e) {
			
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
				
				}
			}
		} finally {
			if (s != null) {
				s.close();
			}
		}
		return retour;

	}

}
