package fr.afpa.gesper.dtos;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.gesper.dao.entites.Compte;
import fr.afpa.gesper.dao.entites.Personne;
import fr.afpa.gesper.dao.persistances.PersonneDao;
import fr.afpa.gesper.dao.entites.Adresse;

public class PersonneDto {

	/**
	 * lien entre le service personne et le dto personne pour la sauvegarde d'une personne
	 * @param personne : la personne à sauvegarder
	 * @return true si la personne a été sauvegardée, false sinon
	 */
	public boolean savePersonne(fr.afpa.gesper.metier.entites.Personne personne) {
		Personne personnePersistant = personneToPersonneDao(personne);
		return new PersonneDao().savePersonne(personnePersistant);
	}

	/**
	 * lien entre le service personne et le dto personne pour la recupération de la liste de toutes les personnes
	 * @return la liste de toutes les personnes
	 */
	public List<fr.afpa.gesper.metier.entites.Personne> getAllPersonnes() {
		List<Personne> listePersonneDao= new PersonneDao().getAllPersonnes();
		List<fr.afpa.gesper.metier.entites.Personne> listePersonne = new ArrayList<>();
		listePersonneDao.forEach(personneDao->listePersonne.add(personneDaoToPersonne(personneDao)));
		return listePersonne;
		
	}

	/**
	 * lien entre le service personne et le dto personne pour la suppression d'une personne
	 * @param personne : la personne à supprimer
	 * @return true si la personne a été supprimer, false sinon
	 */
	public boolean deletePersonne(fr.afpa.gesper.metier.entites.Personne personne) {
		Personne personneDao = personneToPersonneDao(personne);
		personneDao.getListeAdresse().forEach(System.out::println);
		return new PersonneDao().deletePersonne(personneDao);
	}

	/**
	 * lien entre le service personne et le dto personne pour la recherche d'une personne
	 * @param login : l'id de la personne recherchée
	 * @return la personne recherchée ou null si elle n'existe pas en bdd
	 */
	public fr.afpa.gesper.metier.entites.Personne getPersonne(String login) {
		Personne personneDao =  new PersonneDao().getPersonne(login);
		if(personneDao!=null) {
		return personneDaoToPersonne(personneDao);
		}
		return null;
	}

	/**
	 * lien entre le service personne et le dto personne pour la mise à jour d'une personne
	 * @param personne : la personne à mettre à jour
	 * @return true si la personne a été mis à jour, false sinon
	 */
	public boolean updatePersonne(fr.afpa.gesper.metier.entites.Personne personne) {
		Personne personneDao = personneToPersonneDao(personne);
		return new PersonneDao().updatePersonne(personneDao);
		
	}
	
	/**
	 * transforme une personne persistant en personne metier
	 * @param personneDao : la personne persistant à transformer
	 * @return une personne métier 
	 */
	public fr.afpa.gesper.metier.entites.Personne personneDaoToPersonne(Personne personneDao) {
		fr.afpa.gesper.metier.entites.Personne personne = new fr.afpa.gesper.metier.entites.Personne(personneDao.getId(), personneDao.getNom(), personneDao.getPrenom());
		fr.afpa.gesper.metier.entites.Compte compte = new fr.afpa.gesper.metier.entites.Compte(personneDao.getCompte().getLogin(), personneDao.getCompte().getMdp());
		personne.setCompte(compte);
		List<fr.afpa.gesper.metier.entites.Adresse> listeAdresse = new ArrayList<>();
		personneDao.getListeAdresse().forEach(adresse -> listeAdresse.add(new fr.afpa.gesper.metier.entites.Adresse(adresse.getIdAd(),adresse.getNumero(), adresse.getRue(), adresse.getVille())));
		personne.setListeAdresse(listeAdresse);
		return personne;
		
	}
	
	/**
	 * transforme une personne metier  en personne persistant
	 * @param personne : la personne metier à transformer
	 * @return une personne persistant 
	 */
	public  Personne personneToPersonneDao(fr.afpa.gesper.metier.entites.Personne personne) {
		Personne personneDao = new Personne(personne.getNom(), personne.getPrenom());
		Compte compte = new Compte(personne.getCompte().getLogin(), personne.getCompte().getMdp());
		personneDao.setCompte(compte);
		List<Adresse> listeAdresse = new ArrayList<>();
		personne.getListeAdresse().forEach(adresse -> listeAdresse.add(new Adresse(adresse.getId(),adresse.getNumero(), adresse.getRue(), adresse.getVille(),personneDao)));
		personneDao.setListeAdresse(listeAdresse);
		if(personne.getId()!=0) {
			personneDao.setId(personne.getId());
		}
		return personneDao;
		
	}

}
