package fr.afpa.gesper.dao.entites;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
public class Compte {

	@Id
	private String login;
	@Column
	private String mdp;

	@ToString.Exclude
	@OneToOne(mappedBy = "compte")
	private Personne personne;

	public Compte(String login, String mdp) {
		super();
		this.login = login;
		this.mdp = mdp;
	}

	public Compte() {
		super();
	}

}
