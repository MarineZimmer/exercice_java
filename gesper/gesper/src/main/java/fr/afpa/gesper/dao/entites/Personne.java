package fr.afpa.gesper.dao.entites;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
public class Personne {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private int id;
	@Column
	private String nom;
	@Column
	private String prenom;

	@OneToMany(mappedBy = "pers", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Adresse> listeAdresse = new ArrayList<Adresse>();

	@OneToOne(orphanRemoval = true)
	@JoinColumn(name = "login", referencedColumnName = "login")
	private Compte compte;

	public Personne(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}

	public Personne() {
		super();
	}

}
