import java.util.List;

public class ThreadSup extends Thread {
	public List<Integer> liste;

	@Override
	public void run() {
		while (!liste.isEmpty()) {
			liste.remove(0);
		}
		System.out.println("Suppression gagne");
	}

	public ThreadSup(List<Integer> liste) {
		super();
		this.liste = liste;
	}

}
