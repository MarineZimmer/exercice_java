
public class ThreadCompteur extends Thread{
public int no_fin;

public ThreadCompteur(int no_fin) {
	super();
	this.no_fin = no_fin;
}

@Override
public void run() {
	for (int i = 0; i < no_fin; i++) {
		System.out.println(this.getName()  + " : " + i);
	}
}

}
