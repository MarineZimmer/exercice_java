import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
	/*ThreadCompteur cp1 = new ThreadCompteur(100);
	ThreadCompteur cp2 = new ThreadCompteur(50);
	cp1.start();
	cp2.start();*/
		
		List<Integer> liste = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			liste.add(i);
		}
		
		ThreadAjout cp1 = new ThreadAjout(liste);
		ThreadSup cp2 = new ThreadSup(liste);
		cp1.start();
		cp2.start();

	}

}
