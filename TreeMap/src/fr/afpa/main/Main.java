package fr.afpa.main;

import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;

import fr.afpa.objets.Personne;

public class Main {

	public static void main(String[] args) {
		
		TreeMap<String, String> treeMap = new TreeMap<String, String>();
		
		treeMap.put("5", "E1P2");
		treeMap.put("1", "E1P1");
		treeMap.put("7", "E2P2");
		treeMap.put("4", "GP");
		treeMap.put("6", "P2");
		treeMap.put("3", "E2P1");
		treeMap.put("2", "P1");
		System.out.println();
		System.out.println("TreeMap : " + treeMap);
		System.out.println();
		Map.Entry<String, String> dernier = treeMap.lastEntry();
		System.out.println("dernier : " + dernier);
		System.out.println();
		NavigableMap<String, String> subMap = treeMap.subMap("2",true,"6",false);
		System.out.println("supMap entre 2 inclus et 6 non inclus) : " + subMap);
	}
}
